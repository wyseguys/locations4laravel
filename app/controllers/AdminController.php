<?php


class AdminController extends BaseController {

    /*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	|
    */
    public function __construct(IAccountRepository $account)
    {
        $this->account = $account;

        $this->view_data['pageTitle'] = NULL; //default value in case I forget it in an action

        if (Auth::check())
        {
            $this->user = ( Auth::user()->acting_as ) ? Auth::user()->acting_as : Auth::user();
        }
        else
        {
            $this->user = NULL;
        }
            
        $this->beforeFilter('csrf', array('on' => 'post'));
        /*
        $this->beforeFilter('auth');
        
        $this->afterFilter('log', array('only' => array('fooAction', 'barAction')));
        */
    }

	/**
	 * Show the act as customer form
	 *
	 * @return Response
	 */
	public function getActas( $user_id = '' )
    {
        
        $this->view_data['pageTitle'] = 'Act As a Customer';
        if ( $user_id != '' )
        {
            Session::forget('acting_as');
            if( Auth::user()->id == $user_id)
            {
                $this->user->acting_as = NULL;
                return Redirect::to('admin/actas')->with('flash_success', Lang::get('admin.success_actas_clear'));   
            }
            else
            {
                $user = $this->account->findUser(array('id' => $user_id));
                if($user)
                {
                    $this->user->setActAs($user);
                    return Redirect::to('admin/actas')->with('flash_success', Lang::get('admin.success_actas_set') . ' ' . $user->first . ' ' . $user->last);   
                }
            }
        }

        $this->view_data['users'] = Session::get('users', NULL);

        return View::make('admin/actas', $this->view_data);

	}

	/**
	 * Show the act as customer form
	 *
	 * @return Response
	 */
	public function postActas()
	{
        
        $input = Input::all();
        $rules = array(
            'search' => array('required')
        );
        //TODO: move these errors to a language file
        $messages = array(
            'required' => ' is required.',
        );
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes())
        {
            $found_users = $this->account->getImpersonable($input['search']);
            if ( $found_users )
            {
                return Redirect::to('admin/actas')->with('users', $found_users);
            }
            else
            {
                return Redirect::to('admin/actas')->with('flash_error', Lang::get('admin.error_actas_search'));
            }
        }
        else
        {
            return Redirect::to('admin/actas')
                            ->withInput()
                            ->withErrors($validator)
                            ->with('flash_error', Lang::get('admin.error_actas_search'));  
        }
        
	}

	/**
	 * Show the login form
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        $this->view_data['pageTitle'] = 'Admin Dashboard';
        $this->view_data['user'] = $this->user;

        return View::make('admin/index', $this->view_data);

	}

}