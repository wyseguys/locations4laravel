<?php

class CategoryController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Location Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    /*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	|
    */
    public function __construct(ICategoryRepository $category,
                                IProjectRepository $project)
    {
        $this->category = $category;
        $this->project = $project;
        $this->user = ( Auth::user()->acting_as ) ? Auth::user()->acting_as : Auth::user();

        $this->view_data['pageTitle'] = NULL; //default
        /*
        $this->beforeFilter('auth');
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->afterFilter('log', array('only' => array('fooAction', 'barAction')));
        */
    }


    /*
	|--------------------------------------------------------------------------
	| index
	|--------------------------------------------------------------------------
	| show a listing of all the categories available
    */
	public function getIndex()
	{
        $this->view_data['pageTitle'] = Lang::get('category.page_categories');
        $this->view_data['allCategories'] = $this->category->getAllCategories();

		return View::make('category/index', $this->view_data);
	}

    /*
	|--------------------------------------------------------------------------
	| Group
	|--------------------------------------------------------------------------
	| show the locations in the group, as well as up and down navigation in the other gourps
    */
	public function getGroup($id)
	{
        $current_cat = $this->category->findCategory(array('id' => $id));
        if( ! $current_cat) 
        {
            return Redirect::to('category')->with('flash_error', Lang::get('category.category_not_found') );    
        }

        //TODO:  get the page size from a config file
        $pageSize = 15;
        
        $this->view_data['pageTitle'] = $current_cat->title;
        $this->view_data['category'] = $current_cat;
        $this->view_data['children'] = $current_cat->getSubCategories();
        $this->view_data['ancestors'] = $current_cat->getAncestorCategories();
        $this->view_data['locations'] = $current_cat->getLocations($pageSize);
        $this->view_data['project'] = $this->project->getActiveproject($this->user->id);

		return View::make('category/group', $this->view_data)
            ->nest('catchildren', 'category._catchildren', $this->view_data)
            ->nest('catparents', 'category._catparents')
            ->nest('catbreadcrumb', 'category._catbreadcrumb', $this->view_data);

	}

}