<?php

class ProjectController extends BaseController {


    public function __construct(IProjectRepository $project,
                                ILocationRepository $location)
    {
        $this->project = $project;
        $this->location = $location;
        $this->user = ( Auth::user()->acting_as ) ? Auth::user()->acting_as : Auth::user();

        $this->view_data['pageTitle'] = NULL; //default
        $this->beforeFilter('csrf', array('on' => 'post'));
        
        /*
        $this->beforeFilter('auth');
        $this->afterFilter('log', array('only' => array('fooAction', 'barAction')));
        */
    }

	/**
	 * Activate a given project for the current user
	 *
	 * @return Response
	 */
	public function getActivate( $project_id, $location_id = NULL )
	{
        //Activate the project for the current user
        $project = $this->project->getProject($this->user, $project_id);
        if($project) { $project->activateProject(); }

        if($location_id)
        {
            $location = $this->location->findLocation(array('id'=>$location_id));
            $project->addLocation($location);
            Session::forget('location_to_add');
        }

        //The activateProject filter should have setup a session variable
        $redir_url = Session::get('redir_url', '/');
        Session::forget('redir_url');
        return Redirect::to($redir_url)->with('flash_info', Lang::get('project.project_activated') );
	}

	/**
	 * Show the new project form
	 *
	 * @return Response
	 */
	public function getCreate()
	{
        $project_user = $this->user;
        $project_user->customer_title = $project_user->title;
        $project_user->title = "";

        $this->view_data['pageTitle'] = "New Project";
        $this->view_data['project'] = $project_user;
        return View::make('project/create', $this->view_data);
	}

	/**
	 * Processs the new project form
	 *
	 * @return Response
	 */
	public function postCreate()
	{
        $input = Input::all();
        $rules = array(
            'title' => array('required'),
            'start_date' => array('required', 'date'),
            'end_date' => array('required', 'date'),
            'email' => array('required', 'email'),
            'first' => array('required'),
            'last' => array('required'),
            'phone' => array('required')
        );
        //TODO: move these errors to a language file
        $messages = array(
            'required' => ' is required.',
            'date' => ' must be a date.',
        );
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes())
        {
            $project = $this->project->createProject($this->user, $input);
            if ( $project )
            {
                $project->activateProject($project->id);

                //add any locations the customer might have been browsing
                if(Session::has('location_to_add'))
                {
                    $location_id = Session::get('location_to_add');
                    $location = $this->location->findLocation(array('id'=>$location_id));
                    $project->addLocation($location);
                    Session::forget('location_to_add');
                    //Send them back where they were
                    $redir_url = Session::get('redir_url', '/');
                    Session::forget('redir_url');
                    return Redirect::to($redir_url)->with('flash_success', Lang::get('location.location_added_success') );
                }


                return Redirect::to('project/edit/' . $project->id)->with('flash_success', Lang::get('project.success_create_project'));
            }
            else
            {
                //TODO: Why didn't the valid account information change the password?    
                return Redirect::to('project/create')->with('flash_error', Lang::get('projectg.error_create_project'));
            }
        }
        else
        {
            return Redirect::to('project/create')
                            ->withInput()
                            ->withErrors($validator)
                            ->with('flash_error', Lang::get('project.error_create_project'));  
        }

	}

	/**
	 * Show the project edit form
     *
	 * @return Response
	 */
	public function getEdit($project_id)
	{
        $project = $this->project->getProject($this->user, $project_id);
        if ( $project )
        {
            $this->view_data['pageTitle'] = "Edit Project: " . $project->title;
            $this->view_data['project'] = $project;
            return View::make('project/edit', $this->view_data);
        }
        return Redirect::to('account')->with('flash_error', Lang::get('project.invalid_project'));
	}


	/**
	 * Process the project edit form
	 *
	 * @return Response
	 */
	public function postEdit($project_id)
	{
        $input = Input::all();
        $rules = array(
            'title' => array('required'),
            'start_date' => array('required', 'date'),
            'end_date' => array('required', 'date'),
            'email' => array('required', 'email'),
            'first' => array('required'),
            'last' => array('required'),
            'phone' => array('required')
        );
        //TODO: move these errors to a language file
        $messages = array(
            'required' => ' is required.',
            'date' => ' must be a date.',
        );
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes())
        {
            $project = Project::where('id', '=',$project_id)->first();
            $attempt = $project->editProject($this->user, $input);
            if ( $attempt )
            {
                return Redirect::to('project/edit/' . $project_id)->with('flash_success', Lang::get('project.success_edit_project'));
            }
            else
            {
                $messages = ( count($project->errors[0]) > 0 ) ? $project->errors[0] : Lang::get('project.error_edit_project');
                //TODO: Why didn't the valid account information change the password?    
                return Redirect::to('project/edit/' . $project_id)->with('flash_error', $messages);
            }
        }
        else
        {
            return Redirect::to('project/edit/' . $project_id)
                            ->withInput()
                            ->withErrors($validator)
                            ->with('flash_error', Lang::get('project.error_edit_project'));  
        }
	}

	/**
	 * Show all the available projects to activate, plus an option to create a 
     *   new project if needed.
	 *
	 * @return Response
	 */
	public function getSelect()
	{
        $this->view_data['pageTitle'] = "Select a Project";
        $this->view_data['projects'] = $this->project->getUserProjects($this->user, array('status'=>'active'));
        $this->view_data['locationId'] = Session::get('location_to_add', NULL);

        return View::make('project/select', $this->view_data);
	}

	/**
	 * Submit the project
     *
	 * @return Response
	 */
	public function getSubmit($project_id)
	{
        $project = $this->project->getProject($this->user, $project_id);
        if ( $project )
        {
            $project->submitProject();

            //The activateProject filter should have setup a session variable
            $redir_url = Session::get('redir_url', '/');
            Session::forget('redir_url');
            if( count($project->errors) > 0 )
            {
                return Redirect::to($redir_url)->with('flash_error', $project->errors );
            }
            else
            {             
                return Redirect::to('account/projects/submitted')->with('flash_success', Lang::get('project.success_project_submitted') );
            }

        }
        return Redirect::to('account')->with('flash_error', Lang::get('project.invalid_project'));
	}

	/**
	 * View the project and the locations in it
     *
	 * @return Response
	 */
	public function getView($project_id)
	{
        $project = $this->project->getProject($this->user, $project_id);
        if ( $project )
        {
            $this->view_data['pageTitle'] = $project->title;
            $this->view_data['project'] = $project;
            $this->view_data['user'] = $this->user;
            $this->view_data['locations'] = $project->getLocations();
            return View::make('project/view', $this->view_data);
        }
        return Redirect::to('account')->with('flash_error', Lang::get('project.invalid_project'));
	}
}