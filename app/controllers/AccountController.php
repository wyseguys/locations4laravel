<?php

class AccountController extends BaseController {

    /*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	|
    */
    public function __construct(
        IAccountRepository $account, 
        IPasswordQuestion $password_question,
        IPasswordReminder $password_reminder,
        IProjectRepository $project)
    {
        $this->account = $account;
        $this->password_question = $password_question;
        $this->password_reminder = $password_reminder;
        $this->project = $project;

        $this->view_data['pageTitle'] = NULL; //default value in case I forget it in an action

        if (Auth::check())
        {
            $this->user = ( Auth::user()->acting_as ) ? Auth::user()->acting_as : Auth::user();
        }
        else
        {
            $this->user = NULL;
        }
            
        $this->beforeFilter('csrf', array('on' => 'post'));
        /*
        $this->beforeFilter('auth');
        $this->afterFilter('log', array('only' => array('fooAction', 'barAction')));
        */
    }

	/**
	 * Show the home page
	 *
	 * @return Response
	 */
	public function getIndex()
	{
        $this->user->getActiveproject();
        $this->view_data['pageTitle'] = 'Locations Dashboard';
        $this->view_data['user'] = $this->user;
        //$this->user->activeProject = NULL;

        return View::make('account/index', $this->view_data);
	}

	/**
	 * Show the account challenge question change form for a logged in user
	 *
	 * @return Response
	 */
	public function getChallenge()
	{
        $current_question = $this->user->challenge_question;

        $questions = $this->password_question->getCurrentQuestions(array('current_question'=>$current_question));

        $this->view_data['pageTitle'] = 'Account Security Question';
        $this->view_data['questions'] = $questions;
        $this->view_data['current_question'] = $current_question;

        return View::make('account/challenge', $this->view_data);
	}

	/**
	 * Process the account challenge question change form for a logged in user
	 *
	 * @return Response
	 */
	public function postChallenge()
	{
        $input = Input::all();
        $rules = array(
            'challenge_question' => array('required'),
            'challenge_answer' => array('required')
        );
        //TODO: move these errors to a language file
        $messages = array(
            'required' => ' is required.',
        );
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes())
        {
            $this->user->activeProject = NULL; //TODO:  why the hell do I have to clear this attribute
            $user = $this->user->editChallenge($input);
            if ( $user )
            {
                //TODO: send a success message to the flash
                return Redirect::to('account')->with('flash_success', Lang::get('account.success_edit_challenge'));
            }
            else
            {
                //TODO: Why didn't the valid account information change the password?    
                return Redirect::to('account/challenge')->with('flash_error', Lang::get('account.error_edit_challenge'));
            }
        }
        else
        {
            return Redirect::to('account/challenge')
                            ->withInput()
                            ->withErrors($validator)
                            ->with('flash_error', Lang::get('account.error_edit_challenge'));  
        }
	}


	/**
	 * Show the account edit form for a logged in user
	 *
	 * @return Response
	 */
	public function getEdit()
	{
        $this->view_data['pageTitle'] = 'Edit Account';
        $this->view_data['user'] = $this->user;
        return View::make('account/edit', $this->view_data);
	}


	/**
	 * Process the account edit attempt for a logged in user
	 *
	 * @return Response
	 */
	public function postEdit()
	{
        $input = Input::all();
        $rules = array(
            'first' => array('required'),
            'last' => array('required')
        );
        //TODO: move these errors to a language file
        $messages = array(
            'required' => ' required.',
        );
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes())
        {
            $user = $this->user->editUser($input);
            if ( $user )
            {
                return Redirect::to('account')->with('flash_success', Lang::get('account.success_edit_account'));
            }
            else
            {
                //TODO: Why didn't the valid account information change the password?    
                return Redirect::to('account/edit')->with('flash_error', Lang::get('account.error_edit_account'));
            }
        }
        else
        {
            return Redirect::to('account/edit')
                            ->withInput()
                            ->withErrors($validator)
                            ->with('flash_error', Lang::get('account.error_edit_account'));  
        }

	}

	/**
	 * Show the login form
	 *
	 * @return Response
	 */
	public function getlogin()
	{
        if ( Auth::check() )
        {
            return Redirect::to('account');
        }
        $this->view_data['pageTitle'] = '';
        return View::make('account/login', $this->view_data);
	}


	/**
	 * Process the login attempt
	 *
	 * @return Response
	 */
	public function postlogin()
	{
        $input = Input::only('email', 'password');
        if ($this->account->login($input['email'], $input['password']))
        {
            return Redirect::intended('account')->with('flash_success', Lang::get('account.success_logging_in'));
        }
        else
        {
            return Redirect::to('login')->with('flash_error', Lang::get('account.error_logging_in'));
        }
	}


	/**
	 * Logout and send them somewhere for a logged in user
	 *
	 * @return Response
	 */
	public function getlogout()
	{
        $this->account->logout();
        return Redirect::to('login')->with('flash_warning', Lang::get('account.success_logging_out'));
	}

	/**
	 * Show the password change form for a logged in user
	 *
	 * @return Response
	 */
	public function getPassword()
	{
        $this->view_data['pageTitle'] = 'Password Change';
        return View::make('account/password', $this->view_data);
	}


	/**
	 * Process the password change attempt for a logged in user
	 *
	 * @return Response
	 */
	public function postPassword()
	{
        $input = Input::all();
        $rules = array(
            'currentpassword' => array('required'),
            'password' => array('required', 'min:5', 'same:password_confirm')
        );
        //TODO: move these errors to a language file
        $messages = array(
            'required' => ' is required.',
        );
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes())
        {
            $user = $this->user->changePassword($input);
            if ( $user )
            {
                return Redirect::to('account')->with('flash_success', Lang::get('account.success_password_change'));
            }
            else
            {
                //TODO: Why didn't the valid account information change the password?    
                return Redirect::to('account/password')->with('flash_error', Lang::get('account.error_password_change'));
            }
        }
        else
        {
            return Redirect::to('account/password')
                            ->withInput()
                            ->withErrors($validator)
                            ->with('flash_error', Lang::get('account.error_password_change'));  
        }

	}

	/**
	 * Show the projects listings
	 *
	 * @return Response
	 */
	public function getProjects( $status = "active" )
	{
        //TODO:  Make a configuration item for the default project status
        $this->view_data['current_status'] = $status;

        $options = array();
        $options['status'] = $status;

        $this->project->updateActiveProjectStatus($this->user);
        $projects = $this->project->getUserProjects($this->user, $options);
        $status = $this->project->getUserProjectStatus($this->user);

        $this->view_data['pageTitle'] = 'Projects';
        $this->view_data['projects'] = $projects;
        $this->view_data['status'] = $status;
        
        return View::make('account/projects', $this->view_data);
	}



	/**
	 * Show the registration form
	 *
	 * @return Response
	 */
	public function getRegister()
	{
        $this->view_data['pageTitle'] = 'Register A New Account';
        return View::make('account/register', $this->view_data);
	}


	/**
	 * Process the registration attempt
	 *
	 * @return Response
	 */
	public function postRegister()
	{
        $input = Input::all();
        $rules = array(
            'first'     => array('required', 'min:1'),
            'last'     => array('required', 'min:1'),
            'email'    => array('required', 'email', 'unique:users,email'),
            'password' => array('required', 'min:5', 'same:password_confirm')
        );
        //TODO: move these errors to a language file
        $messages = array(
            'required' => ' is required.',
            'email' => ' must be an email',
        );
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes())
        {
            $newUser = $this->account->createUser($input);
            if ( $newUser )
            {
                $this->account->loginUser($newUser);
                return Redirect::to('account')->with('flash_success', Lang::get('account.success_creating_account'));
            }
            else
            {
                //TODO: Why didn't the valid account information become an account?    
                return View::make('account/register')
                            ->with('flash_error', Lang::get('account.error_creating_account'));
            }
        }
        else
        {
            return Redirect::to('account/register')
                            ->withInput()
                            ->withErrors($validator)
                            ->with('flash_error', Lang::get('account.error_registration'));
        }

	}

	/**
	 * Show the lost password reminder form
	 *
	 * @return Response
	 */
	public function getLostpassword()
	{
        $this->view_data['pageTitle'] = 'Lost Password Help';
        return View::make('account/lostpassword', $this->view_data);
	}


	/**
	 * Process the lost password reminder attempt
	 *
	 * @return Response
	 */
	public function postLostpassword()
	{
        $input = Input::all();
        $rules = array(
            'email'    => array('required', 'email')
        );
        //TODO: move these errors to a language file
        $messages = array(
            'required' => ' is required.',
            'email' => ' must be an email',
        );
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes())
        {
            if ( $this->account->findUser( array('email' => $input['email']) ) )
            {
                return Redirect::to('account/reminder/' . $input['email'] );
            }
            else
            {
                return Redirect::to('account/lostpassword')->with('flash_error', Lang::get('account.error_found_account'))->with('error', 'hello world')->with('reason', 'reason');
            }
                
        }
        else
        {
            return Redirect::to('account/lostpassword')->withInput()->withErrors($validator);  
        }
	}


	/**
	 * Show the success page for a password reminder
	 *
	 * @return Response
	 */
	public function getReminded()
	{
        $this->view_data['pageTitle'] = 'Password Reminder';
        return View::make('account/reminded', $this->view_data);
	}

	/**
	 * Show the challenge portion of password reminder form
	 *
	 * @return Response
	 */
	public function getReminder($email)
	{
        $user = $this->account->findUser(array('email'=>$email));
        if ( ! $user)
        {
            return Redirect::to('account/lostpassword')->with('flash_error', Lang::get('account.error_found_account'));
        }
        $current_question = $user->challenge_question;

        $questions = $this->password_question->getCurrentQuestions(array('current_question'=>$current_question));

        $this->view_data['pageTitle'] = 'Password Reset Challenge';
        $this->view_data['questions'] = $questions;
        $this->view_data['current_question'] = $current_question;
        $this->view_data['email'] = $email;

        return View::make('account/reminder', $this->view_data);
	}

	/**
	 * Show the challenge portion of password reminder form
	 *
	 * @return Response
	 */
	public function postReminder($email)
	{

        $input = Input::all();
        $rules = array(
            'email'    => array('required', 'email'),
            'challenge_answer'    => array('required')
        );
        //TODO: move these errors to a language file
        $messages = array(
            'required' => ' is required.',
            'email' => ' must be an email',
        );
        $validator = Validator::make($input, $rules, $messages);

        if ($validator->passes())
        {
            $user = $this->account->findUser(array('email' => $input['email']));
            if ( $user->confirmChallenge($input['email'], $input['challenge_answer']) )
            {
                $options = array('email' => $input['email']);
                $attempt = $this->account->passwordReminder($options, URL::route('account/reminded'));
                return $attempt;
            }
            else
            {
                return Redirect::to('account/reminder/' . $email)->with('flash_error', Lang::get('account.error_challenge_answer'));
            }
        }
        else
        {
            return Redirect::to('account/reminder/' . $email)->withInput()->withErrors($validator);  
        }
	}

    

	/**
	 * Show the password reset form
	 *
	 * @return Response
	 */
	public function getReset($token)
	{
        $email = $this->password_reminder->getEmail($token);

        $possible_user = $this->account->findUser(array('email'=>$email));
        $current_question = ($possible_user) ? $possible_user->challenge_question : "";
        
        $this->view_data['pageTitle'] = 'Password Reset Challenge';
        $this->view_data['token'] = $token;
        $this->view_data['current_question'] = $current_question;
        
        return View::make('account.reset', $this->view_data);
	}
    
	/**
	 * Process the password reset form
	 *
	 * @return Response
	 */
	public function postReset($token)
	{
        $challenge_answer = Input::get('challenge_answer');
        $credentials = array('email' => Input::get('email'));
        //TODO make this an account function so I can require a challenge question and answer
        return Password::reset($credentials, function($user, $password)
        {
            $user->password = Hash::make($password);
            $user->save();
            return Redirect::to('account')->with('flash_success', Lang::get('account.success_password_change'));                
        });
	}
    

}