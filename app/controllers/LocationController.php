<?php

class LocationController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Location Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    /*
	|--------------------------------------------------------------------------
	| 
	|--------------------------------------------------------------------------
	|
    */
    public function __construct(ICategoryRepository $category, 
                                ILocationRepository $location,
                                IProjectRepository $project)
    {
        
        $this->category = $category;
        $this->location = $location;
        $this->project = $project;
        $this->user = ( Auth::user()->acting_as ) ? Auth::user()->acting_as : Auth::user();

        $this->view_data['pageTitle'] = NULL; //default
        /*
        $this->beforeFilter('auth');
        $this->beforeFilter('csrf', array('on' => 'post'));
        $this->afterFilter('log', array('only' => array('fooAction', 'barAction')));
        */
    }


    /*
	|--------------------------------------------------------------------------
	| index
	|--------------------------------------------------------------------------
	| show a listing of all the locations available
    */
	public function getIndex()
	{
        //TODO:  config this setting
        $pageSize = 15;

        $this->view_data['pageTitle'] = Lang::get('location.page_locations');
        $this->view_data['project'] = $this->project->getActiveproject($this->user->id);
        $this->view_data['locations'] = $this->location->getAllLocations($pageSize);

		return View::make('location/index', $this->view_data);
	}

    /*
	|--------------------------------------------------------------------------
	| Listing
	|--------------------------------------------------------------------------
	| show the location details
    */
	public function getListing($id)
	{
        $current_location = $this->location->findLocation(array('id' => $id));
        if( ! $current_location) 
        {
            return Redirect::to('location')->with('flash_error', Lang::get('location.location_not_found') );    
        }

        $this->view_data['location'] = $current_location;
        $this->view_data['pageTitle'] = $current_location->name;

		return View::make('location/listing', $this->view_data);
	}


    /*
	|--------------------------------------------------------------------------
	| getAddlocation
	|--------------------------------------------------------------------------
	| Add the location to the current project
    */
	public function getAddlocation($location_id)
    {
        //Is there a current active project?
        $project = $this->project->getActiveproject($this->user->id);

        if ( ! $project )
        {
            Session::put('location_to_add', $location_id);
            $redir_url = 'project/select';            
            return Redirect::to($redir_url)->with('flash_info', Lang::get('location.location_added_no_project') );
        }

        //Is it a valid location ID
        $location = $this->location->findLocation(array('id' => $location_id) );

        //Did it add?
        $result = $project->addLocation($location);

        //Send them back where they were
        $redir_url = Session::get('redir_url', '/');
        Session::forget('redir_url');
        return Redirect::to($redir_url)->with('flash_success', Lang::get('location.location_added_success') );

    }

    /*
	|--------------------------------------------------------------------------
	| getRemovelocation
	|--------------------------------------------------------------------------
	| Add the location from the current project
    */
	public function getRemovelocation($location_id)
    {
        //Is there a current active project?
        $project = $this->project->getActiveproject($this->user->id);

        //Is it a valid location ID
        $location = $this->location->findLocation(array('id' => $location_id) );

        //Did it remove?
        $result = $project->removeLocation($location);

        //Send them back where they were
        $redir_url = Session::get('redir_url', '/');
        Session::forget('redir_url');
        return Redirect::to($redir_url)->with('flash_success', Lang::get('location.location_removed_success') );

    }

}