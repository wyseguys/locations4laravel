<?php
/*
*   Testing that rounte exist, filter the correct input, and redirect by default as expected
*
*/
use Zizaco\FactoryMuff\Facade\FactoryMuff;

class RoutesPublicTest extends TestCase {
    

    public function setUp()
    {
        parent::setUp();
        $user = new User(array('email' => 'admin@example.com'));
        //$this->be($user);
    }

    public function testRouteAccountRegister()
    {
        $this->call('GET', 'account/register');
        $this->assertResponseOk();
    }

    public function testRouteAccountLostPassword()
    {       
        $this->call('GET', 'account/lostpassword');
        $this->assertResponseOk();
    }

    public function testRouteAccountReminderBadInput()
    {
        $this->call('GET', 'account/reminder/TEST_EMAIL_TEST');
        $this->assertRedirectedTo('account/lostpassword');
    }

    public function testRouteAccountReminderGoodInput()
    {
        $user = FactoryMuff::create('User');
        $this->call('GET', 'account/reminder/' . $user->email);
        $this->assertResponseOk();
    }

    public function testRouteAccountReminded()
    {       
        $this->call('GET', 'account/reminded');
        $this->assertResponseOk();
    }

    public function testRouteAccountReset()
    {       
        $this->call('GET', 'account/reset/TEST_FAKE_TOKEN_TEST');
        $this->assertResponseOk();
    }

    public function testRouteLogin()
    {       
        $this->call('GET', 'login');
        $this->assertResponseOk();
    }
/*
    public function testRouteQuicklink()
    {       
        //$this->call('GET', 'quicklink/TEST_PROJECT_KEY_TEST');
        //$this->assertResponseOk();
    }

    public function testRouteQuicklinkLocatiopn()
    {       

        //$this->call('GET', 'quicklink/location/TEST_ID_TEST/TEST_PROJECT_KEY_TEST');
        //$this->assertResponseOk(); 
    }



    public function testRoute()
    {
        $this->call('GET', '');
        $this->assertResponseOk();
        $this->assertResponseStatus(404);
        $this->assertRedirectedTo('account/actas');
    }
*/
}