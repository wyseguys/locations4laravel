<?php
/*
*   Testing that rounte exist, filter the correct input, and redirect by default as expected
*
*/
use Zizaco\FactoryMuff\Facade\FactoryMuff;

class RoutesAdminTest extends TestCase {
        
    public function setUp()
    {
        parent::setUp();
        $user = new User(array('email' => 'admin@example.com', 'privilege' => 100));
        $this->be($user);
    }

    public function testRouteAdminIndex()
    {
        $this->call('GET', 'admin');
        $this->assertResponseOk();
    }

    public function testRouteAdminActAs()
    {
        $this->call('GET', 'admin/actas');
        $this->assertResponseOk();
    }

    public function testRouteAdminActAsUser()
    {
        //Arrange
        $admin = FactoryMuff::create('User');
        $admin->privilege = 100;
        $admin->save();
        $this->be($admin);

        $user = FactoryMuff::create('User');

        //Act
        $this->call('GET', 'admin/actas/' . $user->id);

        //Assert
        $this->assertRedirectedTo('admin/actas');
    }

}