<?php
/*
*   Testing that rounte exist, filter the correct input, and redirect by default as expected
*
*/

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class RoutesProjectsLoggedInTest extends TestCase {

    
    public function setUp()
    {
        parent::setUp();
        $user = new User(array('email' => 'admin@example.com'));
        $this->be($user);
    }

    public function testRouteProject()
    {
        $this->call('GET', 'project');
        $this->assertResponseOk();
    }

    public function testRouteProjectCreate()
    {
        $this->call('GET', 'project/create');
        $this->assertResponseOk();
    }

    public function testRouteProjectActivateGood()
    {
        $project = FactoryMuff::create('Project');
        $this->call('GET', 'project/activate/' . $project->id);
        //TODO:: look for success message
        $this->assertRedirectedTo('/');
    }

    public function testRouteProjectActivateBad()
    {
        $user = FactoryMuff::create('User');
        $this->be($user);
        $this->call('GET', 'project/activate/404');
        //TODO:: look for error message
        $this->assertRedirectedTo('/'); //it redirects to a value stored in the session, which I am not testing in teh route
    }

    public function testRouteProjectEditGood()
    {
        $user = FactoryMuff::create('User');
        $start = new DateTime();
        $end = new DateTime();
        $end = $end->add(new DateInterval('P10D'));
        $fields = array('title'=>'title', 
                        'start_date'=>$start->format('Y-m-d'), 
                        'end_date'=>$end->format('Y-m-d'), 
                        'description'=>'description',
                        'first'=>$user->first,
                        'last'=>$user->last,
                        'email'=>$user->emails,
                        'active' => FALSE);
        $project = Project::createProject($user, $fields);
        $this->be($user);

        $this->call('GET', 'project/edit/' . $project->id);
        //TODO:: look for success message
        $this->assertResponseOk();
    }

    public function testRouteProjectEditBad()
    {
        //TODO:: look for error message
        $this->call('GET', 'project/edit/404');
        $this->assertRedirectedTo('account');
    }

    public function testRouteProjectSubmitGood()
    {
        //TODO:: look for success message
        $this->call('GET', 'project/submit/1');
        $this->assertRedirectedTo('account');
    }

    public function testRouteProjectSubmitBad()
    {
        //TODO:: look for error message
        $this->call('GET', 'project/submit/404');
        $this->assertRedirectedTo('account');
    }

/*
    public function testRoute()
    {
        $this->call('GET', '');
        $this->assertResponseOk();
        $this->assertResponseStatus(404);
        $this->assertRedirectedTo('account/actas');
    }
*/
}