<?php
/*
*   Testing that rounte exist, filter the correct input, and redirect by default as expected
*
*/

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class RoutesLocationsLoggedInTest extends TestCase {

    
    public function setUp()
    {
        parent::setUp();
        $user = new User(array('email' => 'admin@example.com'));
        $this->be($user);
    }

    public function testRouteCategories()
    {
        $this->call('GET', 'category');
        $this->assertResponseOk();

        //$this->assertRedirectedTo('/');
    }


    public function testRouteCategoryString()
    {
        $this->call('GET', 'category/1/testcatname');
        $this->assertResponseOk();
    }

    /*
    public function testRouteCategoryAlphaNumeric()
    {
        $this->call('GET', 'category/1/testcatname123');
        $this->assertResponseOk();
    }

    public function testRouteCategoryAlphaNumericHyphens()
    {
        $this->call('GET', 'category/1/test-cat-name');
        $this->assertResponseOk();
    }

    public function testRouteCategoryWithoutSlug()
    {
        $this->call('GET', 'category/1');
        $this->assertResponseOk();
    }
    */


/*
    public function testRoute()
    {
        $this->call('GET', '');
        $this->assertResponseOk();
        $this->assertResponseStatus(404);
        $this->assertRedirectedTo('account/actas');
    }
*/
}