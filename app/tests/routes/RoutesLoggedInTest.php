<?php
/*
*   Testing that rounte exist, filter the correct input, and redirect by default as expected
*
*/
class RoutesLoggedInTest extends TestCase {

    
    public function setUp()
    {
        parent::setUp();
        $user = new User(array('email' => 'admin@example.com', 'privilege' => 1));
        $this->be($user);
    }

    public function testRouteLogout()
    {
        $this->call('GET', 'logout');
        $this->assertRedirectedTo('login');
    }

    public function testRouteAccountHome()
    {
        $this->call('GET', 'account');
        $this->assertResponseOk();
    }

    public function testRouteAccountEdit()
    {
        $this->call('GET', 'account/edit');
        $this->assertResponseOk();
    }

    public function testRouteAccountPasswordChange()
    {
        $this->call('GET', 'account/password');
        $this->assertResponseOk();
    }

    public function testRouteAccountChallengeChange()
    {
        $this->call('GET', 'account/challenge');
        $this->assertResponseOk();
    }

    public function testRouteAccountProjectsDefault()
    {
        $this->call('GET', 'account/projects');
        $this->assertResponseOk();
    }

/*
    public function testRoute()
    {
        $this->call('GET', '');
        $this->assertResponseOk();
        $this->assertResponseStatus(404);
        $this->assertRedirectedTo('account/actas');
    }
*/
}