<?php

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class CategoryModelTest extends TestCase {

    public $fields;
    public $user;

    public function setUp()
    {
        parent::setUp();       
    }

	public function testCanCreateCategory()
	{ 
		//Arrange
        
        //Act
        $category = FactoryMuff::create('Category');

        //Assert
        $this->assertNotNull($category);
	}

	public function testCanGetOnlyParentCategories()
	{ 
		//Arrange
        $parent1 = FactoryMuff::create('Category');
        $parent2 = FactoryMuff::create('Category');
        $parent3 = FactoryMuff::create('Category');
        $child1 = FactoryMuff::create('Category');
        $child1->parent_id = $parent1->id;
        $child1->save();

        //Act
        $parents = Category::getAllParentCategories();

        //Assert
        $this->assertEquals(3, count($parents));
	}

    
	public function testCanGetChildrenOfCategory()
	{ 
		//Arrange
        $parent = FactoryMuff::create('Category');
        
        $child1 = FactoryMuff::create('Category');
        $child1->parent_id = $parent->id;
        $child1->save();
        
        $child2 = FactoryMuff::create('Category');
        $child2->parent_id = $parent->id;
        $child2->save();
        
        //Act
        $children = $parent->getSubCategories();

        //Assert
        $this->assertEquals(2, count($children));
	}


	public function testCanGetParentOfChildCategory()
	{ 
		//Arrange
        $parent1 = FactoryMuff::create('Category');
        $parent2 = FactoryMuff::create('Category');
        
        $child = FactoryMuff::create('Category');
        $child->parent_id = $parent1->id;
        $child->save();
        
        //Act
        $catparent = $child->getParentCategory();

        //Assert
        $this->assertEquals($parent1->title, $catparent->title);
	}

	public function testCanGetAllParentsOfChildCategoryInOrder()
	{ 
		//Arrange
        $greatgrand = FactoryMuff::create('Category');
        
        $grand = FactoryMuff::create('Category');
        $grand->parent_id = $greatgrand->id;
        $grand->save();

        $parent = FactoryMuff::create('Category');
        $parent->parent_id = $grand->id;
        $parent->save();
        
        $child = FactoryMuff::create('Category');
        $child->parent_id = $parent->id;
        $child->save();
        
        //Act
        $ancestors = $child->getAncestorCategories();

        //Assert
        $this->assertEquals(4, count($ancestors));
        $this->assertEquals($greatgrand->title, $ancestors[0]->title);
	}

	public function testCanGetCategoryDetails()
	{ 
		//Arrange
        $parent1 = FactoryMuff::create('Category');

        //Act
        $cat = Category::findCategory(array('id' => $parent1->id));

        //Assert
        $this->assertEquals($parent1->title, $cat->title);
	}


	public function testCanAddOneLocationToCategory()
	{ 
		//Arrange
        $category = FactoryMuff::create('Category');
        $location = FactoryMuff::create('Location');
                
        //Act
        $result = $category->addLocation($location);
        
        //Assert
        $this->assertTrue($result);

	}


	public function testCanFetchLocationsInCategory()
	{ 
		//Arrange
        $category = FactoryMuff::create('Category');
        $location1 = FactoryMuff::create('Location');
        $location2 = FactoryMuff::create('Location');
        $location3 = FactoryMuff::create('Location');

        $result = $category->addLocation($location1);
        $result = $category->addLocation($location2);
        $result = $category->addLocation($location3);               
        
        //Act
        $results = $category->getLocations();
        $count = count($results);

        
        //Assert
        $this->assertEquals(3, $count);

	}

	public function testCanFetchOnlyEnabledLocationsInCategory()
	{ 
		//Arrange
        $category = FactoryMuff::create('Category');
        $location1 = FactoryMuff::create('Location');
        $location1->enabled = 0;
        $location1->save();
        $location2 = FactoryMuff::create('Location');
        $location3 = FactoryMuff::create('Location');

        $result = $category->addLocation($location1);
        $result = $category->addLocation($location2);
        $result = $category->addLocation($location3);               
        
        //Act
        $results = $category->getLocations();
        $count = count($results);

        
        //Assert
        $this->assertEquals(2, $count);

	}

	public function testCanFetchOnlyLocationsInCategoryInOrder()
	{ 
		//Arrange
        $category = FactoryMuff::create('Category');
        $location1 = FactoryMuff::create('Location');
        $location2 = FactoryMuff::create('Location');
        $location3 = FactoryMuff::create('Location');
        $location3->priority = 10;
        $location3->save();

        $result = $category->addLocation($location1);
        $result = $category->addLocation($location2);
        $result = $category->addLocation($location3);               
        
        //Act
        $results = $category->getLocations();

        
        //Assert
        $this->assertEquals($location3->id, $results[0]->id);

	}


    /*
	public function testCan()
	{ 
		//Arrange
        
        //Act
        
        //Assert

	}
    */
}