<?php

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class LocationModelTest extends TestCase {

    public function setUp()
    {
        parent::setUp();       
    }

	public function testCanCreateLocation()
	{ 
		//Arrange
        
        //Act
        $location = FactoryMuff::create('Location');

        //Assert
        $this->assertNotNull($location);
	}

	public function testCanFetchAllLocations()
	{ 
		//Arrange
        $location1 = FactoryMuff::create('Location');
        $location2 = FactoryMuff::create('Location');
        $location3 = FactoryMuff::create('Location');
        $location4 = FactoryMuff::create('Location');
        $location5 = FactoryMuff::create('Location');
                
        //Act
        $locations = Location::getAllLocations();

        //Assert
        $this->assertEquals(5, count($locations));
	}

	public function testCanFetchLocationDetails()
	{ 
		//Arrange
        $location1 = FactoryMuff::create('Location');
                
        //Act
        $location = Location::findLocation(array('id' => $location1->id));

        //Assert
        $this->assertEquals($location1->name, $location->name);
	}

	public function testCanFetchLocationCategories()
	{ 
		//Arrange
        $category1 = FactoryMuff::create('Category');
        $category2 = FactoryMuff::create('Category');
        $location1 = FactoryMuff::create('Location');
        
        $category1->addLocation($location1);        
        $category2->addLocation($location1);
                
        //Act
        $categories = $location1->getAllLocationCategories();

        //Assert
        $this->assertEquals(2, count($categories));
	}


    /*
	public function testCan()
	{ 
		//Arrange
        
        //Act
        
        //Assert

	}
    */
}