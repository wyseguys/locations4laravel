<?php

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class UserModelAccountTest extends TestCase {


    public function setUp()
    {
        parent::setUp();

    }

	public function testUserCanLoginSuccess()
	{ 
        //Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password');
        $user = User::createUser($fields);

        //Act
        $logged_in = User::login('test@example.com', 'password');

        //Assert
        $this->assertTrue($logged_in);		
	}

	public function testUserCanLoginFail()
	{ 
        //Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password');
        $user = User::createUser($fields);

        //Act
        $logged_in = User::login('test@example.com', 'BAD_PASSWORD');

        //Assert
        $this->assertFalse($logged_in);		
	}

	public function testUserCanLogout()
	{ 
        //Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password');
        $user = User::createUser($fields);

        //Act
        $logged_in = User::login('test@example.com', 'password');
        $logged_in_user = User::getCurrentUser();
        $logged_in_user->logout();
        $logged_in_user = User::getCurrentUser();

        //Assert
        $this->assertNotInstanceOf('User', $logged_in_user);

	}

	public function testCanDisableUser()
	{ 
        //Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password');
        $user = User::createUser($fields);

        //Act
        $user->disable();
        $logged_in = User::login('test@example.com', 'password');

        //Assert
        $this->assertFalse($logged_in);	
	}


    

    /*
	public function testUserCanLogout()
	{ 
		//Arrange
        
        //Act
        
        //Assert

	}
    */
}