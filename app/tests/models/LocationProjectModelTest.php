<?php

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class LocationProjectModelTest extends TestCase {

    public $fields;

    public function setUp()
    {
        parent::setUp();
    }

	public function testCanCountLocationProject()
	{ 
		//Arrange
        $project = FactoryMuff::create('Project');
        $location1 = FactoryMuff::create('Location');
        $location2 = FactoryMuff::create('Location');
        $location3 = FactoryMuff::create('Location');

        //Act
        $result = $project->addLocation($location1);
        $result = $project->addLocation($location2);
        $result = $project->addLocation($location3);

        $count = $project->projectCount();
        
        //Assert
        $this->assertEquals(3, $count);

	}

	public function testCanConfirmLocationInProject()
	{ 
		//Arrange
        $project = FactoryMuff::create('Project');
        $location1 = FactoryMuff::create('Location');
        $location2 = FactoryMuff::create('Location');
        $location3 = FactoryMuff::create('Location');

        //Act
        $result = $project->addLocation($location1);
        $result = $project->addLocation($location2);
        $result = $project->addLocation($location3);

        $check = $project->hasLocation($location1);

        
        //Assert
        $this->assertTrue($check);

	}

	public function testCanConfirmLocationNotInProject()
	{ 
		//Arrange
        $project = FactoryMuff::create('Project');
        $location1 = FactoryMuff::create('Location');
        $location2 = FactoryMuff::create('Location');
        $location3 = FactoryMuff::create('Location');

        //Act
        $check = $project->hasLocation($location1);

        
        //Assert
        $this->assertFalse($check);

	}

	public function testLocationCanBeAddedToProject()
	{ 
        //Arrange
        $project = FactoryMuff::create('Project');
        $location = FactoryMuff::create('Location');

        //Act
        $result = $project->addLocation($location);
        $project_locations = $project->getLocations();

        //Assert
        $this->assertTrue($result);
        $this->assertEquals($location->sku, $project_locations[0]->sku);
	}

	public function testLocationCanBeRemovedFromProject()
	{ 
        //Arrange
        $project = FactoryMuff::create('Project');
        $location1 = FactoryMuff::create('Location');
        $location2 = FactoryMuff::create('Location');

        //Act
        $result_add = $project->addLocation($location1);
        $result_add = $project->addLocation($location2);
        $result_remove = $project->removeLocation($location1);
        $project_locations = $project->getLocations();

        //Assert
        $this->assertTrue($result_remove);
        $this->assertEquals(1, count($project_locations));
	}


/*

    */


    /*
	public function testCanCreateProject()
	{ 
		//Arrange
        
        //Act
        
        //Assert

	}
    */
}