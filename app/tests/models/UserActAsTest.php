<?php

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class UserModelActAsTest extends TestCase {


    public function setUp()
    {
        parent::setUp();

        //$user1 = FactoryMuff::create('User');

    }

	public function testUserAdminCanActAsAnother()
	{ 
		//Arrange
        $admin = FactoryMuff::create('User');
        $admin->privilege = 100;
        $admin->save();

        $user = FactoryMuff::create('User');
        
        //Act
        $acting_as = $admin->setActAs($user);
        
        //Assert
        $this->assertTrue($acting_as);
        $this->assertNotNull($admin->acting_as);
        $this->assertEquals($user->first, $admin->acting_as->first);
        $this->assertNotEquals($admin->first, $admin->acting_as->first);
	}

	public function testUserCannotActAsAnotherUser()
	{ 
		//Arrange
        $user1 = FactoryMuff::create('User');
        $user2 = FactoryMuff::create('User');
        
        //Act
        try {
            $acting_as = $user1->setActAs($user2);
        }
        catch(Exception $expected) {
            return;
        }

        //Assert
        $this->fail('An exception was not raised');
        
	}

	public function testDisabledUserCannotActAsAnotherUser()
	{ 
		//Arrange
        $user1 = FactoryMuff::create('User');
        $user1->enabled = FALSE;
        $user1->save();
        $user2 = FactoryMuff::create('User');
        
        //Act
        try {
            $acting_as = $user1->setActAs($user2);
        }
        catch(Exception $expected) {
            return;
        }

        //Assert
        $this->fail('An exception was not raised');
        
	}

	public function testUserCannotActAsAnotherDisabledUser()
	{ 
		//Arrange
        $user1 = FactoryMuff::create('User');
        $user1->privilege = 100;
        $user1->save();
        $user2 = FactoryMuff::create('User');
        $user2->enabled = FALSE;
        $user2->save();
        
        //Act
        try {
            $acting_as = $user1->setActAs($user2);
        }
        catch(Exception $expected) {
            return;
        }

        //Assert
        $this->fail('An exception was not raised');
	}

	public function testAdminCannotActAsAnotherHigherAdmin()
	{ 
		//Arrange
        $user1 = FactoryMuff::create('User');
        $user1->privilege = 50;
        $user1->save();
        $user2 = FactoryMuff::create('User');
        $user2->privilege = 100;
        $user2->save();
        
        //Act
        try {
            $acting_as = $user1->setActAs($user2);
        }
        catch(Exception $expected) {
            return;
        }

        //Assert
        $this->fail('An exception was not raised');
	}

    /*
	public function testUserCanLogout()
	{ 
		//Arrange
        
        //Act
        
        //Assert

	}
    */
}