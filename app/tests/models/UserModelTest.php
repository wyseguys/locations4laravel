<?php

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class UserModelTest extends TestCase {


    public function setUp()
    {
        parent::setUp();

    }

	public function testCanCreateUser()
	{ 
		//arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password');
        
        //act
        $new_user = User::createUser($fields);

        //assert
        $this->assertNotNull($new_user);
	}

	public function testCanCheckUserPasswordSuccess()
	{ 
		//Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password');
        $user = User::createUser($fields);

        //Act
        $knows_password = $user->confirmPassword('password');

        //Assert
        $this->assertTrue($knows_password);

	}

	public function testCanCheckUserPasswordFail()
	{ 
		//Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password');
        $user = User::createUser($fields);

        //Act
        $knows_password = $user->confirmPassword('NOT_PASSWORD');

        //Assert
        $this->assertFalse($knows_password);

	}
    
	public function testCanSetUserPassword()
	{ 
		//Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password');
        $user = User::createUser($fields);
        
        //Act
        $user->changePassword( array('currentpassword' => 'password', 'password' => 'new_password') );
        $knows_password = $user->confirmPassword('new_password');

        //Assert
        $this->assertTrue($knows_password);
	}

	public function testCannotChangePasswordWithoutCurrentPassword()
	{ 
		//Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password');
        $user = User::createUser($fields);
        
        //Act
        $user->changePassword( array('currentpassword' => 'unknown', 'password' => 'new_password') );
        $knows_password = $user->confirmPassword('new_password');

        //Assert
        $this->assertFalse($knows_password);
	}

	public function testCanEditUserName()
	{ 
		//Arrange
        $user = FactoryMuff::create('User');
        $fields = array('first'=>'NEW TEST', 'last' => 'NEW TEST');

        //Act
        $user->editUser($fields);
        
        //Assert
        $this->assertEquals($user->first, $fields['first']);
        $this->assertEquals($user->last, $fields['last']);
	}

    public function testCanEditUserAddress()
	{ 
		//Arrange
        $user = FactoryMuff::create('User');
        $fields = array('address_1'=>'NEW TEST', 'address_2' => 'NEW TEST', 'address_3' => 'NEW TEST', 'city' => 'NEW TEST', 'state' => 'NEW TEST', 'zip' => 'NEW TEST', 'country' => 'NEW TEST');

        //Act
        $user->editUser($fields);
        
        //Assert
        $this->assertEquals($user->address_1, $fields['address_1']);
        $this->assertEquals($user->address_2, $fields['address_2']);
        $this->assertEquals($user->address_3, $fields['address_3']);
        $this->assertEquals($user->city, $fields['city']);
        $this->assertEquals($user->state, $fields['state']);
        $this->assertEquals($user->zip, $fields['zip']);
        $this->assertEquals($user->country, $fields['country']);

	}

    public function testCanEditUserContactInfo()
	{ 
		//Arrange
        $user = FactoryMuff::create('User');
        $fields = array('company'=>'NEW TEST', 'title' => 'NEW TEST', 'phone' => 'NEW TEST', 'fax' => 'NEW TEST', 'mobile' => 'NEW TEST', 'web_site' => 'NEW TEST');

        //Act
        $user->editUser($fields);
        
        //Assert
        $this->assertEquals($user->company, $fields['company']);
        $this->assertEquals($user->title, $fields['title']);
        $this->assertEquals($user->phone, $fields['phone']);
        $this->assertEquals($user->fax, $fields['fax']);
        $this->assertEquals($user->mobile, $fields['mobile']);
        $this->assertEquals($user->web_site, $fields['web_site']);
	}
    
	public function testUserCanChangeSecretQuestionAnswer()
	{ 
		//Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password', 'challenge_question'=>'Question', 'challenge_answer'=>'Answer');
        $user = User::createUser($fields);
        
        //Act
        $user->editChallenge(array('challenge_question'=>'New Question', 'challenge_answer'=>'New Answer'));

        //Assert
        $this->assertNotNull($user);
        $this->assertEquals($user->challenge_question, 'New Question');
	}  

	public function testUserCanConfirmSecretQuestionAnswerSuccess()
	{ 
		//Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password', 'challenge_question'=>'Question', 'challenge_answer'=>'Answer');
        $user = User::createUser($fields);
        
        //Act
        $user->editChallenge(array('challenge_question'=>'New Question', 'challenge_answer'=>'New Answer'));
        $challenge_passed = $user->confirmChallenge('test@example.com', 'New Answer');

        //Assert
        $this->assertTrue($challenge_passed);
	}

	public function testUserCanConfirmSecretQuestionAnswerFail()
	{ 
		//Arrange
        $fields = array('first'=>'test', 'last' => 'test', 'email' => 'test@example.com', 'password'=>'password', 'challenge_question'=>'Question', 'challenge_answer'=>'Answer');
        $user = User::createUser($fields);
        
        //Act
        $user->editChallenge(array('challenge_question'=>'New Question', 'challenge_answer'=>'New Answer'));
        $challenge_passed = $user->confirmChallenge('test@example.com', 'Answer');

        //Assert
        $this->assertFalse($challenge_passed);
	}   
     
     
    /*
	public function testUserCanLogout()
	{ 
		//Arrange
        
        //Act
        
        //Assert

	}
    */
}