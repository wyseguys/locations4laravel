<?php

use Zizaco\FactoryMuff\Facade\FactoryMuff;

class ProjectModelTest extends TestCase {

    public $fields;
    public $user;

    public function setUp()
    {
        parent::setUp();
        
        $this->user = FactoryMuff::create('User');
        $start = new DateTime();
        $end = new DateTime();
        $end = $end->add(new DateInterval('P10D'));
        $this->fields = array('title'=>'title', 
                        'start_date'=>$start->format('Y-m-d'), 
                        'end_date'=>$end->format('Y-m-d'), 
                        'description'=>'description',
                        'first'=>$this->user->first,
                        'last'=>$this->user->last,
                        'email'=>$this->user->emails,
                        'active' => FALSE);
        
    }


    //TODO:: I don't know how to test this, I think the test in-memory database is screwing me up.
	/*
    public function testCanChangeActiveProject()
	{ 
		//Arrange
        $fields = $this->fields;
        $user = FactoryMuff::create('User');
        $project1 = Project::createProject($user, $fields);
        $project2 = Project::createProject($user, $fields);
        $project3 = Project::createProject($user, $fields);
        $project4 = Project::createProject($user, $fields);

        //Act
        $project1->activateProject();
        $project1->activateProject();

        $project1->setStatus();
        $project2->setStatus();

        //Assert
        $this->assertEquals($project2->user_id, $user->id);
        $this->assertEquals($project2->user_id, $user->id);

        $this->assertFalse($project1->active);
        $this->assertTrue($project2->active);
	}
    */

	public function testCanCreateProjectSuccess()
	{ 
        //Act
        $fields = $this->fields;
        $project = Project::createProject($this->user, $fields);
                
        //Assert
        $this->assertNotNull($project);
	}

	public function testCannotCreateProjectNoUser()
	{ 
		//Arrange
        $user = NULL;
        $fields = $this->fields;
        
        //Act
        $project = Project::createProject($user, $fields);
                
        //Assert
        $this->assertNull($project);

	}

	public function testCannotCreateProjectNoStart()
	{ 
		//Arrange
        $fields = $this->fields;
        unset($fields['start_date']);
        
        //Act
        $project = Project::createProject($this->user, $fields);
                
        //Assert
       $this->assertNull($project);

	}

	public function testCannotCreateProjectNoEnd()
	{ 
		//Arrange
        $fields = $this->fields;
        unset($fields['end_date']);
        
        //Act
        $project = Project::createProject($this->user, $fields);
                
        //Assert
       $this->assertNull($project);

	}

	public function testCanCreateProjectNoTitle()
	{ 
        //Arrange
        $fields = $this->fields;
        unset($fields['title']);
        
        //Act
        $project = Project::createProject($this->user, $fields);
                
        //Assert
        $this->assertEquals($project->title, Lang::get('project.default_project_title'));
	}

	public function testCanCreateProjectNoDescription()
	{ 
        //Arrange
        $fields = $this->fields;
        unset($fields['description']);
        
        //Act
        $project = Project::createProject($this->user, $fields);
                
        //Assert
        $this->assertEquals($project->description, Lang::get('project.default_project_description'));
	}

	public function testCannotSeeOtherUserProject()
	{ 
		//Arrange
        $fields = $this->fields;
        $known_user = FactoryMuff::create('User');
        $project1 = Project::createProject($known_user, $fields);
        
        $next_user = FactoryMuff::create('User');
        $project2 = Project::createProject($next_user, $fields);
        
        //Act
        $known_user_project = $known_user->projects()->where('id', '=', $project2->id)->first();

        //Assert
        $this->assertNull($known_user_project);
	}

	public function testCanSeeOwnUserProject()
	{ 
		//Arrange
        $fields = $this->fields;
        $known_user = FactoryMuff::create('User');
        $project1 = Project::createProject($known_user, $fields);
        
        $next_user = FactoryMuff::create('User');
        $project2 = Project::createProject($next_user, $fields);
        
        //Act
        $known_user_project = $known_user->projects()->where('id', '=', $project1->id)->first();

        //Assert
        $this->assertNotNull($known_user_project);
	}

	public function testCanSeeManyOwnUserProject()
	{ 
		//Arrange
        $fields = $this->fields;
        $user = FactoryMuff::create('User');
        $project1 = Project::createProject($user, $fields);
        $project2 = Project::createProject($user, $fields);
        $project3 = Project::createProject($user, $fields);
        $project4 = Project::createProject($user, $fields);
        
        //Act
        $known_user_projects = $user->projects()->count();

        //Assert
        $this->assertEquals($known_user_projects, 4);
	}

	public function testProjectsHaveContactInfo()
	{ 
        //Arrange
        $fields = $this->fields;
        $fields['address_1'] = 'address_1';
        $fields['city'] = 'city';
        $fields['state'] = 'state';
        $fields['zip'] = 'zip';
        $fields['state'] = 'state';
        $fields['country'] = 'country';
        $fields['phone'] = 'phone';
        $fields['email'] = 'email@example.com';

        //Act
        $project = Project::createProject($this->user, $fields);
                
        //Assert
        $this->assertEquals($project->address_1, 'address_1');
        $this->assertEquals($project->city, 'city');
        $this->assertEquals($project->state, 'state');
        $this->assertEquals($project->zip, 'zip');
        $this->assertEquals($project->country, 'country');
        $this->assertEquals($project->phone, 'phone');
        $this->assertEquals($project->email, 'email@example.com');
	}

	public function testCanActivateProject()
	{ 
		//Arrange
        $fields = $this->fields;
        $user = FactoryMuff::create('User');
        $project = Project::createProject($user, $fields);
        
        //Act
        $project->activateProject();

        //Assert
        $this->assertTrue($project->active);
	}

	public function testNewProjectsCalculateStatusActive()
	{ 
		//Arrange
        $project = FactoryMuff::create('Project');

        //Act
        $project->setStatus();

        //Assert
        $this->assertEquals('active', $project->status);
	}

	public function testProjectsCannotBeCreatedInPast()
	{ 
        //Arrange
        $user = FactoryMuff::create('User');
        $date   = new DateTime();
        $start  = $date->sub(new DateInterval('P30D'));
        $date   = new DateTime();
        $end    = $date->sub(new DateInterval('P20D'));
        $end = $end->add(new DateInterval('P10D'));
        $fields = array('title'=>'title', 
                        'start_date'=>$start->format('Y-m-d'), 
                        'end_date'=>$end->format('Y-m-d'), 
                        'description'=>'description',
                        'first'=>$user->first,
                        'last'=>$user->last,
                        'email'=>$user->emails,
                        'active' => FALSE);

        //Act
        $project = Project::createProject($user, $fields);
                
        //Assert
        $this->assertNull($project);
	}

	public function testProjectsCalculateStatusExpired()
	{ 
        //Arrange
        $fields = array();

        //Act
        $project = FactoryMuff::create('Project');
        $project->setStatus();
        $this->assertEquals('active', $project->status);

        //Rearrange
        $user = FactoryMuff::create('User');
        $date   = new DateTime();
        $fields['start_date'] = $date->sub(new DateInterval('P30D'))->format('Y-m-d');

        $date   = new DateTime();
        $fields['end_date'] = $date->sub(new DateInterval('P20D'))->format('Y-m-d');

        $project->editProject($user, $fields);
        $project->setStatus();
             
        //Assert
        $this->assertEquals('expired', $project->status);
	}

	public function testProjectsExpiredCannotBeCurrentProject()
	{ 
        //Arrange
        $fields = array();

        //Act
        $project = FactoryMuff::create('Project');
        $project->setStatus();
        $this->assertEquals('active', $project->status);

        //Rearrange
        $user = FactoryMuff::create('User');
        $date   = new DateTime();
        $fields['start_date'] = $date->sub(new DateInterval('P30D'))->format('Y-m-d');

        $date   = new DateTime();
        $fields['end_date'] = $date->sub(new DateInterval('P20D'))->format('Y-m-d');

        $project->editProject($user, $fields);
        $project->setStatus();
             
        //Assert
        $this->assertEquals('expired', $project->status);
        $this->assertFalse($project->active);
	}

	public function testProjectsExpiredCannotBeEdited()
	{ 
        //Arrange
        $fields = array();

        //Act
        $project = FactoryMuff::create('Project');
        $project->setStatus();
        
        //Assert
        $this->assertEquals('active', $project->status);

        //Rearrange
        $user = FactoryMuff::create('User');
        $date   = new DateTime();
        $fields['start_date'] = $date->sub(new DateInterval('P30D'))->format('Y-m-d');

        $date   = new DateTime();
        $fields['end_date'] = $date->sub(new DateInterval('P20D'))->format('Y-m-d');

        //RE-act
        $project->editProject($user, $fields);
        $project->setStatus();
        $this->assertEquals('expired', $project->status);
        $this->assertFalse($project->active);
                     
        //Assert
        $this->assertEquals('expired', $project->status);
        $this->assertFalse($project->active);

        //RE-act
        $project->editProject($user, array('first'=>'NOEDIT'));
                             
        //Assert
        $this->assertNotEquals('NOEDIT', $project->first);

	}


	public function testProjectsCalculateStatusSubmitted()
	{ 
        //Arrange
        $fields = array();

        //Act
        $project = FactoryMuff::create('Project');

        //Rearrange
        $project->activateProject();
        $project->submitProject();
        $project->setStatus();

        //Assert
        $this->assertEquals('submitted', $project->status);
	}

	public function testProjectsCannotSubmitNonActive()
	{ 
        //Arrange
        $fields = array();

        //Act
        $project = FactoryMuff::create('Project');

        //Rearrange
        $project->submitProject();
        $project->setStatus();

        //Assert
        $this->assertContains(Lang::get('errors.project_cannot_submit_not_current'), $project->errors[0]);
	}



/*

    */


    /*
	public function testCanCreateProject()
	{ 
		//Arrange
        
        //Act
        
        //Assert

	}
    */
}