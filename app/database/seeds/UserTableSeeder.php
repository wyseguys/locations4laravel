<?php

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        $admin = new User;
        $admin->password = Hash::make('admin');
        $admin->email = 'admin@example.com';
        $admin->company = 'Developer Co';
        $admin->title = 'Dev';
        $admin->first = 'Admin';
        $admin->last = 'Adminski';
        $admin->address_1 = '123 Any St';
        $admin->address_2 = 'Floor 10';
        $admin->address_3 = 'Suite A';
        $admin->city = 'Portland';
        $admin->state = 'OR';
        $admin->zip = '97204';
        $admin->country = 'US';
        $admin->phone = '555-456-6789';
        $admin->mobile = '555-777-7777';
        $admin->fax = '555-654-6543';
        $admin->web_site = 'http://www.example.com/';
        $admin->address_details = '';
        $admin->save();  

        for($i=0; $i<5; $i++)
        {
            $user = new User;
            $user->password = Hash::make('user' . $i);
            $user->email = 'user'.$i.'@example.com';
            $user->company = 'user'.$i.' Company';
            $user->title = 'User #'.$i;
            $user->first = 'First'.$i;
            $user->last = 'Last'.$i;
            $user->address_1 = 'Address 1-'.$i;
            $user->address_2 = 'Address 2-'.$i;
            $user->address_3 = 'Address 3-'.$i;
            $user->city = 'Portland';
            $user->state = 'OR';
            $user->zip = '97204';
            $user->country = 'US';
            $user->phone = '555-123-1234';
            $user->mobile = '555-555-5555';
            $user->fax = '555-321-3210';
            $user->web_site = 'http://www.example.com/'.$i;
            $user->address_details = '';
            $user->save();           
        }


    }

}