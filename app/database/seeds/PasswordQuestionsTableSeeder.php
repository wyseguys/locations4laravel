<?php

class PasswordQuestionsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('password_questions')->delete();

        PasswordQuestion::create(array('question' => 'What is your name?'));
        PasswordQuestion::create(array('question' => 'Who is your favorite team?'));
        PasswordQuestion::create(array('question' => 'Where is Mars?'));
        PasswordQuestion::create(array('question' => 'What is chocoloate?'));
        PasswordQuestion::create(array('question' => 'Up is down, left is right?'));
        PasswordQuestion::create(array('question' => 'Pickles and peanutbutter sandwich: awesome?'));
        PasswordQuestion::create(array('question' => 'Who are you?  Who-who  Who-who?'));

    }

}