<?php

class CategoryTableSeeder extends Seeder {

    public function run()
    {
        DB::table('categories')->delete();

        //Seed five parent categories each with 5 sub cats
        for($i=0; $i<5; $i++)
        {
            $cat = new Category;
            $cat->parent_id = NULL;
            $cat->title = "Category Title " . $i;
            $cat->description = "Category ".$i." description sentence #".$i;
            $cat->slug = "cat-slug-" .$i;
            $cat->save();   
            
            for($j=0; $j<5; $j++)
            {
                $sub = new Category;
                $sub->parent_id = $cat->id;
                $sub->title = "Sub Category Title of " . $i . " " . $j;
                $sub->description = "Sub category ".$i." description sentence #".$i . " " . $j;
                $sub->slug = "sub-cat-slug-" . $i . "-" . $j;
                $sub->save();           
            }
                    
        }


        
    }
}