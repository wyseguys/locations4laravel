<?php

class ProjectTableSeeder extends Seeder {

    public function run()
    {
        DB::table('projects')->delete();

        //Seed five projects for the #1 seeded user
        for($i=0; $i<5; $i++)
        {
            $project = new Project;
            $project->user_id = 1;
            $project->start_date = strtotime("now");
            $project->end_date = strtotime("+1 week");
            $project->title = "Project Title " . $i;
            $project->description = "Project ".$i." description sentence #".$i;

            $project->email = 'user@example.com';
            $project->company = 'Customer Company';
            $project->customer_title = 'Customer Title';
            $project->first = 'First';
            $project->last = 'Last';
            $project->address_1 = 'Address 1';
            $project->address_2 = '';
            $project->city = 'Portland';
            $project->state = 'OR';
            $project->zip = '97204';
            $project->country = 'US';
            $project->phone = '555-123-1234';
            $project->web_site = 'http://www.example.com/';
                        
            $project->status = 'active';
            $project->enabled = 1;
            $project->active = 0;

            $project->deleted_at = NULL;
            $project->submitted_at = NULL;
            $project->approved_at = NULL;
            $project->cancelled_at = NULL;

            $project->save();           
        }


        //Submitted project
        $i++;
        $project = new Project;
        $project->user_id = 1;
        $project->start_date = strtotime("now");
        $project->end_date = strtotime("+1 week");
        $project->title = "Submitted Title";
        $project->description = "Submitted Project description sentence #".$i;

        $project->email = 'user@example.com';
        $project->company = 'Customer Company';
        $project->customer_title = 'Customer Title';
        $project->first = 'First';
        $project->last = 'Last';
        $project->address_1 = 'Address 1';
        $project->city = 'Portland';
        $project->state = 'OR';
        $project->zip = '97204';
        $project->country = 'US';
        $project->phone = '555-123-1234';
        $project->web_site = 'http://www.example.com/';            
        $project->status = 'submitted';
        $project->enabled = 1;
        $project->active = 0;
        $project->deleted_at = NULL;
        $project->submitted_at = strtotime("now");
        $project->approved_at = NULL;
        $project->cancelled_at = NULL;
        $project->save();      
        
        //Expired project
        $i++;
        $project = new Project;
        $project->user_id = 1;
        $project->start_date = strtotime("-1 week");
        $project->end_date = strtotime("-1 day");
        $project->title = "Expired Title";
        $project->description = "Expired Project description sentence #".$i;

        $project->email = 'user@example.com';
        $project->company = 'Customer Company';
        $project->customer_title = 'Customer Title';
        $project->first = 'First';
        $project->last = 'Last';
        $project->address_1 = 'Address 1';
        $project->city = 'Portland';
        $project->state = 'OR';
        $project->zip = '97204';
        $project->country = 'US';
        $project->phone = '555-123-1234';
        $project->web_site = 'http://www.example.com/';            
        $project->status = 'expired';
        $project->enabled = 1;
        $project->active = 0;
        $project->deleted_at = NULL;
        $project->submitted_at = NULL;
        $project->approved_at = NULL;
        $project->cancelled_at = NULL;
        $project->save();       

        //Approved project
        $i++;
        $project = new Project;
        $project->user_id = 1;
        $project->start_date = strtotime("now");
        $project->end_date = strtotime("+1 week");
        $project->title = "Approved Title";
        $project->description = "Approved Project description sentence #".$i;

        $project->email = 'user@example.com';
        $project->company = 'Customer Company';
        $project->customer_title = 'Customer Title';
        $project->first = 'First';
        $project->last = 'Last';
        $project->address_1 = 'Address 1';
        $project->city = 'Portland';
        $project->state = 'OR';
        $project->zip = '97204';
        $project->country = 'US';
        $project->phone = '555-123-1234';
        $project->web_site = 'http://www.example.com/';            
        $project->status = 'approved';
        $project->enabled = 1;
        $project->active = 0;
        $project->deleted_at = NULL;
        $project->submitted_at = NULL;
        $project->approved_at = strtotime("now");
        $project->cancelled_at = NULL;
        $project->save();   
    }

        //Cancelled project
        $i++;
        $project = new Project;
        $project->user_id = 1;
        $project->start_date = strtotime("now");
        $project->end_date = strtotime("+1 week");
        $project->title = "Cancelled Title";
        $project->description = "Cancelled Project description sentence #".$i;

        $project->email = 'user@example.com';
        $project->company = 'Customer Company';
        $project->customer_title = 'Customer Title';
        $project->first = 'First';
        $project->last = 'Last';
        $project->address_1 = 'Address 1';
        $project->city = 'Portland';
        $project->state = 'OR';
        $project->zip = '97204';
        $project->country = 'US';
        $project->phone = '555-123-1234';
        $project->web_site = 'http://www.example.com/';            
        $project->status = 'cancelled';
        $project->enabled = 1;
        $project->active = 0;
        $project->deleted_at = NULL;
        $project->submitted_at = NULL;
        $project->approved_at = NULL;
        $project->cancelled_at = strtotime("now");;
        $project->save();   
    }
}