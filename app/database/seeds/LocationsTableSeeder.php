<?php

class LocationsTableSeeder extends Seeder {

    public function run()
    {
        DB::table('locations')->delete();

        $cats = Category::all();

        $j = 0;
        foreach ($cats as $cat)
        {
            //Seed five locations per category
            for($i=0; $i<3; $i++)
            {
                $j++;
                $location = new Location;
                $location->sku = "sku" . $j;
                $location->name = "Location Name " . $j;
                $location->slug = "location-title-" . $j;
                $location->description = "loation ".$j." description sentence #".$j;
                $location->enabled = TRUE;
                $location->priority = 100;
                $location->save();   

                $cat->addLocation($location);
            }


        }

        
    }
}