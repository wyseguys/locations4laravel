<?php

use Illuminate\Database\Migrations\Migration;

class Projects extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('projects', function($table)
		{
			$table->increments('id');
            $table->integer('user_id');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('title');
            $table->text('description');

			$table->string('email')->nullable();
            $table->string('company')->nullable();
            $table->string('customer_title')->nullable();
            $table->string('first');
            $table->string('last');
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('fax')->nullable();
            $table->string('web_site')->nullable();
            $table->text('address_details')->nullable();


            $table->softDeletes();            
			$table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('projects');
	}

}