<?php

use Illuminate\Database\Migrations\Migration;

class LocationGroups extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('categories', function($table)
		{
			$table->increments('id');
            $table->integer('parent_id')->nullable();
            $table->string('title');
            $table->string('slug');
            $table->text('description')->nullable();
			$table->boolean('enabled')->default(TRUE);
            $table->integer('priority')->default(100);
            $table->softDeletes();            
			$table->timestamps();
            $table->foreign('parent_id')->references('id')->on('categories');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('categories');
	}

}