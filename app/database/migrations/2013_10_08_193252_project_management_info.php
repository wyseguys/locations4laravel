<?php

use Illuminate\Database\Migrations\Migration;

class ProjectManagementInfo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (Schema::hasTable('projects'))
        {
		    Schema::table('projects', function($table)
		    {
                $table->string('status')->nullable();
                $table->boolean('enabled')->default(1);
                $table->boolean('active')->default(0);
		    });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        if (Schema::hasTable('projects'))
        {
            if (Schema::hasColumn('projects', 'status'))
            {
                Schema::table('projects', function($table)
                {
                   $table->dropColumn('status');
                });
            }


            if (Schema::hasColumn('projects', 'enabled'))
            {
                Schema::table('projects', function($table)
                {
                   $table->dropColumn('enabled');
                });
            }


            if (Schema::hasColumn('projects', 'active'))
            {
                Schema::table('projects', function($table)
                {
                   $table->dropColumn('active');
                });
            }

        }
	}

}