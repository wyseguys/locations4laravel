<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function($table)
		{
			$table->increments('id');
			$table->string('email')->unique();
            $table->string('password');
            $table->string('company')->nullable();
            $table->string('title')->nullable();
            $table->string('first');
            $table->string('last');
            $table->string('address_1')->nullable();
            $table->string('address_2')->nullable();
            $table->string('address_3')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('zip')->nullable();
            $table->string('country')->nullable();
            $table->string('phone')->nullable();
            $table->string('mobile')->nullable();
            $table->string('fax')->nullable();
            $table->string('web_site')->nullable();
            $table->text('address_details')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
        if (Schema::hasTable('users'))
        {
            Schema::drop('users');
        }
	}

}