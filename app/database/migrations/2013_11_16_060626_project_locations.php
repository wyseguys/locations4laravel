<?php

use Illuminate\Database\Migrations\Migration;

class ProjectLocations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('location_project', function($table)
		{
            $table->integer('location_id');
            $table->integer('project_id');
            $table->timestamp('added_on');
            $table->primary(array('location_id', 'project_id'));
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('location_project');
	}

}