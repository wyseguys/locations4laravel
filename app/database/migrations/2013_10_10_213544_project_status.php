<?php

use Illuminate\Database\Migrations\Migration;

class ProjectStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (Schema::hasTable('projects'))
        {
		    Schema::table('projects', function($table)
		    {
                $table->datetime('submitted_at')->nullable();
                $table->datetime('approved_at')->nullable();
                $table->datetime('cancelled_at')->nullable();
		    });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        if (Schema::hasTable('projects'))
        {
            if (Schema::hasColumn('projects', 'submitted_at'))
            {
                Schema::table('projects', function($table)
                {
                   $table->dropColumn('submitted_at');
                });
            }


            if (Schema::hasColumn('projects', 'approved_at'))
            {
                Schema::table('projects', function($table)
                {
                   $table->dropColumn('approved_at');
                });
            }


            if (Schema::hasColumn('projects', 'cancelled_at'))
            {
                Schema::table('projects', function($table)
                {
                   $table->dropColumn('cancelled_at');
                });
            }

        }
	}

}