<?php

use Illuminate\Database\Migrations\Migration;

class Locations extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('locations', function($table)
		{
			$table->increments('id');
            $table->string('sku');
            $table->string('name');
            $table->string('slug');
            $table->text('summary')->nullable();
            $table->text('description')->nullable();
            $table->text('keywords')->nullable();
			$table->integer('gallery_id')->nullable();
            $table->integer('thumbnail_id')->nullable();
            $table->boolean('enabled')->default(TRUE);
            $table->integer('priority')->default(100);
            $table->softDeletes();
			$table->timestamps();
		});

        Schema::create('category_location', function($table)
		{
            $table->integer('category_id');
            $table->integer('location_id');
            $table->primary(array('location_id', 'category_id'));
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('locations');
        Schema::dropIfExists('category_location');
	}

}