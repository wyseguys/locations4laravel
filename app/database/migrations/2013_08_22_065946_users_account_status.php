<?php

use Illuminate\Database\Migrations\Migration;

class UsersAccountStatus extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (Schema::hasTable('users'))
        {
		    Schema::table('users', function($table)
		    {
			    $table->boolean('enabled')->default(TRUE);
		    });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        if (Schema::hasTable('users'))
        {
            if (Schema::hasColumn('users', 'enabled'))
            {
                Schema::table('users', function($table)
                {
                    $table->dropColumn('enabled');
                });
            }
        }
	}

}