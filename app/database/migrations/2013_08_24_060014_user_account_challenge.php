<?php

use Illuminate\Database\Migrations\Migration;

class UserAccountChallenge extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (Schema::hasTable('users'))
        {
		    Schema::table('users', function($table)
		    {
			    $table->string('challenge_question')->nullable();
                $table->string('challenge_answer')->nullable();
		    });
        }

		Schema::create('password_questions', function($table)
		{
			$table->increments('id');
            $table->string('question');
			$table->timestamps();
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        if (Schema::hasTable('users'))
        {
            if (Schema::hasColumn('users', 'challenge_question'))
            {
                Schema::table('users', function($table)
                {
                    $table->dropColumn('challenge_question');
                });
            }
            if (Schema::hasColumn('users', 'challenge_answer'))
            {
                Schema::table('users', function($table)
                {
                    $table->dropColumn('challenge_answer');
                });
            }
        }

        if (Schema::hasTable('password_questions'))
        {
            Schema::drop('password_questions');
        }
	}

}