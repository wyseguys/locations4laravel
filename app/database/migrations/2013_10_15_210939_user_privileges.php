<?php

use Illuminate\Database\Migrations\Migration;

class UserPrivileges extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        if (Schema::hasTable('users'))
        {
		    Schema::table('users', function($table)
		    {
                $table->integer('privilege')->default(1);
		    });
        }
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        if (Schema::hasTable('users'))
        {
            if (Schema::hasColumn('users', 'privilege'))
            {
                Schema::table('users', function($table)
                {
                   $table->dropColumn('privilege');
                });
            }
        }
	}

}