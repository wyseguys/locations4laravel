@if( Auth::check() )
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Locations <b class="caret"></b></a>
    <ul class="dropdown-menu">
        <li class="dropdown-header">Find A Location</li>
        <li>{{ HTML::link('search', 'Search'); }}</li>
        <li>{{ HTML::link('category', 'By Category'); }}</li>
        <li>{{ HTML::link('location', 'Browse All'); }}</li>
    </ul>
</li>
<li class="dropdown">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Projects<b class="caret"></b></a>
    <ul class="dropdown-menu">  
        <li>{{ HTML::link('project/create', Lang::get('project.new_project')); }}</li>
        <li class="dropdown-header">Projects</li>
        <li>{{ HTML::link('account/projects', Lang::get('project.all_project')); }}</li>
        <li>{{ HTML::link('account/projects/expired', Lang::get('project.past_project')); }}</li>
        <li class="divider"></li>
        <li>{{ HTML::link('account', Lang::get('project.current_project')); }}</li>
    </ul>
</li>
@endif