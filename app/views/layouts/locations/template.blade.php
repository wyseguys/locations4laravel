<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{{$pageTitle}}}</title>
        <meta name="description" content="Location Scouting, Photoshoots, video proudction, miami and southern florida">
        <meta name="viewport" content="width=device-width">
        {{ HTML::style('css/bootstrap/bootstrap.css') }}
        {{ HTML::style('css/bootstrap/bootstrap-theme.min.css') }}
        {{ HTML::style('css/location.css') }}
    </head>
    <body>
        @include('layouts.locations._nav')
        @yield('menus')
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!--Flash messages-->
                    <div id="flash-main">
                    @include('layouts.locations._flash')
                    @yield('flash')
                    </div>
                </div>                
            </div>
            <div class="row">
                <div class="col-md-12">
                    @if( isset($pageTitle) )
                    <h1>{{{$pageTitle}}}</h1>
                    @endif
                    @yield('layout')
                </div>
            </div>
            <footer>
                <div class="row">
                    <div class="col-md-4">
                        <ul class="list-unstyled">
                            <li>{{ HTML::link('whydifferent', 'Our Difference'); }}</li>
                            <li>{{ HTML::link('about', 'About Us'); }}</li>
                            <li>{{ HTML::link('client', 'Clients'); }}</li>
                            <li><a href='#'>SEO Site Map</a></li>
                            
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-unstyled">
                            <li>{{ HTML::link('directions', 'Help'); }}</li>
                            <li>{{ HTML::link('weather', 'Miami Weather'); }}</li>
                            <li>{{ HTML::link('featuredlocs', 'Featured Locations'); }}</li>
                            <li>{{ HTML::link('services', 'Production Services'); }}</li>
                            <li>{{ HTML::link('listproperty', 'List Your Property'); }}</li>

                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="list-unstyled">
                            <li>{{ HTML::link('contact', 'Contact'); }}</li>
                            <li><a target="_blank" href='http://www.facebook.com/LocationResourcesMiamiBeach'>Facebook</a></li>
                            <li><a target="_blank" href='#'>Twitter</a></li>
                            <li><a target="_blank" href='#'>Flicker</a></li>
                        </ul>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        {{ HTML::link('tos', 'Terms of Service'); }}
                        &nbsp;&nbsp;|&nbsp;&nbsp;{{ HTML::link('privacy', 'Privacy Policy'); }}
                        &nbsp;&nbsp;|&nbsp;&nbsp;{{ HTML::link('security', 'Security'); }}
                    </div>
                    <div class="col-md-4 pull-right">
                        <p>&copy; Copyright Location Resources - All Rights Reserved</p>
                    </div>
                </div>
   
            </div>
            </footer>
        </div>
        <?php
        /*
        <script>
            var _gaq=[['_setAccount','UA-XXXXX-X'],['_trackPageview']];
            (function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
            g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
            s.parentNode.insertBefore(g,s)}(document,'script'));
        </script>
        */
        ?>
        {{ HTML::script('js/jquery/jquery-1.9.1.min.js') }}
        {{ HTML::script('js/bootstrap/bootstrap.min.js') }}
        {{ HTML::script('js/location.js') }}
    </body>
</html>