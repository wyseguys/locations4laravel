@if(Auth::check())
    <?php if (Auth::user()->privilege > 50) { ?>
    <li class="dropdown">              
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Lang::get('admin.admin_area') }}<b class="caret"></b></a>
        <ul class="dropdown-menu">  
            <li>{{ HTML::link('admin', Lang::get('admin.admin_home')); }}</li>        
            <?php 
            if (Auth::user()->privilege > 50)
            { 
                ?>
                <li>{{ HTML::link('admin/actas/', Lang::get('admin.account_admin_actas')); }}</li>
                <?php
            }
            
            if (Auth::user()->acting_as) 
            {
                ?>
                <li class="dropdown-header">{{ Auth::user()->acting_as->first . ' ' . Auth::user()->acting_as->last }}</li>
                
                <li>{{ HTML::link('admin/actas/'.Auth::user()->id, Lang::get('admin.account_admin_actas_self')); }}</li>
                <?php 
            }
            ?>
            <li>{{ HTML::link('admin/link', 'link'); }}</li>
        </ul>
    </li>
    <?php } ?>
@endif