@if(Auth::check())
    <li class="dropdown">
        <?php if (Auth::user()->acting_as) { ?>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">**{{ Auth::user()->acting_as->first }} {{ Auth::user()->acting_as->last }}**<b class="caret"></b></a>
        <?php } else { ?>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->first }} {{ Auth::user()->last }}<b class="caret"></b></a>
        <?php } ?>
        <ul class="dropdown-menu">  
            <li>{{ HTML::link('account', Lang::get('account.account_area')); }}</li>        
            <li class="dropdown-header">{{ Lang::get('account.account_quick_access') }}</li>
            <li>{{ HTML::link('account/edit', Lang::get('account.account_edit')); }}</li>
            <li>{{ HTML::link('account/password', Lang::get('account.new_password')); }}</li>
            <li>{{ HTML::link('account/challenge', Lang::get('account.account_security')); }}</li>
            <li class="divider"></li>
            <li>{{ HTML::link('logout', Lang::get('account.logout')); }}</li>
        </ul>
    </li>
@endif