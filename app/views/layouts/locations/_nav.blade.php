@section('all_menu')
    @include('layouts.locations._all_actions')
@stop

@section('project_menu')
    @include('layouts.locations._project_actions')
@stop

@section('account_menu')
    @include('layouts.locations._account_actions')
@stop

@section('admin_menu')
    @include('layouts.locations._admin_actions')
@stop

@section('login_block')
    @if(!Auth::check() )
        {{ Form::open(array('url' =>'login', 'class' => 'form-inline')) }}
        <div class="form-group">
            <label class="sr-only" for="email">Lang::get('account.account_email')</label>
            <input type="email" class="form-control" id="email" name="email" placeholder="{{{Lang::get('account.account_email')}}}">
        </div>
        <div class="form-group">
            <label class="sr-only" for="password">{{{Lang::get('account.account_password')}}}</label>
            <input type="password" class="form-control" id="password" name="password" placeholder="{{{Lang::get('account.account_password')}}}">
        </div>
        <!-- <div class="checkbox">
            <label><input type="checkbox"> Remember me</label>
        </div>-->
        <button type="submit" class="btn btn-default">{{{Lang::get('account.login')}}}</button>
        {{ Form::token() . Form::close() }}
    @endif
@stop

@section('search_block')
   @if(Auth::check() ) 
        {{ Form::open(array('url' =>'/search', 'class' => 'form-inline')) }}  
            <div class="form-group">
                <label class="sr-only" for="search_keyword">{{{Lang::get('template.nav_search_keywords')}}}</label>
                <input type="text" class="form-control" id="search_keyword" name="search_keyword" placeholder="{{{ Lang::get('template.nav_search_keywords') }}}">
            </div>
            <button type="submit" class="btn btn-default">{{{ Lang::get('template.nav_search') }}}</button> 
        {{ Form::close() }}
    @endif
@stop



@section('menus')
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                {{ HTML::link('', 'Location Resources', array('class'=>'navbar-brand hidden-sm')); }}
            </div>
            <div id="nav-main" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-left">
                    @yield('all_menu')    
                    @yield('project_menu')
                   
                    @yield('admin_menu')
                    @yield('account_menu')

                    @if( ! Auth::check())
                        <li>{{ HTML::link('login', Lang::get('account.login')); }}</li>
                    @endif
                </ul>
              
                <ul class="nav navbar-nav navbar-right">
                    {{-- @yield('login_block') --}}
                    {{-- @yield('search_block') --}}
                </ul>
            </div><!--/.nav-collapse -->
        </div>
    </div>
</div>
@stop