@section('flash')
    <?php
    $messages = NULL;
    if ( Session::has('flash_success') )
    {
        $alertState = 'alert-success';
        $messages = Session::get('flash_success'); 
    }

    if ( Session::has('flash_info') )
    {
        $alertState = 'alert-info';
        $messages = Session::get('flash_info'); 
    }

    if ( Session::has('flash_warning') )
    {
        $alertState = 'alert-warning';
        $messages = Session::get('flash_warning'); 
    }

    if ( Session::has('flash_error') )
    {
        $alertState = 'alert-danger';
        $messages = Session::get('flash_error'); 
    }

    if ( Session::has('error') )
    {
        $alertState = 'alert-danger';
        $messages = Session::get('reason'); 
    }


    if ( $messages )
    {
        echo "<div class='alert alert-dismissable ".$alertState."'>";
        echo "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>";
        if( is_array($messages) ) 
        {
            echo "<ul>";
            foreach($messages as $message)
            {
                ?><li>{{{ trans($message) }}}</li><?php       
            }         
            echo "</ul>";
        }
        else
        { 
            ?> {{{ trans($messages) }}} <?php
        } 
        
        echo "</div>";
    }
    ?>
@stop