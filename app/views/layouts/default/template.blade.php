<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Laravel PHP Framework</title>
        <style>
            @import url(//fonts.googleapis.com/css?family=Lato:300,400,700);

            body {
                margin:0;
                font-family:'Lato', sans-serif;
                text-align:center;
                color: #999;
            }

            a, a:visited {
                color:#FF5949;
                text-decoration:none;
            }

            a:hover {
                text-decoration:underline;
            }

            ul li {
                display:inline;
                margin:0 1.2em;
            }

            p {
                margin:2em 0;
                color:#555;
            }
            #flash-main{
                margin: 10px;
            }
            .flash{
                border: 2px dashed #990000;
                padding: 10px;
                font-size: 1.2em;
                width: 60%;
                margin: auto;
            }
            #flash-success{
                background-color: #b6ff00;
                color: #00542d;
            }
            #flash-error, #flash-errorsub{
                background-color: #ffe3e3;
                color: #a00000;
            }
        </style>
    </head>
    <body>
        <h1>Laravel Locations</h1>
        <div id="main">
            <!--Navigation-->
            <div id="nav-main">

            </div>

            <!--Flash messages-->
            <div id="flash-main">

            </div>

            <!--Main output-->
            <div id="content-main">
            @yield('layout')
            </div>        
        </div>
    </body>
</html>