

        <div class="form-group row">
            {{ Form::label('email', Lang::get('project.project_email'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('email', $project->email, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('email') ? $errors->first('email') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('first', Lang::get('account.account_first'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('first', $project->first, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('first') ? $errors->first('first') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('last', Lang::get('account.account_last'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('last', $project->last, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('last') ? $errors->first('last') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('customer_title', Lang::get('account.account_title'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('customer_title', $project->customer_title, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('customer_title') ? $errors->first('customer_title') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('company', Lang::get('account.account_company'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('company', $project->company, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('company') ? $errors->first('company') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('address_1', Lang::get('account.account_address_1'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('address_1', $project->address_1, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('address_1') ? $errors->first('address_1') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('address_2', Lang::get('account.account_address_2'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('address_2', $project->address_2, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('address_2') ? $errors->first('address_2') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('city', Lang::get('account.account_city'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('city', $project->city, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('city') ? $errors->first('city') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('state', Lang::get('account.account_state'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('state', $project->state, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('state') ? $errors->first('state') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('zip', Lang::get('account.account_zip'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('zip', $project->zip, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('zip') ? $errors->first('zip') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('country', Lang::get('account.account_country'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('country', $project->country, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('country') ? $errors->first('country') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('phone', Lang::get('account.account_phone'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('phone', $project->phone, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('web_site', Lang::get('account.account_web_site'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('web_site', $project->web_site, array('class' => 'form-control')); }}   
                <span class="help-inline">{{ $errors->has('web_site') ? $errors->first('web_site') : '' }}</span>
            </div>
        </div>
