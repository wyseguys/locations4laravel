@extends('layouts._content')

@section('content')
{{ Form::open(array('url' => 'project/create'))  }}
    
    <div class="row">
	    <div class="col-md-6">
            @include('project._project_summary')
        </div>    

        <div class="col-md-4">
            @include('project._project_contact')
        </div>

        <div class="col-md-2">
            <div class="allProjects">        
                <h4><span class="glyphicon glyphicon-th-list"></span> {{ link_to('account/projects', Lang::get('project.all_project')); }}</h4>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-3 col-md-offset-5">
            <div class="form-actions">
                <button type="submit" class="btn btn-lg btn-primary">{{{Lang::get('project.new_project')}}}</button>
            </div>
        </div>
    </div>
{{ Form::token() . Form::close() }}
@stop