@extends('layouts._content')

@section('content')
    <div class="row">
	    <div class="col-md-10">
            @if( isset($project) ) 
                @include('project._project_dashboard', array('activeProject' => $project, 'user' => $user))
                @yield('dashboard')
            @endif
        </div>

        <div class="col-md-2">
            <div class="newProject">
                <h4><span class="glyphicon glyphicon-file"></span> {{ link_to('project/create', Lang::get('project.new_project')); }}</h4>
            </div>
            <div class="allProjects">        
                <h4><span class="glyphicon glyphicon-th-list"></span> {{ link_to('account/projects', Lang::get('project.all_project')); }}</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            @include('location._project_listings')
            @yield('project_listings')
        </div>
    </div>
        
@stop