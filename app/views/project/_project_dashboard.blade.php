@section('dashboard')
    <?php if( isset($activeProject) ) { ?>
        <div class="projectDashboard">
            <h4><span class="glyphicon glyphicon-edit"></span> {{ link_to('project/edit/' . $activeProject->id, $activeProject->title) }}</h4>
            <div class="well">{{{$activeProject->description}}}</div>
            <table class="table table-striped projectTable">
                <tr>
                    <th></th>
                    <th>{{{ Lang::get('project.project_title') }}}</th>
                    <th class="hidden-xs hidden-sm">{{{ Lang::get('project.project_start') }}}</th>
                    <th class="hidden-xs">{{{ Lang::get('project.project_expire') }}}</th>
                    <th>{{{ Lang::get('project.project_status') }}}</th>
                    <th>{{{ Lang::get('project.project_location_count') }}}</th>
                    <th class="hidden-xs hidden-sm">{{{ Lang::get('project.project_updated_at') }}}</th>
                    <th></th>
                </tr>
                <tr>
                    <td><span class="glyphicon glyphicon-eye-open"></span> {{ link_to('project/view/' . $activeProject->id, Lang::get('project.project_view')) }}</td>
                    <td>{{{ $activeProject->title }}}</td>
                    <td class="hidden-xs hidden-sm">{{{ $activeProject->getStartdate() }}}</td>
                    <td class="hidden-xs">{{{ $activeProject->getEnddate() }}}</td>
                    <td>{{{ $activeProject->status }}}</td>
                    <td><span class="badge">{{ $activeProject->projectCount() }}</span></td>
                    <td class="hidden-xs hidden-sm">{{{ $activeProject->updated_at->format('m/d/Y') }}}</td>
                    <td>
                        <?php if( ( ! $activeProject->active) && ($activeProject->status == 'active') ) { ?>
                            {{ link_to('project/activate/' . $activeProject->id, Lang::get('project.project_active'))  }}
                        <?php } ?>
                        <?php if( ($activeProject->active) && ($activeProject->status == 'active')) { ?>
                        <button type="button" class="btn btn-default">
                            <span class="glyphicon glyphicon-cloud-upload"></span> 
                            {{ link_to('project/submit/' . $activeProject->id, Lang::get('project.project_submit'))  }}
                        </button>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </div>
   <?php } ?>
@stop