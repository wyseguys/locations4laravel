        <div class="form-group row">
            {{ Form::label('title', Lang::get('project.project_title'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('title', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
            </div>
        </div>
        <div class="form-group row">
            {{ Form::label('start_date', Lang::get('project.project_start'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('start_date', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('start_date') ? $errors->first('start_date') : '' }}</span>
            </div>
        </div>
        <div class="form-group row">
            {{ Form::label('end_date', Lang::get('project.project_end'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('end_date', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('end_date') ? $errors->first('end_date') : '' }}</span>
            </div>
        </div>
        <div class="form-group row">
            {{ Form::label('description', Lang::get('project.project_description'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::textarea('description', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('description') ? $errors->first('description') : '' }}</span>
            </div>
        </div>