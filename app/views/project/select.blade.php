@extends('layouts._content')

@section('content')
    <div class="row">
        <div class="col-md-12">
        @if(count($projects) > 0)
            @foreach ($projects as $project)
            <div class="panel panel-default">
                <h4>{{ link_to('project/activate/' . $project->id . '/' . $locationId, $project->title); }}</h4>
            </div>
            @endforeach
         @endif
        <div class="newProject">
            <h4><span class="glyphicon glyphicon-file"></span> {{ link_to('project/create', Lang::get('project.new_project')); }}</h4>
        </div>
       
        </div>
    </div>
        
@stop