@extends('layouts._content')

@section('content')

<div class="row">
    <div class="col-md-12">
        PAGE CONTENT GOES HERE
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @include('location._all_locations')
        @yield('all_locations')
    </div>
</div>

@stop