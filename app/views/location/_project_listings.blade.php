@section('project_listings')
@if(isset($locations))
<div class="container">
    <div class="row">
        <div class="col-md-12">
            @foreach ($locations as $location)
                <div class="location locationSummary">
                    @include('location._listing_addremove', array('location' => $location, 'project' => $project))
                    <h4>{{ link_to('location/'.$location->id.'/'.$location->slug,  $location->name) }}</h4>
                    <p>{{{ $location->description }}}</p>
                </div>
            @endforeach
        </div>
    </div>
</div>
@endif
@stop