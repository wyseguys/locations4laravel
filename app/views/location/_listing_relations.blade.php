@if($categories)
    <ul>
    @foreach($categories as $category)
        <li>{{ link_to('category/'.$category->id.'/' . $category->slug, $category->title) }}</li>
    @endforeach
    </ul>
@endif