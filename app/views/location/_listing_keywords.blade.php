<?php
$keywords = explode(',', $keywords);    
?>
@foreach($keywords as $keyword)
    <span class="label label-default">{{ link_to('search/keywords/' . urlencode($keyword), $keyword) }}</span>
@endforeach