@section('all_locations')
    @foreach ($locations as $location)
    <div class="panel panel-default">
        @include('location._listing_addremove', array('location' => $location, 'project' => $project))
        <h4>{{ link_to('location/' . $location->id . '/' . $location->slug , $location->name); }}</h4>
        <p>{{{ $location->description }}}</p>
    </div>
    @endforeach
    
    <div class="row">
        <div class="col-md-12">
            <?php echo $locations->links(); ?>
        </div>
    </div>
@stop