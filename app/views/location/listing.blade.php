@extends('layouts._content')

@section('content')

<div class="row">
    <div class="col-sm-12 col-md-3">
        Thumbnail
    </div>
    <div class="col-sm-12 col-md-8">
        @include('location._listing_addremove', array('location' => $location, 'project' => $project))
        {{{ $location->sku }}}
    </div>
</div>
<div class="row">

</div>
<div class="row">
    <div class="col-sm-12 col-md-8">
        <p>{{{ $location->summary }}}</p>
        <p class="hidden-xs">{{{ $location->description }}}</p>
    </div>
    <div class="col-sm-12 col-md-4">
        <p> @include('location._listing_relations', array('categories' => $location->getAllLocationCategories())) </p>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        Gallery
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <p> @include('location._listing_keywords', array('keywords' => $location->keywords)) </p>
    </div>
</div>


@stop