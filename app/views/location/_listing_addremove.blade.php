<?php
$location_exists = ( $project ) ? $project->hasLocation($location) : FALSE;
?>

@if($location_exists)
<a href="{{ URL::to('location/remove/' . $location->id); }}" >
    <span class='glyphicon glyphicon-minus'>{{ Lang::get("location.remove_location_from_project") }}</span>
</a>
@else
<a href="{{ URL::to('location/add/' . $location->id); }}" >
    <span class='glyphicon glyphicon-plus'>{{ Lang::get('location.add_location_to_project') }}</span>
</a>
@endif