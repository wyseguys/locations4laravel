@extends('layouts._content')

@section('content')

<div class="row">
    <div class="col-md-9">
        PAGE CONTENT GOES HERE
    </div>
</div>
<div class="row">
    @include('category._all_categories')
    @yield('all_categories')
</div>

@stop