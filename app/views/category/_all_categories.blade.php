@section('all_categories')
    @foreach ($allCategories as $cat)
    <div class="pull-left panel panel-default">
        <div class="list-group">
            <a href="{{ route('category/group', array($cat[0]->id, $cat[0]->slug)) }}" class="list-group-item active">
                <h4 class="list-group-item-heading">{{{ $cat[0]->title }}}</h4>
                <p class="list-group-item-text">{{{ $cat[0]->description }}}</p>
            </a>
        </div>
        @include('category._cat_and_child_list', array('cat'=>$cat[1]))
    </div>
    @endforeach
@stop