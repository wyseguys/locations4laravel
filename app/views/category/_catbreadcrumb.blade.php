@if(count($ancestors) > 0)
<div class="categories">
    <ol class="breadcrumb">
        @foreach ($ancestors as $cat)
            <li>{{ link_to('category/' . $cat->id . '/' . $cat->slug, $cat->title) }}</li>
        @endforeach        
    </ol>
</div>
@endif