@foreach ($cat as $subcat)
    <div class="list-group">
        <a href="{{ route('category/group', array($subcat[0]->id, $subcat[0]->slug)) }}" class="list-group-item">
            <h4 class="list-group-item-heading">{{{ $subcat[0]->title }}}</h4>
            <p class="list-group-item-text">{{{ $subcat[0]->description }}}</p>
        </a>
    </div>
    @include('category._cat_and_child_list', array('cat'=>$subcat[1]))
@endforeach