@extends('layouts._content')

@section('content')

<div class="row">
    <div class="col-md-12">
        <?php echo $catparents; ?>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <?php echo $catbreadcrumb; ?>
    </div>
</div>
<div class="row">
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="categoryDescription panel-body">
              {{{ $category->description }}}
            </div>
        </div>
        @include('location._location_listings')
        @yield('location_listings')
    </div>
    <div class="col-md-3 pull-left">
        <?php echo $catchildren; ?>
    </div>
</div>


@stop