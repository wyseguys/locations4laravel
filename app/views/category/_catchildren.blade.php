@if(count($children) > 0)
<div class="categories childCategories">
    <h3>{{{ Lang::get('category.pagenav_childcategories') }}}</h3>
    <ul class="nav nav-pills nav-stacked">
        @foreach ($children as $cat)
            <li>{{ link_to('category/' . $cat->id . '/' . $cat->slug, $cat->title) }}</li>
        @endforeach        
    </ul>
</div>
@endif