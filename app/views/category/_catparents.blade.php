<div class="categories parentCategories">
    <ul class="nav nav-pills">
        @foreach ($parents as $cat)
            <li>{{ link_to('category/' . $cat->id . '/' . $cat->slug, $cat->title) }}</li>
        @endforeach        
    </ul>
</div>
