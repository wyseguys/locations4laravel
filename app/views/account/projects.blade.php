@extends('layouts._content')

@section('content')

<div class="row">
    <div class="col-md-12">
        {{{ Lang::get('template.nav_project_status') }}}
        <ul class="nav nav-pills">
        @foreach($status as $stat)
            <li>{{ link_to('account/projects/' . $stat, ucfirst($stat)) }}</li>
        @endforeach
        </ul>
    </div>
</div>

<div class="row">
    <div class="col-md-9">
        <table class="table table-striped projectTable">
            <tr>
                <th></th>
                <th></th>
                <th>{{{ Lang::get('project.project_title') }}}</th>
                <th class="hidden-xs hidden-sm">{{{ Lang::get('project.project_start') }}}</th>
                <th class="hidden-xs">{{{ Lang::get('project.project_expire') }}}</th>
                <th>{{{ ucfirst($current_status) }}}</th>
                <th class="hidden-xs hidden-sm">{{{ Lang::get('project.project_location_count') }}}</th>
                <th class="hidden-xs hidden-sm">{{{ Lang::get('project.project_updated_at') }}}</th>
                <th></th>
            </tr>
        @foreach ($projects as $project)
            <tr>
                <td rowspan="2">
                    @if( $project->status == 'active' )
                    <span class="glyphicon glyphicon-edit"></span> {{ link_to('project/edit/' . $project->id, Lang::get('project.project_edit')) }}</td>
                    @endif
                <td rowspan="2"><span class="glyphicon glyphicon-eye-open"></span> {{ link_to('project/view/' . $project->id, Lang::get('project.project_view')) }}</td>
                <td>{{{ Str::limit($project->title, 20) }}}</td>
                <td class="hidden-xs hidden-sm">{{{ $project->getStartdate() }}}</td>
                <td class="hidden-xs">{{{ $project->getEnddate() }}}</td>
                <td>
                    @if( ( ! $project->active) && ($project->status == 'active') )
                        <span class="glyphicon glyphicon-pushpin"></span> 
                        {{ link_to('project/activate/' . $project->id, Lang::get('project.project_active'))  }}
                    @endif
                    @if( ($project->active) && ($project->status == 'active'))
                    <button type="button" class="btn btn-default">
                        <span class="glyphicon glyphicon-cloud-upload"></span> 
                        {{ link_to('project/submit/' . $project->id, Lang::get('project.project_submit'))  }}
                    </button>
                    @endif

                    @if( $current_status == 'approved' )
                        {{{ $project->getApprovedDate() }}}
                    @endif

                    @if( $current_status == 'cancelled' )
                        {{{ $project->getCancelledDate() }}}
                    @endif

                    @if( $current_status == 'deleted' )
                        {{{ $project->deleted_at }}}
                    @endif

                    @if( $current_status == 'submitted' )
                        {{{ $project->getSubmittedDate() }}}
                    @endif

                    @if( $current_status == 'expired' )
                        {{{ $project->getEnddate() }}}
                    @endif

                </td>
                <td class="hidden-xs hidden-sm"><span class="badge">{{ $project->projectCount() }}</span></td>
                <td class="hidden-xs hidden-sm">{{{ $project->updated_at->format('m/d/Y') }}}</td>
            </tr>
            <tr>
                <td colspan="3">{{{ Str::words($project->description, 100) }}}</td>
            </tr>
        @endforeach
        </table>
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-md-12 newProject">
                <h4><span class="glyphicon glyphicon-file"></span> {{ link_to('project/create', Lang::get('project.new_project')); }}</h4>
            </div>
        </div>
    </div>

</div>


@stop