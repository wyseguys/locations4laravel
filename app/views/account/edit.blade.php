@extends('layouts._content')

@section('content')

<!--<form action="billing" method="post" class="form-horizontal" id="billingform" accept-charset="utf-8">-->
{{ Form::model($user, array('url' => 'account/edit', 'class' => 'form-horizontal'))  }}

<div class="row">
	<div class="col-md-6">


        <div class="form-group row">
            {{ Form::label('first', Lang::get('account.account_first'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('first', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('first') ? $errors->first('first') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('last', Lang::get('account.account_last'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('last', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('last') ? $errors->first('last') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('title', Lang::get('account.account_title'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('title', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('title') ? $errors->first('title') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('company', Lang::get('account.account_company'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('company', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('company') ? $errors->first('company') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('phone', Lang::get('account.account_phone'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('phone', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('phone') ? $errors->first('phone') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('web_site', Lang::get('account.account_web_site'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('web_site', $value= null, array('class' => 'form-control')); }}   
                <span class="help-inline">{{ $errors->has('web_site') ? $errors->first('web_site') : '' }}</span>
            </div>
        </div>

    </div>
    <div class="col-md-6">

        <div class="form-group row">
            {{ Form::label('address_1', Lang::get('account.account_address_1'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('address_1', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('address_1') ? $errors->first('address_1') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('address_2', Lang::get('account.account_address_2'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('address_2', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('address_2') ? $errors->first('address_2') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('city', Lang::get('account.account_city'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('city', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('city') ? $errors->first('city') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('state', Lang::get('account.account_state'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('state', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('state') ? $errors->first('state') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('zip', Lang::get('account.account_zip'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('zip', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('zip') ? $errors->first('zip') : '' }}</span>
            </div>
        </div>

        <div class="form-group row">
            {{ Form::label('country', Lang::get('account.account_country'), array('class' => 'col-md-3 control-label')); }}
            <div class="col-md-7">
                {{ Form::text('country', $value= null, array('class' => 'form-control')); }}
                <span class="help-inline">{{ $errors->has('country') ? $errors->first('country') : '' }}</span>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-3 col-md-offset-5">
        <div class="form-actions">
            <button type="submit" class="btn btn-lg btn-primary">{{{Lang::get('account.account_edit')}}}</button>
        </div>
    </div> <!-- .col-md-6 -->
{{ Form::token() . Form::close() }}
</div>
@stop