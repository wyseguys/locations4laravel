@extends('layouts._content')

@section('content')
{{ Form::open(array('url' => 'account/register', 'class' => 'form-horizontal')) }}
    <div class="row">
	    <div class="col-md-7 col-md-offset-2">
            <div class="form-group row">        
                {{ Form::label('first', Lang::get('account.account_first'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::text('first', $value=null, array('class' => 'form-control')); }}
                    <span class="help-inline">{{ $errors->has('first') ? $errors->first('first') : '' }}</span>
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('last', Lang::get('account.account_last'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::text('last', $value=null, array('class' => 'form-control')); }}
                    <span class="help-inline">{{ $errors->has('last') ? $errors->first('last') : '' }}</span>
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('email', Lang::get('account.account_email'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::text('email', $value=null, array('class' => 'form-control')); }}
                    <span class="help-inline">{{ $errors->has('email') ? $errors->first('email') : '' }}</span>
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('password', Lang::get('account.account_password'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::password('password', array('class' => 'form-control')); }}
                    <span class="help-inline">{{ $errors->has('password') ? $errors->first('password') : '' }}</span>
                </div>
           </div>
            <div class="form-group row">
                {{ Form::label('password_confirm', Lang::get('account.account_confirmpassword'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::password('password_confirm', array('class' => 'form-control')); }}        
                    <span class="help-inline">{{ $errors->has('password_confirm') ? $errors->first('password_confirm') : '' }}</span>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div>{{ link_to('login', Lang::get('account.login')); }}</div>
            <div> {{ link_to('account/lostpassword', Lang::get('account.account_reminder')); }}</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-md-offset-5">
            <div class="form-actions">
                <button type="submit" class="btn btn-lg btn-primary">{{{Lang::get('account.new_account')}}}</button>
            </div>
        </div>
    </div>
{{ Form::token() . Form::close() }}
@stop