@extends('layouts._content')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3>Welcome, {{ $user->first; }}</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-9">
    @if( isset($user->activeProject) ) 
        @include('project._project_dashboard', array('activeProject' => $user->activeProject, 'user' => $user))
        @yield('dashboard')
    @endif
    </div>
    <div class="col-md-3">
        <div class="row">
            <div class="col-xs-5 col-sm-5 col-md-12 newProject">
                <h4><span class="glyphicon glyphicon-file"></span> {{ link_to('project/create', Lang::get('project.new_project')); }}</h4>
            </div>
            <div class="col-xs-5 col-sm-5 col-md-12 allProjects">        
                <h4><span class="glyphicon glyphicon-th-list"></span> {{ link_to('account/projects', Lang::get('project.all_project')); }}</h4>
            </div>
        </div>
        <div class="row hidden-xs hidden-sm">
            <div class="col-md-12 accountMaintenance">
                <h4>{{ Lang::get('account.account_quick_access') }}</h4>
                <h5><span class="glyphicon glyphicon-pencil"></span> {{ HTML::link('account/edit', Lang::get('account.account_edit')); }}</h5>
                <h5><span class="glyphicon glyphicon-tower"></span> {{ HTML::link('account/password', Lang::get('account.new_password')); }}</h5>
                <h5><span class="glyphicon glyphicon-question-sign"></span> {{ HTML::link('account/challenge', Lang::get('account.account_security')); }}</h5>
            </div>
        </div>
    </div>
</div>
@stop