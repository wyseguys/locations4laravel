@extends('layouts._content')

@section('content')
{{ Form::open(array('url' => 'account/password', 'class' => 'form-horizontal')) }}
    <div class="row">
	    <div class="col-md-8 col-md-offset-3">
            <div class="form-group row">
                {{ Form::label('currentpassword', Lang::get('account.current_password'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::password('currentpassword', array('class' => 'form-control')); }}
                    <span class="help-inline">{{ $errors->has('currentpassword') ? $errors->first('currentpassword') : '' }}</span>
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('password', Lang::get('account.account_newpassword'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::password('password', array('class' => 'form-control')); }}
                    <span class="help-inline">{{ $errors->has('password') ? $errors->first('password') : '' }}</span>
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('password_confirm', Lang::get('account.account_newconfirmpassword'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::password('password_confirm', array('class' => 'form-control')); }}
                    <span class="help-inline">{{ $errors->has('password_confirm') ? $errors->first('password_confirm') : '' }}</span>
                </div>                        
            </div>        
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-md-offset-5">
            <div class="form-actions">
                <button type="submit" class="btn btn-lg btn-primary">{{{Lang::get('account.new_password')}}}</button>
            </div>
        </div>
    </div>

{{ Form::token() . Form::close() }}
@stop