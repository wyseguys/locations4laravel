@extends('layouts._content')

@section('content')
{{ Form::open(array('url' => 'login', 'class' => 'form-horizontal')) }}
    

<div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-lock"></span> {{{ Lang::get('account.login') }}}</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form">
                    <div class="form-group">
                        {{ Form::label('email', Lang::get('account.account_email'), array('class' => 'col-md-3 control-label')); }}
                        <div class="col-sm-9">
                            {{ Form::text('email', $value=null, array('class' => 'form-control', 'placeholder'=>'you@example.com', 'required'=>'required')); }}
                        </div>

                    </div>
                    <div class="form-group">
                        {{ Form::label('password', Lang::get('account.account_password'), array('class' => 'col-md-3 control-label')); }}
                        <div class="col-sm-9">
                            {{ Form::password('password', array('class' => 'form-control', 'placeholder'=>Lang::get('account.account_password'), 'required'=>'required')); }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox"/>Remember me
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group last">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-success btn-sm">{{{Lang::get('account.login')}}}</button>
                        </div>
                    </div>
                    </form>
                    <div class="pull-right"> {{ link_to('account/lostpassword', Lang::get('account.account_reminder')); }}</div>
                </div>
                <div class="panel-footer">
                    <span class="glyphicon glyphicon-user"></span> {{ link_to('account/register', Lang::get('account.new_account')); }}
                </div>
            </div>
        </div>
    </div>
{{ Form::token() . Form::close() }}

@stop
