@extends('layouts._content')

@section('content')
{{ Form::open(array('url' => 'account/challenge', 'class' => 'form-horizontal')) }}
    <div class="row">
	    <div class="col-md-8 col-md-offset-1">
            <div class="form-group row">
                {{ Form::label('challenge_question', Lang::get('account.account_challenge_question'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-9">
                    {{ Form::select('challenge_question', $questions, $current_question, array('class' => 'form-control')); }}
                    <span class="help-inline">{{ $errors->has('challenge_question') ? $errors->first('challenge_question') : '' }}</span>
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('challenge_answer', Lang::get('account.account_challenge_answer'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-9">
                    {{ Form::text('challenge_answer', $value= null, array('class' => 'form-control')); }}
                    <span class="help-inline">{{ $errors->has('challenge_answer') ? $errors->first('challenge_answer') : '' }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-md-offset-5">
            <div class="form-actions">
                <button type="submit" class="btn btn-lg btn-primary">{{{Lang::get('account.account_security')}}}</button>
            </div>
        </div>
    </div>
    {{ Form::token() . Form::close() }}
@stop