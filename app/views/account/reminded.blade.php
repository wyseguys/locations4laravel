@extends('layouts._content')

@section('content')
    <div class="row">
	    <div class="col-md-7 col-md-offset-2">
            <div class="form-group row">        
                @if (Session::has('success'))
                    <h4>{{ Lang::get('account.success_reminder_sent') }}</h4>
                @elseif (Session::has('error'))
                    <h4>{{ Lang::get('account.error_reminder_sent') }}</h4>
                @endif
            </div>
        </div>
        <div class="col-md-3">
            <h4><span class="glyphicon glyphicon-user"></span> {{ link_to('account/register', Lang::get('account.new_account')); }}</h4>
            <div> {{ link_to('login', Lang::get('account.login')); }}</div>
        </div>
    </div>
@stop