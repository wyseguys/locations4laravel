@extends('layouts._content')

@section('content')

{{ Form::open(array('url' => 'account/lostpassword', 'class' => 'form-horizontal')) }}

    <div class="row">
	    <div class="col-md-7 col-md-offset-2">
            <div class="form-group row">  
                {{ Form::label('email', Lang::get('account.account_email'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::text('email', $value=null, array('class' => 'form-control')); }}
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <h4><span class="glyphicon glyphicon-user"></span> {{ link_to('account/register', Lang::get('account.new_account')); }}</h4>
            <div> {{ link_to('login', Lang::get('account.login')); }}</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-md-offset-5">
            <div class="form-actions">
                <button type="submit" class="btn btn-lg btn-primary">{{{Lang::get('account.account_reminder')}}}</button>
            </div>
        </div>
    </div>
    {{ Form::token() . Form::close() }}
@stop