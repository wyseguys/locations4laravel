@extends('layouts._content')

@section('content')
{{ Form::open(array('url' => 'account/reminder/' . $email, 'class' => 'form-horizontal')) }}

    <div class="row">
	    <div class="col-md-7 col-md-offset-2">
            <div class="form-group row">        
                {{ Form::label('email', Lang::get('account.account_email'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::hidden('email', $email); }}
                    {{ $email; }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('challenge_question', Lang::get('account.account_challenge_question'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ $current_question; }}
                </div>
            </div>
            <div class="form-group row">
                {{ Form::label('challenge_answer', Lang::get('account.account_challenge_answer'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::password('challenge_answer', array('class' => 'form-control')); }}
                    <span class="help-inline">{{ $errors->has('challenge_answer') ? $errors->first('challenge_answer') : '' }}</span>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <h4><span class="glyphicon glyphicon-user"></span> {{ link_to('account/register', Lang::get('account.new_account')); }}</h4>
            <div> {{ link_to('login', Lang::get('account.login')); }}</div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 col-md-offset-5">
            <div class="form-actions">
                <button type="submit" class="btn btn-lg btn-primary">{{{Lang::get('account.account_reminder')}}}</button>
            </div>
        </div>
    </div>

{{ Form::token() . Form::close() }}  
@stop