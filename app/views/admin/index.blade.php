@extends('layouts._content')

@section('content')
<div class="row">
    <div class="col-md-12">
        <h3>Administrator, {{ $user->first; }}</h3>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="newProject">
            <h4><span class="glyphicon glyphicon-file"></span> Admin Actions Link</h4>
        </div>
        <div class="allProjects">        
            <h4><span class="glyphicon glyphicon-th-list"></span> Admin Actions Link</h4>
        </div>
    </div>
</div>
@stop