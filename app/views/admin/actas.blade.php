@extends('layouts._content')

@section('content')

{{ Form::open(array('url' => 'admin/actas', 'class' => 'form-horizontal')) }}
    <div class="row">
	    <div class="col-md-3">
            <div class="form-group row">
                <div class="col-md-12">
                {{ Form::label('search', Lang::get('admin.account_actas_keywords'), array('class' => 'col-md-3 control-label')); }}
                <div class="col-md-6">
                    {{ Form::text('search', $value=null, array('class' => 'form-control')); }}
                </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-9 col-md-offset-1">
                <div class="form-actions">
                    <button type="submit" class="btn btn-lg btn-primary">{{{Lang::get('admin.account_actas_search')}}}</button>
                </div>
                </div>
            </div>
        </div>

        @if( isset($users) )
        <div class="col-md-7">
            <h4>Found Users:</h4>
            <ul>
            @foreach ($users as $user)
                <li>{{ link_to('admin/actas/' . $user->id, '['.$user->email.'] ' . $user->last . ', ' . $user->first ) }}</li>
            @endforeach
            </ul>
        </div>
        @endif
    </div>

{{ Form::token() . Form::close() }}

@stop