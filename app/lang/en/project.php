<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Project Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used for the project controller actions. 
    | T
	|
	*/
    'project_area' => 'Projects',
    'all_project' => 'All Projects',
    'past_project' => 'Project Archive',
    'new_project' => 'New Project',
    'edit_project' => 'Edit Project',
    'current_project' => 'Current Project',
    'invalid_project' => 'Invalid Project',
    'project_activated' => 'Project is now activated',
    
    'default_project_title' => 'No Project Title Specified',
    'default_project_description' => 'No Project Description Specified',

    'project_title' => 'Project Title',
    'project_start' => 'Start Date',
    'project_end' => 'End Date',
    'project_expire' => 'Expire Date',
    'project_description' => 'Description',
    'project_status' => 'Status',
    'project_updated_at' => 'Last Update',
    'project_location_count' => 'Locations',

    'project_email' => 'E-Mail Address',
    'project_password' => 'Password',
    'project_company' => 'Company',
    'project_title' => 'Title',
    'project_first' => 'First',
    'project_last' => 'Last',
    'project_address_1' => 'Address',
    'project_address_2' => ' ',
    'project_address_3' => ' ',
    'project_city' => 'City',
    'project_state' => 'State',
    'project_zip' => 'Zip Code',
    'project_country' => 'Country',
    'project_phone' => 'Phone',
    'project_web_site' => 'Web Site',

    'project_edit' => 'Edit',
    'project_view' => 'View',
    'project_active' => 'Use',
    'project_submit' => 'Submit',


    'success_create_project' => 'Project was created',
    'success_edit_project' => 'Project was updated',
    'success_project_submitted' => 'Project has been submitted',



    'error_create_project' => 'Project was NOT created!',
    'error_edit_project' => 'Project was NOT updated!',
    'error_project_submitted' => 'Project has NOT been submitted',

);