<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Admin Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used for the admin controller actions. 
    | These include logging in, out and signing up for a new account
	|
	*/
    'admin_tools' => 'Admin Tools',
    'admin_home' => 'Admin Home',
    'admin_area' => 'Admin Tools',  
    'account_quick_access' => 'Account Quick Access',

    'account_admin_actas' => 'Act For Customer',
    'account_admin_actas_self' => 'Stop Acting For Customer',
    'account_actas_search' => 'Search for customer email',
    'account_actas_keywords' => 'Email Address',

    'success_actas_set' => 'Successfully acting as ',
    'success_actas_clear' => 'Successfully cleared acting as',
    'success_actas_clear' => 'Successfully cleared acting as',

    'error_actas_set' => 'Unable to act as customer',
    'error_actas_search' => 'Unable to find any customers',
    'error_actas_admin_disabled' => 'Admin is not enabled',
    'error_actas_user_disabled' => 'User is not enabled',
    'error_actas_admin_privilege' => 'Admin lacks privilege to act as another',
    'error_actas_user_privilege' => 'User exceeds admin\'s privilege',

);