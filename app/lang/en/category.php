<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Category Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used for the category controller actions. 
    | These include logging in, out and signing up for a new account
	|
	*/

    'page_categories' => 'Location Categories',

    'pagenav_childcategories' => 'More Categories',

    'category_not_found' => 'That category wasn\'t found',


);