<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Template Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used for the layouts and templates
	|
	*/
    'all_navigation' => 'Main',

    'nav_search' => 'Search',
    'nav_search_keywords' => 'Keyword Search',

    'nav_project_status' => 'Sort By Project Status',
);