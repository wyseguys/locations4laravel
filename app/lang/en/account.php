<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Account Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used for the account controller actions. 
    | These include logging in, out and signing up for a new account
	|
	*/
    'admin_tools' => 'Admin Tools',
    'account_quick_access' => 'Account Quick Access',
    'account_area' => 'Account Home',
    'admin_home' => 'Admin Home',
    'admin_area' => 'Admin Tools',
    'account_email' => 'E-Mail Address',
    'account_password' => 'Password',
    'account_company' => 'Company',
    'account_title' => 'Title',
    'account_first' => 'First',
    'account_last' => 'Last',
    'account_address_1' => 'Address',
    'account_address_2' => ' ',
    'account_city' => 'City',
    'account_state' => 'State',
    'account_zip' => 'Zip Code',
    'account_country' => 'Country',
    'account_phone' => 'Phone',
    'account_web_site' => 'Web Site',

    'account_challenge_question' => 'Question',
    'account_challenge_answer' => 'Answer',

    'account_confirmpassword' => 'Confirm Your Password',
    'account_newpassword' => 'New Password',
    'account_newconfirmpassword' => 'Confirm Your New Password',
    
    'account_edit' => 'Edit Your Account',
    'account_security' => 'Edit Account Security',

    
    'account_reminder' => 'Get Password Reminder',
    'account_reset' => 'Reset Your Reminder',

    'account_admin_actas' => 'Act For Customer',
    'account_admin_actas_self' => 'Stop Acting For Customer',

    'acount_actas_search' => 'Search for customers',
    'acount_actas_keywords' => 'Keywords',

    'login' => 'Sign in',
    'logout' => 'Sign out',
	'new_account' => 'Register A New Account',
    'new_password' => 'Change Your Password',
    'current_password' => 'Current Password',
    'reset_lost_password' => 'Reset Your Lost Password',

    'success_creating_account' => 'Your account was successfully created',
    'success_logging_in' => 'You are now logged in',
    'success_logging_out' => 'You are now logged out',
    'success_password_change' => 'Your password has been updated',
    'success_reminder_sent' => 'An e-mail with the password reset has been sent.',
    'success_edit_account' => 'Your account has been updated',
    'success_edit_challenge' => 'Your challenge and answer have been updated',
    'success_found_account' => 'Your account was found',
    'success_actas_set' => 'Successfully acting as ',
    'success_actas_clear' => 'Successfully cleared acting as',

    'error_registration'=> 'We\'re sorry, there was a problem with your form',
    'error_creating_account' => 'There was an error creating your account!',
    'error_logging_in' => 'Please check your credentials and try again',
    'error_password_change' => 'Your password has NOT been changed!',
    'error_reminder_sent' => 'An error occured attempting to reset your password',
    'error_edit_account' => 'Your account has NOT been changed!',
    'error_edit_challenge' => 'Your challenge and answer have NOT been updated!',
    'error_found_account' => 'Your account was NOT found',
    'error_challenge_answer' => 'Your challenge answer was incorrect',
    'error_actas_set' => 'Unable to act as customer',
    'error_actas_search' => 'Unable to find any customers',
);