<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Error messages
	|--------------------------------------------------------------------------
	|
	| The following language lines are used for the errors for all the models.
	|
	*/
    'project_not_active' => 'This project is not active and cannot be saved',
    'project_expired' => 'This project is expired',
    'project_disabled' => 'This project is disabled',
    'project_deleted' => 'This project is deleted',

    'project_cannot_submit_not_current' => 'Cannot submit a project unless it is the currently active project',

);