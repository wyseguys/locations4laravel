<?php 

return array(

	/*
	|--------------------------------------------------------------------------
	| Location Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used for the category controller actions. 
    | These include logging in, out and signing up for a new account
	|
	*/

    'page_locations' => 'All Locations',

    'location_not_found' => 'We were unable to find a matching location',
    'add_location_to_project' => 'Add',
    'remove_location_from_project' => 'Remove',
    
    'location_added_success' => 'Location added',
    'location_removed_success' => 'Location removed',

    'location_added_no_project' => 'No active project found',

    

);