<?php
use LaravelBook\Ardent\Ardent;

class Location extends Eloquent implements ILocationRepository {

	/**
	 * The database fields the model can mass assign.
	 *
	 * @var array
	 */
    protected $fillable = array(
                            'sku', 
                            'name',
                            'slug', 
                            'summary',
                            'description',
                            'keywords',
                            'gallery_id',
                            'thumbnail_id',
                            'enabled',
                            'priority',
                            );


	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'locations';

	/**
     * Belongs mang to Categories
     */
    public function Category()
    {
        return $this->belongsToMany('Category')
                ->where('categories.enabled', '=', '1')
                ->orderBy('categories.priority', 'asc', 'categories.title', 'asc');
    }


    /**
     * Ardent validation rules
     */
    public static $rules = array(
        'sku' => 'required',
        'name' => 'required', 
        'slug' => 'required',
        'enabled' => 'required',
        'priority' => 'int'
    );

    /**
     * Array used by FactoryMuff to create Test objects
     */
    public static $factory = array(
        'sku' => 'string',
        'name' => 'string', 
        'slug' => 'string', 
        'description' => 'text',
        'priority' => 100,
        'enabled' => 1 
    );


	/**
	 * EVENTS
	*/
    public static function boot()
    {
        parent::boot();

        //Project::saving(function($project) {  });
    }


	/**
	 * Attributes
	*/
    public $errors = array();



    /**
	 * Create a location
	 *
	 * @return Location
	 */
    public static function createLocation($options)
    {
        throw new Exception('Not implemented');
    }

    /**
	 * Edit a location
	 *
	 * @return Location
	 */
    public function editLocation($options)
    {
        throw new Exception('Not implemented');
    }

    /**
	 * Delete a location category
	 *
	 * @return Boolean
	 */
    public function deleteLocation($options)
    {
        throw new Exception('Not implemented');
    }

    /**
	 * Find a location
	 *
	 * @return Location
	 */
    public static function findLocation($options)
    {
        if ( array_key_exists('id', $options) )
        {
            return Location::where('id', '=', $options['id'])
                            ->where('enabled', '=', 1)
                            ->first();
        }
        if ( array_key_exists('slug', $options) )
        {
            return Location::where('slug', '=', $options['slug'])
                            ->where('enabled', '=', 1)
                            ->first();
        }
        return NULL; 
    }

    /**
	 * Find all locations in the system
	 *
	 * @return Array of Locations
	 */
    public static function getAllLocations($pageSize = 10)
    {
        $results = Location::where('enabled', '=', 1)
                    ->orderBy('priority', 'asc')
                    ->orderBy('name', 'asc')
                    ->paginate($pageSize);

        return $results;
    }

    /**
	 * Find a location's categories
	 *
	 * @return Array of Category
	 */
    public function getAllLocationCategories()
    {
        $results = Location::find($this->id)->category;
        return $results;
    }


}