<?php

interface IProjectRepository
{

    /**
	 * Activate the project by flagging the current user
	 *
	 * @return NULL
	 */
    public function activateProject();

    /**
	 * Create a new project for the specified user id
	 *
	 * @return Project
	 */
    public static function createProject($user, $options);

    /**
	 * Edit a project by various criteria
	 *
	 * @return Project
	 */
    public function editProject($user, $options);

    /**
	 * Get the project that is 'active' for the specified user
	 *
	 * @return Project or NULL
	 */
    public static function getActiveproject($user_id);

    /**
	 * Get a project by various criteria
	 *
	 * @return Project
	 */
    public function getProject($user, $project_id);

    /**
    * Return the end date with the format
    *
    * @return string
    */
    public function getEnddate($format);

    /**
    * Return the start date with the format
    *
    * @return string
    */
    public function getStartdate($format);

    /**
    * Return the approved date with the format
    *
    * @return string
    */
    public function getApprovedDate($format);

    /**
    * Return the cancelled date with the format
    *
    * @return string
    */
    public function getCancelledDate($format);

    /**
    * Return the submitted date with the format
    *
    * @return string
    */
    public function getSubmittedDate($format);

    /**
	 * Get an array of projects
	 *
	 * @return Array of Project
	 */
    public function getUserProjects($user, $options);

    /**
	 * Get an array the statuses for the users projects
	 *
	 * @return Array of Strings
	 */
    public function getUserProjectStatus($user, $options);

    /**
	 * Checks to see if the current project is valid for the customer to use
	 * which will mean not expired, not deleted, etc
	 * @return Project
	 */
    public function isValid();

    /**
	 * Set the status and check the expiration status of a project
	 *
	 */
    public function setStatus();

    /**
	 * Perform the actions for submitting a project for review
     *  ie setting the status, emailing admins
     *
	 */
    public function submitProject();

    /**
     * Find and loop through every active project and update the status
     *  so the customer is working with a current list of projects
     */
    public function updateActiveProjectStatus($user);

    /**
     * Return an integer of the number of locations in the project
     */
    public function projectCount();

    /**
     * Return an array of locations that have been added to the project
     */
    public function getLocations();

    /**
     * Return an boolean if the location exists in within the project
     */
    public function hasLocation($location);
    
    /**
     * Add the location to the project if the location doesn't
     * already exist in the project
     */
    public function addLocation($location);

    /**
     * Remove any instance of the location from the project
     */
    public function removeLocation($location);
}