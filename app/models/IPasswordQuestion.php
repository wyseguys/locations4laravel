<?php

interface IPasswordQuestion
{
    /**
	 * Get all of the available questions from the database
     *  add in the current question if it doesn't already exist
     *  in the list from the database, then shuffle the output
	 *
	 * @return Array of PasswordChallenge strings
	 */
    public function getCurrentQuestions($options);

	/**
	 * Can be used to check either the question
     *  or the answer.
	 *
	 * @return Boolean
	 */
    public function check($options);
}