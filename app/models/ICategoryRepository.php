<?php

interface ICategoryRepository
{

    /**
	 * Create a location category
	 *
	 * @return Category
	 */
    public static function createCategory($options);

    /**
	 * Edit a location category
	 *
	 * @return Category
	 */
    public function editCategory($options);

    /**
	 * Delete a location category
	 *
	 * @return Boolean
	 */
    public function deleteCategory($options);

    /**
	 * Find a location category
	 *
	 * @return Category
	 */
    public static function findCategory($options);

    /**
	 * Find the parent categories of the current category all the way up
	 *
	 * @return Array of Category
	 */
    public function getAncestorCategories();

    /**
	 * Find all valid location categories
	 *
	 * @return Array of Category
	 */
    public static function getAllCategories($startingCategory = NULL);

    /**
	 * Find a location category that has no other parent
	 *
	 * @return Array of Category
	 */
    public static function getAllParentCategories();

     /**
	 * Find the parent category of the current category
	 *
	 * @return Category
	 */
    public function getParentCategory();

    /**
	 * Find the child categories
	 *
	 * @return Array of Categories or NULL
	 */
    public function getSubCategories();

    /**
	 * Add the the location to the category
	 *
	 * @return Boolean
	 */
    public function addLocation($location);

    /**
	 * Get the the location in the category
	 *
	 * @return Array of Locations
	 */
    public function getLocations($pageSize);


}