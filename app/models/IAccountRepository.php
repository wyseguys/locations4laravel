<?php

interface IAccountRepository
{
    /**
	 * Changes the account password
	 *
	 * @return User
	 */
    public function changePassword($options);

	/**
	 * Creates a new Account
	 *
	 * @return User
	 */
    public static function createUser($options);

	/**
	 * Edit an existing user's challenge question and answer
	 *
	 * @return User
	 */
    public function editChallenge($options);

	/**
	 * Edit an existing Account
	 *
	 * @return User
	 */
    public function editUser($options);

	/**
	 * Get an existing Account
	 * - by id
     * - by email
	 * @return User or NULL
	 */
    public static function findUser($options);

    /**
    *  Confirm challenge for the exiting user
    *
    * @return boolean
    */
    public function confirmChallenge($question, $answer);

     /**
	 * Authenticate the user with the Auth class
	 *
	 * @return boolean
	 */
    public static function login($email, $secret);

	/**
	 * Unauthorizes the user with the built in method
	 *
	 * @return null
	 */
    public function logout();

    /**
	 * Changes the account password to a random value
	 *
	 * @return User
	 */
    public function passwordReminder($optionsl, $success_url);

}