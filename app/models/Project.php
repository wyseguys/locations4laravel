<?php
use LaravelBook\Ardent\Ardent;

class Project extends Eloquent implements IProjectRepository {

	/**
	 * The database fields the model can mass assign.
	 *
	 * @var array
	 */
    protected $fillable = array(
                            'user_id', 
                            'start_date', 
                            'end_date', 
                            'title', 
                            'description',
                            'company', 
                            'customer_title',
                            'title', 
                            'first', 
                            'last', 
                            'address_1', 
                            'address_2', 
                            'city', 
                            'state', 
                            'zip', 
                            'country', 
                            'phone', 
                            'web_site',
                            'email',
                            'active',
                            'enabled',
                            );

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'projects';

	/**
     * Belongs to user
     */
    public function user()
    {
        return $this->belongsTo( 'User', 'user_id' );
    }

	/**
     * Belongs mang to Categories
     */
    public function Locations()
    {
        return $this->belongsToMany('Location')
                ->where('locations.enabled', '=', '1')
                ->orderBy('added_on');
    }

    /**
     * Ardent validation rules
     */
    public static $rules = array(
        'start_date' => 'required',
        'end_date' => 'required',
        'title' => 'required',
        'description' => 'required',
        'status' => 'required',
        'user_id' => 'required|numeric',
        'first' => 'required',
        'last' => 'required',
        'email'=>'required',
    );

    /**
     * Array used by FactoryMuff to create Test objects
     */
    public static $factory = array(
        'start_date' => '2014-01-01',
        'end_date' => '2014-02-01',
        'title' => 'string',
        'description' => 'text',
        'status' => '',
        'user_id' => 'factory|User',
        'first' => 'string',
        'last' => 'string',
        'email'=>'test@example.com',
        'enabled' => 1,
    );


	/**
	 * EVENTS
	*/
    public static function boot()
    {
        parent::boot();

        Project::saving(function($project)
        {
            $old_status = $project->status;
            $project->setStatus();
            //TODO:  set a config item to define admin user privelege levels and swap '10' with a constant
            //$allow_save = ( Auth::user()->privelege > 10) ? TRUE : FALSE;
            $allow_save = FALSE;
            $allow_save = ( $old_status == NULL) ? TRUE : $allow_save;
            $allow_save = ( $old_status == 'active') ? TRUE : $allow_save;

            //TODO: some sort of admin credentials needed to save here
            if ( ! $allow_save )
            {
                $project->errors[] = Lang::get('errors.project_not_active');
                return FALSE;
            }
        });
    }


	/**
	 * Attributes
	*/
    public $errors = array();


    /**
	 * Activate the project by flagging it and de-actiavating the others
	 *
	 * @return Current object
	 */
    public function activateProject()
    {
        //TODO:  cannot make a project active unless is a valid project

        $affectedRows = Project::where('user_id', '=', $this->user_id)->update(array('active' => 0));

        $this->active = TRUE;
        $this->save();
        return $this;
    }

    /**
	 * Create a new project for the specified user id
	 *
	 * @return Project
	 */
    public static function createProject($user, $options)
    {
        if ( $user == NULL ) { return NULL; }
        
        $start_date = ( array_key_exists('start_date', $options) ) ? $options['start_date'] : NULL;
        if ( $start_date == NULL ) { return NULL; }
        $end_date = ( array_key_exists('end_date', $options) ) ? $options['end_date'] : NULL;
        if ( $end_date == NULL ) { return NULL; }
        
        $today = new DateTime();
        $start_date = new DateTime($options['start_date']);
        $end_date   = new DateTime($options['end_date']);
        
        if ( $start_date->format('Y-m-d') < $today->format('Y-m-d') ) { return NULL; }
        if ( $end_date->format('Y-m-d') < $start_date->format('Y-m-d') ) { return NULL; }


        $options['title']       = ( array_key_exists('title', $options) ) ? $options['title'] : Lang::get('project.default_project_title');
        $options['description'] = ( array_key_exists('description', $options) ) ? $options['description'] : Lang::get('project.default_project_description');
        $options['user_id']    = $user->id;
        $options['start_date'] = $start_date;
        $options['end_date']   = $end_date;
        $options['enabled']    = 1;

        $project = Project::create($options);

        if ( $project )
        {
            return $project;
        }
        else
        {
            //TODO: log an error, return an error???
            return NULL;
        }
        
    }

    /**
	 * Edit a project by various criteria
	 *
	 * @return Project
	 */
    public function editProject($user, $options)
    {
        if ( $user == NULL ) { return NULL; }

        $start_date = ( array_key_exists('start_date', $options) ) ? $options['start_date'] : NULL;
        $end_date = ( array_key_exists('end_date', $options) ) ? $options['end_date'] : NULL;
        if ( $start_date == NULL ) { return NULL; }
        if ( $end_date == NULL ) { return NULL; }

        $options['title']       = ( array_key_exists('title', $options) ) ? $options['title'] : Lang::get('project.default_project_title');
        $options['description'] = ( array_key_exists('description', $options) ) ? $options['description'] : Lang::get('project.default_project_description');
        $options['user_id']    = $user->id;
        $options['start_date'] = new DateTime($options['start_date']);
        $options['end_date']   = new DateTime($options['end_date']);

        $project = $this->update($options);

        if ( $project )
        {
            return $project;
        }
        else
        {
            //TODO: log an error, return an error???
            return NULL;
        }
    }

    /**
	 * Get the project that is 'active' for the specified user
	 *
	 * @return Project or NULL
	 */
    public static function getActiveproject($user_id)
    {
        $project = Project::where('user_id', '=', $user_id)->where('active', '=', TRUE)->first();
        return $project;
    }

    /**
	 * Get a project by various criteria
	 *
	 * @return Project
	 */
    public function getProject($user, $project_id)
    {
        $project = Project::where('id', '=',$project_id)->where('user_id', '=', $user->id)->first();
        return $project;
    }

    /**
    * Return the end date with the format
    *
    * @return string
    */
    public function getEnddate($format = "m/d/Y")
    {
        return date_format(date_create($this->end_date), $format);
    }

    /**
    * Return the start date with the format
    *
    * @return string
    */
    public function getStartdate($format = "m/d/Y")
    {
        return date_format(date_create($this->start_date), $format);
    }

    /**
    * Return the approved date with the format
    *
    * @return string
    */
    public function getApprovedDate($format = "m/d/Y")
    {
        return date_format(date_create($this->approved_at), $format);
    }

    /**
    * Return the cancelled date with the format
    *
    * @return string
    */
    public function getCancelledDate($format = "m/d/Y")
    {
        return date_format(date_create($this->cancelled_at), $format);
    }

    /**
    * Return the submitted date with the format
    *
    * @return string
    */
    public function getSubmittedDate($format = "m/d/Y")
    {
        return date_format(date_create($this->submitted_at), $format);
    }

    /**
	 * Get an array of projects
	 *
	 * @return Array of Project
	 */
    public function getUserProjects($user, $options)
    {
        $status = ( array_key_exists('status', $options) ) ? $options['status'] : 'active';

        $projects = Project::where('user_id', '=', $user->id)
                            ->where('status', '=', $status)
                            ->orderBy('end_date', 'asc')
                            ->get();

        return $projects;
    }

    /**
	 * Get an array the statuses for the users projects
	 *
	 * @return Array of Strings
	 */
    public function getUserProjectStatus($user, $options = array())
    {
        $include_admin = array_key_exists('include_admin', $options);

        $result = DB::table('Projects')->where('user_id', '=', $user->id)->distinct()->lists('status');

        if ( ! $include_admin)
        {
            if ( ($key = array_search('deleted', $result)) !== FALSE)
            {
                unset($result[$key]);
            }
            if ( ($key = array_search('disabled', $result)) !== FALSE)
            {
                unset($result[$key]);
            }
        }
        sort($result);

        return $result;
    }

    /**
	 * Checks to see if the current project is valid for the customer to use
	 * which will mean not expired, not deleted, etc
	 * @return Project
	 */
    public function isValid()
    {
        $this->errors = array();
        if ($this->status == 'expired')
        {
            $this->errors[] = Lang::get('errors.project_expired');
        }

        if ($this->status == 'disabled')
        {
            $this->errors[] = Lang::get('errors.project_disabled');
        }

        if ($this->status == 'deleted')
        {
            $this->errors[] = Lang::get('errors.project_deleted');
        }

    }


    /**
	 * Set the status and check the expiration status of a project
	 * active, expired, submitted, approved, cancelled, disabled, deleted
	 */
    public function setStatus()
    {
        $this->status = 'active'; //default, if no other state applies, then it is active

        $end_date = ( ($this->end_date instanceof DateTime) ) ? $this->end_date->format('Y-m-d') : $this->end_date;

        //Expired based on the current date and the project end date
        if ( strtotime($end_date) < strtotime('now') )
        {
            $this->active = FALSE;
            $this->status = 'expired';
        }            

        //Various date flags will set the status
        if ( $this->submitted_at != NULL )
        {
            $this->active = FALSE;
            $this->status = 'submitted';
        }
        if ( $this->approved_at != NULL )
        {
            $this->active = FALSE;
            $this->status = 'approved';
        }
        if ( $this->cancelled_at != NULL )
        {
            $this->active = FALSE;
            $this->status = 'cancelled';
        }

        if ( ! $this->enabled )  //Only an admin can set this status.
        {
            $this->active = FALSE;
            $this->status = 'disabled';
        }

        if ( $this->deleted_at != NULL ) //Only an admin can delete projects, this is the trump status
        {
            $this->active = FALSE;
            $this->status = 'deleted';
        }
        
    }

    /**
	 * Perform the actions for submitting a project for review
     *  ie setting the status, emailing admins
     *
	 */
    public function submitProject()
    {
        if ( $this->active )
        {
            $time = new DateTime;
            $this->submitted_at = $time;
            $this->save();
            //TODO: send admin alert email, log action, etc          
        }
        else
        {
            $this->errors[] = Lang::get('errors.project_cannot_submit_not_current');
        }        
    }

    /**
     * Find and loop through every active project and update the status
     *  so the customer is working with a current list of projects
     */
    public function updateActiveProjectStatus($user)
    {
        $projects = Project::where('user_id', '=', $user->id)
                            ->where('status', '=', 'active')
                            ->get();
        if ( $projects )
        {
            foreach($projects as $project)
            {
                $prevStatus = $project->status;
                $project->setStatus();
                if( $prevStatus != $project->status )
                {
                    $project->save();
                }
            }
        }

    }

    /**
     * Return an integer of the number of locations in the project
    */
    public function projectCount()
    {
        return $this->locations->count();
    }

    /**
     * Return an array of locations that have been added to the project
     */
    public function getLocations()
    {
        return $this->locations;
    }

    /**
     * Return an boolean if the location exists in within the project
     */
    public function hasLocation($location)
    {
        $locations = $this->locations;
        $result = $locations->contains($location->id);
        return $result;
    }


    /**
     * Add the location to the project if the location doesn't
     * already exist in the project
     * return Boolean success
     */
    public function addLocation($location)
    {
        try
        {
            $now = new DateTime();
            $this->locations()->attach($location->id, array('added_on' => $now));    
            return TRUE;
        } catch(Exception $e) {
            return FALSE;
        }
    }

    /**
     * Remove any instance of the location from the project
     */
    public function removeLocation($location)
    {
        try
        {
            $this->locations()->detach($location->id);    
            return TRUE;
        } catch(Exception $e) {
            return FALSE;
        }    }

}