<?php


use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;
use LaravelBook\Ardent\Ardent;

class User extends Eloquent implements UserInterface, RemindableInterface, IAccountRepository {

	/**
	 * The database fields the model can mass assign.
	 *
	 * @var array
	 */
    protected $fillable = array(
                            'company', 
                            'title', 
                            'first', 
                            'last', 
                            'address_1', 
                            'address_2', 
                            'address_3', 
                            'city', 
                            'state', 
                            'zip', 
                            'country', 
                            'phone', 
                            'mobile',
                            'fax',
                            'web_site',
                            'challenge_question',
                            'challenge_answer');

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

    /**
     * Ardent validation rules
     */
    public static $rules = array(
        'first' => 'required',
        'last' => 'required',
        'email' => 'required',
        'password' => 'required'
    );

    /**
     * Array used by FactoryMuff to create Test objects
     */
    public static $factory = array(
        'first' => 'string',
        'last' => 'string',
        'address_1' => 'string',
        'address_2' => 'string',
        'address_3' => 'string',
        'city' => 'string',
        'state' => 'string',
        'zip' => 'string',
        'country' => 'string',
        'email' => 'email',
        'password' => 'access',
        'privilege' => 1,
        'acting_as' => NULL,
        'enabled' => 1
    );

    public function projects()
    {
        return $this->hasMany('project');
    }

    /**
	 * Attributes
	*/
    public $activeProject;
    public $acting_as = NULL;

    public function __construct($attributes = array(), $exists = false) 
    {
        parent::__construct($attributes, $exists);
        // Your construct code.
        $acting_as = Session::get('acting_as', function() { return NULL; });
        $this->acting_as = $acting_as;
    }

	/**
	 * Changes the account password
	 *
	 * @return User
	 */
    public function changePassword($inputs)
	{
        if (Hash::check($inputs['currentpassword'], $this->password))
        {
            //TODO - try, catch and throw exception
            // The passwords match...
            $this->password = Hash::make($inputs['password']);
            $this->save();
            return $this;
        }
        else
        {
            //TODO: return some error about an invalid current password
            return NULL;
        }
        return NULL;
    }

	/**
	 * Confirms that the input is the current password for the user
	 *
	 * @return Boolean
	 */
    public function confirmPassword($input)
	{
        return (Hash::check($input, $this->password));
    }

    /**
    *  Confirm the email address and challenge answer are the correct entries
    *
    * @return boolean
    */
    public function confirmChallenge($email, $answer)
    {
        $passed = Hash::check($answer, $this->challenge_answer);
        if($passed)
        {
            $passed = ($this->email == $email) ? TRUE : FALSE;
        }       
        return $passed;
    }

	/**
	 * Create a hashed password
	 *
	 * @return User
	 */
    public static function createPassword($input)
	{
        return Hash::make($input);
    }

	/**
	 * Creates a new Account
	 *
	 * @return User
	 */
    public static function createUser($options)
	{
        $user = new User;
        $user->first = $options['first'];
        $user->last = $options['last'];
        $user->email = $options['email'];
        $user->password = User::createPassword($options['password']);
        $user->save();

        if ( $user )
        {
            return $user;
        }
        else
        {
            //TODO: log an error, return an error???
            return NULL;
        }
	}

	/**
	 * Changes the account to a disabled state
	 *
	 * @return User
	 */
    public function disable()
	{
        //TODO - will this affect other projects?
        //TODO - check the permission of the account attempt to disabled
        //TODO - check the privilege of the current account
        //TODO - remove the privilege of the disabled account?
        $this->enabled = FALSE;
        $this->save();
        return $this;
    }

	/**
	 * Edit an existing user's challenge question and answer
	 *
	 * @return User
	 */
    public function editChallenge($options)
    {
        //TODO see if eloquent can validate before save                
        $this->challenge_question = $options['challenge_question'];
        $this->challenge_answer = Hash::make($options['challenge_answer']);
        $this->save();
        return $this;
    }

	/**
	 * Edit an existing Account
	 *
	 * @return User
	 */
    public function editUser($options)
    {               
        //TODO see if eloquent can validate before save                
        $this->update($options);
        return $this;
    }

	/**
	 * Find an existing Account
     *  by id
     *  by email
     *
	 * @return User or NULL
	 */
    public static function findUser($options)
    {
        if ( array_key_exists('id', $options) )
        {
            return User::where('id', '=', $options['id'])->first();
        }
        if ( array_key_exists('email', $options) )
        {
            return User::where('email', '=', $options['email'])->first();
        }
        return NULL;  
    }

	/**
	 * Set the user's active project if there is one
	 *
	 */
	public static function getCurrentUser()
	{
        return Auth::user();
	}

	/**
	 * Set the user's active project if there is one
	 *
	 */
	public function getActiveProject()
	{
        $this->activeProject = Project::getActiveproject($this->id);
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	/**
	 * Get a list of users that can be impersonated
	 *
	 * @return array of users
	 */
	public static function getImpersonable($search)
	{
        $pageSize = 5;

        $query = new User;
        if ( $search ) 
        {
            $query = $query->where('enabled', '=', 1);
            //$query = $query->orwhere('first', 'LIKE', '%'.$search.'%');
            //$query = $query->orwhere('last', 'LIKE', '%'.$search.'%');
            $query = $query->where('email', 'LIKE', '%'.$search.'%');
        }
        $results = $query->get();
        return $results;
	}

	/**
	 * Get the e-mail address where password reminders are sent.
	 *
	 * @return string
	 */
	public function getReminderEmail()
	{
		return $this->email;
	}

    /**
	 * Attempt the login the user with the built in security system
	 *
     * return Boolean of attempt
	 */
    public static function login($email, $secret)
    {
        Session::forget('acting_as');
        return Auth::attempt(array('email' => $email, 'password' => $secret, 'enabled' => TRUE));
    }

    /**
	 * Attempt the login the user with the built in security system
	 *
     * return Boolean of attempt
	 */
    public static function loginUser($user)
    {
        Session::forget('acting_as');
        return Auth::login($user);
    }

	/**
	 * Unauthorizes the user with the built in method
	 *
	 * @return null
	 */
    public function logout()
    {
        Session::flush();
        return Auth::logout();
    }

    /**
	 * Changes the account password to a random value
	 *
	 * @return User
	 */
    public function passwordReminder($options, $success_target)
    {
        $attempt = Password::remind($options, function($message, $user){});
        //TODO:  check for failure here

        // redirect the user to the success target
        $attempt->setTargetUrl($success_target);

        return $attempt;
    }

    /**
	 * Set the user account to act for (if privilege allows)
	 *
     * return Boolean of attempt
	 */
    public function setActAs($user)
    {
        $min_level = 50; //TODO:  add this to a config file

        if ( ! $this->enabled) { throw new Exception(Lang::get('admin.error_actas_admin_disabled')); return FALSE; }
        if ( ! $user->enabled) { throw new Exception(Lang::get('admin.error_actas_user_disabled')); return FALSE; }
        if ( $this->privilege < $min_level )  { throw new Exception(Lang::get('admin.error_actas_admin_privilege')); return FALSE; }
        if ( $this->privilege <= $user->privilege )  { throw new Exception(Lang::get('admin.error_actas_user_privilege')); return FALSE; }
        
        Session::put('acting_as', $user);
        $this->acting_as = $user;
        return TRUE;        
    }


}