<?php
use LaravelBook\Ardent\Ardent;

class Category extends Eloquent implements ICategoryRepository {

	/**
	 * The database fields the model can mass assign.
	 *
	 * @var array
	 */
    protected $fillable = array(
                            'title', 
                            'slug', 
                            'description',
                            'enabled',
                            'priority',
                            'parent_id'
                            );

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	/**
     * Belongs to many locations
     */	
    public function Locations()
    {
        return $this->belongsToMany('Location')
                ->where('locations.enabled', '=', '1')
                ->orderBy('locations.priority', 'asc', 'locations.name', 'asc');
    }

    /**
     * Ardent validation rules
     */
    public static $rules = array(
        'title' => 'required', 
        'slug' => 'required',
        'enabled' => 'required',
        'priority' => 'int'
    );

    /**
     * Array used by FactoryMuff to create Test objects
     */
    //'user_id' => 'factory|User',
    public static $factory = array(
        'title' => 'string', 
        'slug' => 'string', 
        'description' => 'text',
        'priority' => 100,
        'enabled' => 1,
        'parent_id' => NULL,    
    );


	/**
	 * EVENTS
	*/
    public static function boot()
    {
        parent::boot();

        //Project::saving(function($project) {  });
    }


	/**
	 * Attributes
	*/
    public $errors = array();



    /**
	 * Create a location category
	 *
	 * @return Category
	 */
    public static function createCategory($options)
    {
        throw new Exception('Not implemented');
    }

    /**
	 * Edit a location category
	 *
	 * @return Category
	 */
    public function editCategory($options)
    {
        throw new Exception('Not implemented');
    }

    /**
	 * Delete a location category
	 *
	 * @return Boolean
	 */
    public function deleteCategory($options)
    {
        throw new Exception('Not implemented');
    }

    /**
	 * Find a location category
	 *
	 * @return Category
	 */
    public static function findCategory($options)
    {
        if ( array_key_exists('id', $options) )
        {
            return Category::where('id', '=', $options['id'])->first();
        }
        if ( array_key_exists('slug', $options) )
        {
            return Category::where('slug', '=', $options['slug'])->first();
        }
        return NULL; 
    }

    /**
	 * Find all valid location categories
	 *
	 * @return Array of Category
	 */
    public static function getAllCategories($startingCategory = NULL)
    {
        $output = array();
       
        $cats = ( $startingCategory ) ? $startingCategory->getSubCategories() : Category::getAllParentCategories();

        foreach($cats as $cat)
        {
            $output[] = array($cat, $cat->getAllCategories($cat));
        }

        return $output;
    }

    /**
	 * Find the parent categories of the current category all the way up
	 *
	 * @return Array of Category
	 */
    public function getAncestorCategories()
    {
        $categories = array();
        
        $current_category = $this;

        $categories[] = $current_category;
        do
        {
            $current_category = $current_category->getParentCategory();
            if( $current_category ) { $categories[] = $current_category; }
        } while($current_category != NULL);
        
        return array_reverse($categories);
    }

    /**
	 * Find a location category that has no other parent
	 *
	 * @return Array of Category
	 */
    public static function getAllParentCategories()
    {
        $parents = Category::where('parent_id', '=', NULL)
                            ->where('enabled', '=', 1)
                            ->orderBy('priority', 'asc')
                            ->orderBy('name', 'asc')
                            ->get();
        return $parents;
    }

    /**
	 * Find the parent category of the current category
	 *
	 * @return Category
	 */
    public function getParentCategory()
    {
        $parent = Category::where('id', '=', $this->parent_id)
                            ->where('id', '<>', $this->id)
                            ->where('enabled', '=', 1)
                            ->first();;
        return $parent;
    }

    /**
	 * Find the child categories
	 *
	 * @return Array of Categories or NULL
	 */
    public function getSubCategories()
    {
        $children = Category::where('parent_id', '=', $this->id)
                                ->where('enabled', '=', 1)
                                ->orderBy('priority', 'asc')
                                ->orderBy('name', 'asc')
                                ->get();
        return $children;
    }

    /**
	 * Add the the location to the category
	 *
	 * @return Boolean
	 */
    public function addLocation($location)
    {
        try
        {
            $this->locations()->attach($location->id);
            return TRUE;
        }
        catch(Exception $e)
        {
            return FALSE;
        }
    }

    /**
	 * Get the the location in the category
	 *
	 * @return Array of Locations
	 */
    public function getLocations($pageSize = 10)
    {
        return Category::find($this->id)->locations()->paginate($pageSize);

    }
}