<?php

interface ILocationRepository
{

    /**
	 * Create a location
	 *
	 * @return Location
	 */
    public static function createLocation($options);

    /**
	 * Edit a location
	 *
	 * @return Location
	 */
    public function editLocation($options);

    /**
	 * Delete a location category
	 *
	 * @return Boolean
	 */
    public function deleteLocation($options);

    /**
	 * Find a location
	 *
	 * @return Location
	 */
    public static function findLocation($options);

    /**
	 * Find all locations in the system
	 *
	 * @return Array of Locations
	 */
    public static function getAllLocations($pageSize);

    /**
	 * Find a location's categories
	 *
	 * @return Array of Category
	 */
    public function getAllLocationCategories();




}