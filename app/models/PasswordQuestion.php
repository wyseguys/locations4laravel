<?php
class PasswordQuestion extends Eloquent implements IPasswordQuestion
{
	/**
	 * The database fields the model can mass assign.
	 *
	 * @var array
	 */
    protected $fillable = array('question');

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'password_questions';

    /**
	 * Get all of the available questions from the database
     *  add in the current question if it doesn't already exist
     *  in the list from the database, then shuffle the output
	 *
	 * @return Array of PasswordChallenge strings
	 */
    public function getCurrentQuestions($options = NULL)
    {
        $questions = array();

        $db_questions = PasswordQuestion::all();
        foreach($db_questions as $db_question)
        {
            $questions[$db_question->question] = $db_question->question;
        }
        
        if ( array_key_exists('current_question', $options) )
        {
            $user_question = ($options['current_question'] . "x" != "x") ? $options['current_question'] : NULL;
            if ( $user_question )
            {
                if ( ! in_array($user_question, $questions) )
                {
                    $questions[$user_question] = $user_question;   
                }
            }
        }
        shuffle($questions);

        //Reset the shuffled array to an associative array with the value == the key for the blade engine output
        $questions = array_combine(array_values($questions), array_values($questions));

        return $questions;
    }

	/**
	 * Can be used to check either the question
     *  or the answer.
	 *
	 * @return Boolean
	 */
    public function check($options)
    {
        return FALSE;
    }
}