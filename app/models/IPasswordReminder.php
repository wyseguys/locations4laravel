<?php

interface IPasswordReminder
{
    /**
	 * Get the email address associated with the token presented
	 *
	 * @return String email address
	 */
    public function getEmail($token);
}