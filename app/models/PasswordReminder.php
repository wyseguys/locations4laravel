<?php
class PasswordReminder extends Eloquent implements IPasswordReminder
{
	/**
	 * The database fields the model can mass assign.
	 *
	 * @var array
	 */
    protected $fillable = NULL;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'password_reminders';


    /**
	 * Get the email address associated with the token presented
	 *
	 * @return String email address
	 */
    public function getEmail($token)
    {
        $reminder = PasswordReminder::where('token', '=', $token)->first();
        $email = ($reminder) ? $reminder->email : NULL;
        return $email;

    }
}