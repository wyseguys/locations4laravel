<?php

/*
|--------------------------------------------------------------------------
| View Composers
|--------------------------------------------------------------------------
|
*/
View::composer('category._catparents', function($view)
{
    
    $view->with('parents', Category::getAllParentCategories());
});



/*
|--------------------------------------------------------------------------
| Binding IoC
|--------------------------------------------------------------------------
|
*/
App::bind('IAccountRepository', function($app)
{
    return new User;
});
App::bind('IPasswordQuestion', function($app)
{
    return new PasswordQuestion;
});
App::bind('IPasswordReminder', function($app)
{
    return new PasswordReminder;
});
App::bind('IProjectRepository', function($app)
{
    return new Project;
});
App::bind('ICategoryRepository', function($app)
{
    return new Category;
});
App::bind('ILocationRepository', function($app)
{
    return new Location;
});



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Wide open routes*/
Route::get('home/{slug?}', function($slug = "") { throw new Exception("Route Not Implemented"); return ""; })->where('slug', '[a-z]+'); /*Maybe this is for the CMS?*/

Route::get('account/register', 'AccountController@getRegister'); 
Route::post('account/register', 'AccountController@postRegister'); 

Route::get('account/lostpassword', 'AccountController@getLostpassword');
Route::post('account/lostpassword', 'AccountController@postLostpassword');

Route::get('account/reminder/{email}', 'AccountController@getReminder');
Route::post('account/reminder/{email}', 'AccountController@postReminder');

Route::get('account/reminded', array('as' => 'account/reminded', 'uses' => 'AccountController@getReminded'));

Route::get('account/reset/{token}', 'AccountController@getReset');
Route::post('account/reset/{token}', 'AccountController@postReset');

Route::get('login', 'AccountController@getLogin');
Route::post('login', 'AccountController@postLogin');

Route::get('quicklink/{project_key}', function($project_key) { throw new Exception("Route Not Implemented"); return ""; })->where('project_key', '[a-z]+');
Route::get('quicklink/location/{id}/{project_key}', function($id, $project_key) { throw new Exception("Route Not Implemented"); return ""; })->where(array('id' => '[0-9]+', 'project_key' => '[a-z]+'));

/*Must be logged in*/
Route::group(array('before' => 'auth'), function()
{
    Route::get('logout', 'AccountController@getLogout');

    //Account
    Route::get('account', 'AccountController@getIndex');
    Route::get('account/edit', 'AccountController@getEdit');
    Route::post('account/edit', 'AccountController@postEdit');
    Route::get('account/password', 'AccountController@getPassword');
    Route::post('account/password', 'AccountController@postPassword');
    Route::get('account/challenge', 'AccountController@getChallenge');
    Route::post('account/challenge', 'AccountController@postChallenge');

    Route::get('account/projects/{status?}', 'AccountController@getProjects');

    //Locations
    Route::get('category', 'CategoryController@getIndex');
    Route::get('category/{group_id}/{slug?}', array('as' => 'category/group', 'uses' => 'CategoryController@getGroup'))->where(array('group_id' => '[0-9]+', 'slug' => '[a-zA-Z0-9/-]+'));

    Route::get('location', 'LocationController@getIndex');
    Route::get('location/{id}/{slug?}', array('as' => 'location/listing', 'uses' => 'LocationController@getListing') )->where(array('id' => '[0-9]+', 'slug' => '[a-zA-Z0-9/-]+'));
    Route::get('location/add/{id}', array('before' => 'returnToStartAuthenticated', 'uses'=>'LocationController@getAddlocation'))->where('id', '[0-9]+');
    Route::get('location/remove/{id}', array('before' => 'returnToStartAuthenticated', 'uses'=>'LocationController@getRemoveLocation'))->where('id', '[0-9]+');
    

    Route::get('search/{keyword}/{category?}', function($keyword, $category = null) { throw new Exception("Route Not Implemented"); return ""; })->where(array('keyword' => '[a-z]+', 'category' => '[a-z]+'));
    Route::post('search', function() { throw new Exception("Route Not Implemented"); return ""; });

    Route::get('project', 'ProjectController@getCreate'); 
    Route::get('project/create', 'ProjectController@getCreate');
    Route::post('project/create', 'ProjectController@postCreate'); 

    Route::get('project/activate/{id}', array('before' => 'returnToStartAuthenticated', 'uses'=>'ProjectController@getActivate'))->where(array('id', '[0-9]+', 'location_id', '[0-9]+'));
    Route::get('project/activate/{id}/{location_id}', array('uses'=>'ProjectController@getActivate'))->where(array('id', '[0-9]+', 'location_id', '[0-9]+'));
    Route::get('project/edit/{id}', 'ProjectController@getEdit')->where('id', '[0-9]+');
    Route::post('project/edit/{id}', 'ProjectController@postEdit')->where('id', '[0-9]+');
    Route::get('project/select', 'ProjectController@getSelect');
    Route::get('project/view/{id}', 'ProjectController@getView')->where('id', '[0-9]+');

    Route::get('project/delete/{id}', function($id) { throw new Exception("Route Not Implemented"); return ""; })->where('id', '[0-9]+');
    Route::get('project/submit/{id}', array('before' => 'returnToStartAuthenticated', 'uses'=>'ProjectController@getSubmit'))->where('id', '[0-9]+');
    Route::get('project/locations/{id}', function($id) { throw new Exception("Route Not Implemented"); return ""; })->where('id', '[0-9]+');
    Route::get('project/share/{id}', function($id) { throw new Exception("Route Not Implemented"); return ""; })->where('id', '[0-9]+');
});

/*Must be an admin*/
Route::group(array('before' => 'auth|hasAdmin'), function()
{
    Route::get('admin', 'AdminController@getIndex');

    Route::get('admin/actas', 'AdminController@getActas');
    Route::get('admin/actas/{user_id?}', 'AdminController@getActas')->where('user_id', '[0-9]+');;
    Route::post('admin/actas', 'AdminController@postActas');
    /*
    ...
    Lots of admin routes here.  Some of the admin functions will be overlays on the customer functions
    */
});


/*Catch all route*/
//Route::get('{slug?}', function($slug = "") { throw new Exception("Catch All Route Not Implemented"); return ""; })->where('slug', '[a-z]+'); /*Maybe this is for the CMS?*/
Route::get('', 'AccountController@getLogin');

App::missing(function($exception)
{
    $view_data['pageTitle'] = 'Page Not Found';
    return Response::view('errors.missing', $view_data, 404);
});

