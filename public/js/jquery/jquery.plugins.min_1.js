﻿/*jslint browser: true */ /*global jQuery: true */

/**
* jQuery Validation Plugin 1.9.0
*
* http://bassistance.de/jquery-plugins/jquery-plugin-validation/
* http://docs.jquery.com/Plugins/Validation
*
* Copyright (c) 2006 - 2011 JÃ¶rn Zaefferer
*
* Dual licensed under the MIT and GPL licenses:
*   http://www.opensource.org/licenses/mit-license.php
*   http://www.gnu.org/licenses/gpl.html
*/
(function (c) {
    c.extend(c.fn, { validate: function (a) {
        if (this.length) {
            var b = c.data(this[0], "validator"); if (b) return b; this.attr("novalidate", "novalidate"); b = new c.validator(a, this[0]); c.data(this[0], "validator", b); if (b.settings.onsubmit) {
                a = this.find("input, button"); a.filter(".cancel").click(function () { b.cancelSubmit = true }); b.settings.submitHandler && a.filter(":submit").click(function () { b.submitButton = this }); this.submit(function (d) {
                    function e() {
                        if (b.settings.submitHandler) {
                            if (b.submitButton) var f = c("<input type='hidden'/>").attr("name",
b.submitButton.name).val(b.submitButton.value).appendTo(b.currentForm); b.settings.submitHandler.call(b, b.currentForm); b.submitButton && f.remove(); return false
                        } return true
                    } b.settings.debug && d.preventDefault(); if (b.cancelSubmit) { b.cancelSubmit = false; return e() } if (b.form()) { if (b.pendingRequest) { b.formSubmitted = true; return false } return e() } else { b.focusInvalid(); return false } 
                })
            } return b
        } else a && a.debug && window.console && console.warn("nothing selected, can't validate, returning nothing")
    }, valid: function () {
        if (c(this[0]).is("form")) return this.validate().form();
        else { var a = true, b = c(this[0].form).validate(); this.each(function () { a &= b.element(this) }); return a } 
    }, removeAttrs: function (a) { var b = {}, d = this; c.each(a.split(/\s/), function (e, f) { b[f] = d.attr(f); d.removeAttr(f) }); return b }, rules: function (a, b) {
        var d = this[0]; if (a) {
            var e = c.data(d.form, "validator").settings, f = e.rules, g = c.validator.staticRules(d); switch (a) {
                case "add": c.extend(g, c.validator.normalizeRule(b)); f[d.name] = g; if (b.messages) e.messages[d.name] = c.extend(e.messages[d.name], b.messages); break; case "remove": if (!b) {
                        delete f[d.name];
                        return g
                    } var h = {}; c.each(b.split(/\s/), function (j, i) { h[i] = g[i]; delete g[i] }); return h
            } 
        } d = c.validator.normalizeRules(c.extend({}, c.validator.metadataRules(d), c.validator.classRules(d), c.validator.attributeRules(d), c.validator.staticRules(d)), d); if (d.required) { e = d.required; delete d.required; d = c.extend({ required: e }, d) } return d
    } 
    }); c.extend(c.expr[":"], { blank: function (a) { return !c.trim("" + a.value) }, filled: function (a) { return !!c.trim("" + a.value) }, unchecked: function (a) { return !a.checked } }); c.validator = function (a,
b) { this.settings = c.extend(true, {}, c.validator.defaults, a); this.currentForm = b; this.init() }; c.validator.format = function (a, b) { if (arguments.length == 1) return function () { var d = c.makeArray(arguments); d.unshift(a); return c.validator.format.apply(this, d) }; if (arguments.length > 2 && b.constructor != Array) b = c.makeArray(arguments).slice(1); if (b.constructor != Array) b = [b]; c.each(b, function (d, e) { a = a.replace(RegExp("\\{" + d + "\\}", "g"), e) }); return a }; c.extend(c.validator, { defaults: { messages: {}, groups: {}, rules: {}, errorClass: "error",
    validClass: "valid", errorElement: "label", focusInvalid: true, errorContainer: c([]), errorLabelContainer: c([]), onsubmit: true, ignore: ":hidden", ignoreTitle: false, onfocusin: function (a) { this.lastActive = a; if (this.settings.focusCleanup && !this.blockFocusCleanup) { this.settings.unhighlight && this.settings.unhighlight.call(this, a, this.settings.errorClass, this.settings.validClass); this.addWrapper(this.errorsFor(a)).hide() } }, onfocusout: function (a) { if (!this.checkable(a) && (a.name in this.submitted || !this.optional(a))) this.element(a) },
    onkeyup: function (a) { if (a.name in this.submitted || a == this.lastElement) this.element(a) }, onclick: function (a) { if (a.name in this.submitted) this.element(a); else a.parentNode.name in this.submitted && this.element(a.parentNode) }, highlight: function (a, b, d) { a.type === "radio" ? this.findByName(a.name).addClass(b).removeClass(d) : c(a).addClass(b).removeClass(d) }, unhighlight: function (a, b, d) { a.type === "radio" ? this.findByName(a.name).removeClass(b).addClass(d) : c(a).removeClass(b).addClass(d) } 
}, setDefaults: function (a) {
    c.extend(c.validator.defaults,
a)
}, messages: { required: "This field is required.", remote: "Please fix this field.", email: "Please enter a valid email address.", url: "Please enter a valid URL.", date: "Please enter a valid date.", dateISO: "Please enter a valid date (ISO).", number: "Please enter a valid number.", digits: "Please enter only digits.", creditcard: "Please enter a valid credit card number.", equalTo: "Please enter the same value again.", accept: "Please enter a value with a valid extension.", maxlength: c.validator.format("Please enter no more than {0} characters."),
    minlength: c.validator.format("Please enter at least {0} characters."), rangelength: c.validator.format("Please enter a value between {0} and {1} characters long."), range: c.validator.format("Please enter a value between {0} and {1}."), max: c.validator.format("Please enter a value less than or equal to {0}."), min: c.validator.format("Please enter a value greater than or equal to {0}.")
}, autoCreateRanges: false, prototype: { init: function () {
    function a(e) {
        var f = c.data(this[0].form, "validator"), g = "on" + e.type.replace(/^validate/,
""); f.settings[g] && f.settings[g].call(f, this[0], e)
    } this.labelContainer = c(this.settings.errorLabelContainer); this.errorContext = this.labelContainer.length && this.labelContainer || c(this.currentForm); this.containers = c(this.settings.errorContainer).add(this.settings.errorLabelContainer); this.submitted = {}; this.valueCache = {}; this.pendingRequest = 0; this.pending = {}; this.invalid = {}; this.reset(); var b = this.groups = {}; c.each(this.settings.groups, function (e, f) { c.each(f.split(/\s/), function (g, h) { b[h] = e }) }); var d =
this.settings.rules; c.each(d, function (e, f) { d[e] = c.validator.normalizeRule(f) }); c(this.currentForm).validateDelegate("[type='text'], [type='password'], [type='file'], select, textarea, [type='number'], [type='search'] ,[type='tel'], [type='url'], [type='email'], [type='datetime'], [type='date'], [type='month'], [type='week'], [type='time'], [type='datetime-local'], [type='range'], [type='color'] ", "focusin focusout keyup", a).validateDelegate("[type='radio'], [type='checkbox'], select, option", "click",
a); this.settings.invalidHandler && c(this.currentForm).bind("invalid-form.validate", this.settings.invalidHandler)
}, form: function () { this.checkForm(); c.extend(this.submitted, this.errorMap); this.invalid = c.extend({}, this.errorMap); this.valid() || c(this.currentForm).triggerHandler("invalid-form", [this]); this.showErrors(); return this.valid() }, checkForm: function () { this.prepareForm(); for (var a = 0, b = this.currentElements = this.elements(); b[a]; a++) this.check(b[a]); return this.valid() }, element: function (a) {
    this.lastElement =
a = this.validationTargetFor(this.clean(a)); this.prepareElement(a); this.currentElements = c(a); var b = this.check(a); if (b) delete this.invalid[a.name]; else this.invalid[a.name] = true; if (!this.numberOfInvalids()) this.toHide = this.toHide.add(this.containers); this.showErrors(); return b
}, showErrors: function (a) {
    if (a) { c.extend(this.errorMap, a); this.errorList = []; for (var b in a) this.errorList.push({ message: a[b], element: this.findByName(b)[0] }); this.successList = c.grep(this.successList, function (d) { return !(d.name in a) }) } this.settings.showErrors ?
this.settings.showErrors.call(this, this.errorMap, this.errorList) : this.defaultShowErrors()
}, resetForm: function () { c.fn.resetForm && c(this.currentForm).resetForm(); this.submitted = {}; this.lastElement = null; this.prepareForm(); this.hideErrors(); this.elements().removeClass(this.settings.errorClass) }, numberOfInvalids: function () { return this.objectLength(this.invalid) }, objectLength: function (a) { var b = 0, d; for (d in a) b++; return b }, hideErrors: function () { this.addWrapper(this.toHide).hide() }, valid: function () {
    return this.size() ==
0
}, size: function () { return this.errorList.length }, focusInvalid: function () { if (this.settings.focusInvalid) try { c(this.findLastActive() || this.errorList.length && this.errorList[0].element || []).filter(":visible").focus().trigger("focusin") } catch (a) { } }, findLastActive: function () { var a = this.lastActive; return a && c.grep(this.errorList, function (b) { return b.element.name == a.name }).length == 1 && a }, elements: function () {
    var a = this, b = {}; return c(this.currentForm).find("input, select, textarea").not(":submit, :reset, :image, [disabled]").not(this.settings.ignore).filter(function () {
        !this.name &&
a.settings.debug && window.console && console.error("%o has no name assigned", this); if (this.name in b || !a.objectLength(c(this).rules())) return false; return b[this.name] = true
    })
}, clean: function (a) { return c(a)[0] }, errors: function () { return c(this.settings.errorElement + "." + this.settings.errorClass, this.errorContext) }, reset: function () { this.successList = []; this.errorList = []; this.errorMap = {}; this.toShow = c([]); this.toHide = c([]); this.currentElements = c([]) }, prepareForm: function () { this.reset(); this.toHide = this.errors().add(this.containers) },
    prepareElement: function (a) { this.reset(); this.toHide = this.errorsFor(a) }, check: function (a) {
        a = this.validationTargetFor(this.clean(a)); var b = c(a).rules(), d = false, e; for (e in b) {
            var f = { method: e, parameters: b[e] }; try { var g = c.validator.methods[e].call(this, a.value.replace(/\r/g, ""), a, f.parameters); if (g == "dependency-mismatch") d = true; else { d = false; if (g == "pending") { this.toHide = this.toHide.not(this.errorsFor(a)); return } if (!g) { this.formatAndAdd(a, f); return false } } } catch (h) {
                this.settings.debug && window.console && console.log("exception occured when checking element " +
a.id + ", check the '" + f.method + "' method", h); throw h;
            } 
        } if (!d) { this.objectLength(b) && this.successList.push(a); return true } 
    }, customMetaMessage: function (a, b) { if (c.metadata) { var d = this.settings.meta ? c(a).metadata()[this.settings.meta] : c(a).metadata(); return d && d.messages && d.messages[b] } }, customMessage: function (a, b) { var d = this.settings.messages[a]; return d && (d.constructor == String ? d : d[b]) }, findDefined: function () { for (var a = 0; a < arguments.length; a++) if (arguments[a] !== undefined) return arguments[a] }, defaultMessage: function (a,
b) { return this.findDefined(this.customMessage(a.name, b), this.customMetaMessage(a, b), !this.settings.ignoreTitle && a.title || undefined, c.validator.messages[b], "<strong>Warning: No message defined for " + a.name + "</strong>") }, formatAndAdd: function (a, b) {
    var d = this.defaultMessage(a, b.method), e = /\$?\{(\d+)\}/g; if (typeof d == "function") d = d.call(this, b.parameters, a); else if (e.test(d)) d = jQuery.format(d.replace(e, "{$1}"), b.parameters); this.errorList.push({ message: d, element: a }); this.errorMap[a.name] = d; this.submitted[a.name] =
d
}, addWrapper: function (a) { if (this.settings.wrapper) a = a.add(a.parent(this.settings.wrapper)); return a }, defaultShowErrors: function () {
    for (var a = 0; this.errorList[a]; a++) { var b = this.errorList[a]; this.settings.highlight && this.settings.highlight.call(this, b.element, this.settings.errorClass, this.settings.validClass); this.showLabel(b.element, b.message) } if (this.errorList.length) this.toShow = this.toShow.add(this.containers); if (this.settings.success) for (a = 0; this.successList[a]; a++) this.showLabel(this.successList[a]);
    if (this.settings.unhighlight) { a = 0; for (b = this.validElements(); b[a]; a++) this.settings.unhighlight.call(this, b[a], this.settings.errorClass, this.settings.validClass) } this.toHide = this.toHide.not(this.toShow); this.hideErrors(); this.addWrapper(this.toShow).show()
}, validElements: function () { return this.currentElements.not(this.invalidElements()) }, invalidElements: function () { return c(this.errorList).map(function () { return this.element }) }, showLabel: function (a, b) {
    var d = this.errorsFor(a); if (d.length) {
        d.removeClass(this.settings.validClass).addClass(this.settings.errorClass);
        d.attr("generated") && d.html(b)
    } else { d = c("<" + this.settings.errorElement + "/>").attr({ "for": this.idOrName(a), generated: true }).addClass(this.settings.errorClass).html(b || ""); if (this.settings.wrapper) d = d.hide().show().wrap("<" + this.settings.wrapper + "/>").parent(); this.labelContainer.append(d).length || (this.settings.errorPlacement ? this.settings.errorPlacement(d, c(a)) : d.insertAfter(a)) } if (!b && this.settings.success) { d.text(""); typeof this.settings.success == "string" ? d.addClass(this.settings.success) : this.settings.success(d) } this.toShow =
this.toShow.add(d)
}, errorsFor: function (a) { var b = this.idOrName(a); return this.errors().filter(function () { return c(this).attr("for") == b }) }, idOrName: function (a) { return this.groups[a.name] || (this.checkable(a) ? a.name : a.id || a.name) }, validationTargetFor: function (a) { if (this.checkable(a)) a = this.findByName(a.name).not(this.settings.ignore)[0]; return a }, checkable: function (a) { return /radio|checkbox/i.test(a.type) }, findByName: function (a) {
    var b = this.currentForm; return c(document.getElementsByName(a)).map(function (d,
e) { return e.form == b && e.name == a && e || null })
}, getLength: function (a, b) { switch (b.nodeName.toLowerCase()) { case "select": return c("option:selected", b).length; case "input": if (this.checkable(b)) return this.findByName(b.name).filter(":checked").length } return a.length }, depend: function (a, b) { return this.dependTypes[typeof a] ? this.dependTypes[typeof a](a, b) : true }, dependTypes: { "boolean": function (a) { return a }, string: function (a, b) { return !!c(a, b.form).length }, "function": function (a, b) { return a(b) } }, optional: function (a) {
    return !c.validator.methods.required.call(this,
c.trim(a.value), a) && "dependency-mismatch"
}, startRequest: function (a) { if (!this.pending[a.name]) { this.pendingRequest++; this.pending[a.name] = true } }, stopRequest: function (a, b) {
    this.pendingRequest--; if (this.pendingRequest < 0) this.pendingRequest = 0; delete this.pending[a.name]; if (b && this.pendingRequest == 0 && this.formSubmitted && this.form()) { c(this.currentForm).submit(); this.formSubmitted = false } else if (!b && this.pendingRequest == 0 && this.formSubmitted) {
        c(this.currentForm).triggerHandler("invalid-form", [this]); this.formSubmitted =
false
    } 
}, previousValue: function (a) { return c.data(a, "previousValue") || c.data(a, "previousValue", { old: null, valid: true, message: this.defaultMessage(a, "remote") }) } 
}, classRuleSettings: { required: { required: true }, email: { email: true }, url: { url: true }, date: { date: true }, dateISO: { dateISO: true }, dateDE: { dateDE: true }, number: { number: true }, numberDE: { numberDE: true }, digits: { digits: true }, creditcard: { creditcard: true} }, addClassRules: function (a, b) {
    a.constructor == String ? this.classRuleSettings[a] = b : c.extend(this.classRuleSettings,
a)
}, classRules: function (a) { var b = {}; (a = c(a).attr("class")) && c.each(a.split(" "), function () { this in c.validator.classRuleSettings && c.extend(b, c.validator.classRuleSettings[this]) }); return b }, attributeRules: function (a) { var b = {}; a = c(a); for (var d in c.validator.methods) { var e; if (e = d === "required" && typeof c.fn.prop === "function" ? a.prop(d) : a.attr(d)) b[d] = e; else if (a[0].getAttribute("type") === d) b[d] = true } b.maxlength && /-1|2147483647|524288/.test(b.maxlength) && delete b.maxlength; return b }, metadataRules: function (a) {
    if (!c.metadata) return {};
    var b = c.data(a.form, "validator").settings.meta; return b ? c(a).metadata()[b] : c(a).metadata()
}, staticRules: function (a) { var b = {}, d = c.data(a.form, "validator"); if (d.settings.rules) b = c.validator.normalizeRule(d.settings.rules[a.name]) || {}; return b }, normalizeRules: function (a, b) {
    c.each(a, function (d, e) {
        if (e === false) delete a[d]; else if (e.param || e.depends) {
            var f = true; switch (typeof e.depends) { case "string": f = !!c(e.depends, b.form).length; break; case "function": f = e.depends.call(b, b) } if (f) a[d] = e.param !== undefined ?
e.param : true; else delete a[d]
        } 
    }); c.each(a, function (d, e) { a[d] = c.isFunction(e) ? e(b) : e }); c.each(["minlength", "maxlength", "min", "max"], function () { if (a[this]) a[this] = Number(a[this]) }); c.each(["rangelength", "range"], function () { if (a[this]) a[this] = [Number(a[this][0]), Number(a[this][1])] }); if (c.validator.autoCreateRanges) { if (a.min && a.max) { a.range = [a.min, a.max]; delete a.min; delete a.max } if (a.minlength && a.maxlength) { a.rangelength = [a.minlength, a.maxlength]; delete a.minlength; delete a.maxlength } } a.messages && delete a.messages;
    return a
}, normalizeRule: function (a) { if (typeof a == "string") { var b = {}; c.each(a.split(/\s/), function () { b[this] = true }); a = b } return a }, addMethod: function (a, b, d) { c.validator.methods[a] = b; c.validator.messages[a] = d != undefined ? d : c.validator.messages[a]; b.length < 3 && c.validator.addClassRules(a, c.validator.normalizeRule(a)) }, methods: { required: function (a, b, d) {
    if (!this.depend(d, b)) return "dependency-mismatch"; switch (b.nodeName.toLowerCase()) {
        case "select": return (a = c(b).val()) && a.length > 0; case "input": if (this.checkable(b)) return this.getLength(a,
b) > 0; default: return c.trim(a).length > 0
    } 
}, remote: function (a, b, d) {
    if (this.optional(b)) return "dependency-mismatch"; var e = this.previousValue(b); this.settings.messages[b.name] || (this.settings.messages[b.name] = {}); e.originalMessage = this.settings.messages[b.name].remote; this.settings.messages[b.name].remote = e.message; d = typeof d == "string" && { url: d} || d; if (this.pending[b.name]) return "pending"; if (e.old === a) return e.valid; e.old = a; var f = this; this.startRequest(b); var g = {}; g[b.name] = a; c.ajax(c.extend(true, { url: d,
        mode: "abort", port: "validate" + b.name, dataType: "json", data: g, success: function (h) { f.settings.messages[b.name].remote = e.originalMessage; var j = h === true; if (j) { var i = f.formSubmitted; f.prepareElement(b); f.formSubmitted = i; f.successList.push(b); f.showErrors() } else { i = {}; h = h || f.defaultMessage(b, "remote"); i[b.name] = e.message = c.isFunction(h) ? h(a) : h; f.showErrors(i) } e.valid = j; f.stopRequest(b, j) } 
    }, d)); return "pending"
}, minlength: function (a, b, d) { return this.optional(b) || this.getLength(c.trim(a), b) >= d }, maxlength: function (a,
b, d) { return this.optional(b) || this.getLength(c.trim(a), b) <= d }, rangelength: function (a, b, d) { a = this.getLength(c.trim(a), b); return this.optional(b) || a >= d[0] && a <= d[1] }, min: function (a, b, d) { return this.optional(b) || a >= d }, max: function (a, b, d) { return this.optional(b) || a <= d }, range: function (a, b, d) { return this.optional(b) || a >= d[0] && a <= d[1] }, email: function (a, b) { return this.optional(b) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))$/i.test(a) },
    url: function (a, b) { return this.optional(b) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(a) },
    date: function (a, b) { return this.optional(b) || !/Invalid|NaN/.test(new Date(a)) }, dateISO: function (a, b) { return this.optional(b) || /^\d{4}[\/-]\d{1,2}[\/-]\d{1,2}$/.test(a) }, number: function (a, b) { return this.optional(b) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(a) }, digits: function (a, b) { return this.optional(b) || /^\d+$/.test(a) }, creditcard: function (a, b) {
        if (this.optional(b)) return "dependency-mismatch"; if (/[^0-9 -]+/.test(a)) return false; var d = 0, e = 0, f = false; a = a.replace(/\D/g, ""); for (var g = a.length - 1; g >=
0; g--) { e = a.charAt(g); e = parseInt(e, 10); if (f) if ((e *= 2) > 9) e -= 9; d += e; f = !f } return d % 10 == 0
    }, accept: function (a, b, d) { d = typeof d == "string" ? d.replace(/,/g, "|") : "png|jpe?g|gif"; return this.optional(b) || a.match(RegExp(".(" + d + ")$", "i")) }, equalTo: function (a, b, d) { d = c(d).unbind(".validate-equalTo").bind("blur.validate-equalTo", function () { c(b).valid() }); return a == d.val() } 
}
}); c.format = c.validator.format
})(jQuery);
(function (c) { var a = {}; if (c.ajaxPrefilter) c.ajaxPrefilter(function (d, e, f) { e = d.port; if (d.mode == "abort") { a[e] && a[e].abort(); a[e] = f } }); else { var b = c.ajax; c.ajax = function (d) { var e = ("port" in d ? d : c.ajaxSettings).port; if (("mode" in d ? d : c.ajaxSettings).mode == "abort") { a[e] && a[e].abort(); return a[e] = b.apply(this, arguments) } return b.apply(this, arguments) } } })(jQuery);
(function (c) {
    !jQuery.event.special.focusin && !jQuery.event.special.focusout && document.addEventListener && c.each({ focus: "focusin", blur: "focusout" }, function (a, b) { function d(e) { e = c.event.fix(e); e.type = b; return c.event.handle.call(this, e) } c.event.special[b] = { setup: function () { this.addEventListener(a, d, true) }, teardown: function () { this.removeEventListener(a, d, true) }, handler: function (e) { arguments[0] = c.event.fix(e); arguments[0].type = b; return c.event.handle.apply(this, arguments) } } }); c.extend(c.fn, { validateDelegate: function (a,
b, d) { return this.bind(b, function (e) { var f = c(e.target); if (f.is(a)) return d.apply(f, arguments) }) } 
    })
})(jQuery);


/**
* jQuery Validation Plugin Methods 1.9.0
*
* http://bassistance.de/jquery-plugins/jquery-plugin-validation/
* http://docs.jquery.com/Plugins/Validation
*
* Copyright (c) 2006 - 2011 JÃ¶rn Zaefferer
*
* Dual licensed under the MIT and GPL licenses:
*   http://www.opensource.org/licenses/mit-license.php
*   http://www.gnu.org/licenses/gpl.html
*/
(function () {
    function a(b) { return b.replace(/<.[^<>]*?>/g, " ").replace(/&nbsp;|&#160;/gi, " ").replace(/[0-9.(),;:!?%#$'"_+=\/-]*/g, "") } jQuery.validator.addMethod("maxWords", function (b, c, d) { return this.optional(c) || a(b).match(/\b\w+\b/g).length < d }, jQuery.validator.format("Please enter {0} words or less.")); jQuery.validator.addMethod("minWords", function (b, c, d) { return this.optional(c) || a(b).match(/\b\w+\b/g).length >= d }, jQuery.validator.format("Please enter at least {0} words.")); jQuery.validator.addMethod("rangeWords",
function (b, c, d) { return this.optional(c) || a(b).match(/\b\w+\b/g).length >= d[0] && b.match(/bw+b/g).length < d[1] }, jQuery.validator.format("Please enter between {0} and {1} words."))
})(); jQuery.validator.addMethod("letterswithbasicpunc", function (a, b) { return this.optional(b) || /^[a-z-.,()'\"\s]+$/i.test(a) }, "Letters or punctuation only please"); jQuery.validator.addMethod("alphanumeric", function (a, b) { return this.optional(b) || /^\w+$/i.test(a) }, "Letters, numbers, spaces or underscores only please");
jQuery.validator.addMethod("lettersonly", function (a, b) { return this.optional(b) || /^[a-z]+$/i.test(a) }, "Letters only please"); jQuery.validator.addMethod("nowhitespace", function (a, b) { return this.optional(b) || /^\S+$/i.test(a) }, "No white space please"); jQuery.validator.addMethod("ziprange", function (a, b) { return this.optional(b) || /^90[2-5]\d\{2}-\d{4}$/.test(a) }, "Your ZIP-code must be in the range 902xx-xxxx to 905-xx-xxxx");
jQuery.validator.addMethod("integer", function (a, b) { return this.optional(b) || /^-?\d+$/.test(a) }, "A positive or negative non-decimal number please");
jQuery.validator.addMethod("vinUS", function (a) { if (a.length != 17) return false; var b, c, d, f, e, g = ["A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"], i = [1, 2, 3, 4, 5, 6, 7, 8, 1, 2, 3, 4, 5, 7, 9, 2, 3, 4, 5, 6, 7, 8, 9], j = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2], h = 0; for (b = 0; b < 17; b++) { f = j[b]; d = a.slice(b, b + 1); if (b == 8) e = d; if (isNaN(d)) for (c = 0; c < g.length; c++) { if (d.toUpperCase() === g[c]) { d = i[c]; d *= f; if (isNaN(e) && c == 8) e = g[c]; break } } else d *= f; h += d } a = h % 11; if (a == 10) a = "X"; if (a == e) return true; return false },
"The specified vehicle identification number (VIN) is invalid."); jQuery.validator.addMethod("dateITA", function (a, b) { var c = false; if (/^\d{1,2}\/\d{1,2}\/\d{4}$/.test(a)) { var d = a.split("/"); c = parseInt(d[0], 10); var f = parseInt(d[1], 10); d = parseInt(d[2], 10); var e = new Date(d, f - 1, c); c = e.getFullYear() == d && e.getMonth() == f - 1 && e.getDate() == c ? true : false } else c = false; return this.optional(b) || c }, "Please enter a correct date");
jQuery.validator.addMethod("dateNL", function (a, b) { return this.optional(b) || /^\d\d?[\.\/-]\d\d?[\.\/-]\d\d\d?\d?$/.test(a) }, "Vul hier een geldige datum in."); jQuery.validator.addMethod("time", function (a, b) { return this.optional(b) || /^([01]\d|2[0-3])(:[0-5]\d){0,2}$/.test(a) }, "Please enter a valid time, between 00:00 and 23:59"); jQuery.validator.addMethod("time12h", function (a, b) { return this.optional(b) || /^((0?[1-9]|1[012])(:[0-5]\d){0,2}(\ [AP]M))$/i.test(a) }, "Please enter a valid time, between 00:00 am and 12:00 pm");
jQuery.validator.addMethod("phoneUS", function (a, b) { a = a.replace(/\s+/g, ""); return this.optional(b) || a.length > 9 && a.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/) }, "Please specify a valid phone number"); jQuery.validator.addMethod("phoneUK", function (a, b) { return this.optional(b) || a.length > 9 && a.match(/^(\(?(0|\+44)[1-9]{1}\d{1,4}?\)?\s?\d{3,4}\s?\d{3,4})$/) }, "Please specify a valid phone number");
jQuery.validator.addMethod("mobileUK", function (a, b) { return this.optional(b) || a.length > 9 && a.match(/^((0|\+44)7(5|6|7|8|9){1}\d{2}\s?\d{6})$/) }, "Please specify a valid mobile number"); jQuery.validator.addMethod("strippedminlength", function (a, b, c) { return jQuery(a).text().length >= c }, jQuery.validator.format("Please enter at least {0} characters"));
jQuery.validator.addMethod("email2", function (a, b) { return this.optional(b) || /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(a) }, jQuery.validator.messages.email);
jQuery.validator.addMethod("url2", function (a, b) { return this.optional(b) || /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(a) },
jQuery.validator.messages.url);
jQuery.validator.addMethod("creditcardtypes", function (a, b, c) {
    if (/[^0-9-]+/.test(a)) return false; a = a.replace(/\D/g, ""); b = 0; if (c.mastercard) b |= 1; if (c.visa) b |= 2; if (c.amex) b |= 4; if (c.dinersclub) b |= 8; if (c.enroute) b |= 16; if (c.discover) b |= 32; if (c.jcb) b |= 64; if (c.unknown) b |= 128; if (c.all) b = 255; if (b & 1 && /^(51|52|53|54|55)/.test(a)) return a.length == 16; if (b & 2 && /^(4)/.test(a)) return a.length == 16; if (b & 4 && /^(34|37)/.test(a)) return a.length == 15; if (b & 8 && /^(300|301|302|303|304|305|36|38)/.test(a)) return a.length == 14; if (b &
16 && /^(2014|2149)/.test(a)) return a.length == 15; if (b & 32 && /^(6011)/.test(a)) return a.length == 16; if (b & 64 && /^(3)/.test(a)) return a.length == 16; if (b & 64 && /^(2131|1800)/.test(a)) return a.length == 15; if (b & 128) return true; return false
}, "Please enter a valid credit card number.");
jQuery.validator.addMethod("ipv4", function (a, b) { return this.optional(b) || /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/i.test(a) }, "Please enter a valid IP v4 address.");
jQuery.validator.addMethod("ipv6", function (a, b) { return this.optional(b) || /^((([0-9A-Fa-f]{1,4}:){7}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}:[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){5}:([0-9A-Fa-f]{1,4}:)?[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){4}:([0-9A-Fa-f]{1,4}:){0,2}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){3}:([0-9A-Fa-f]{1,4}:){0,3}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){2}:([0-9A-Fa-f]{1,4}:){0,4}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){6}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(([0-9A-Fa-f]{1,4}:){0,5}:((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|(::([0-9A-Fa-f]{1,4}:){0,5}((\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b)\.){3}(\b((25[0-5])|(1\d{2})|(2[0-4]\d)|(\d{1,2}))\b))|([0-9A-Fa-f]{1,4}::([0-9A-Fa-f]{1,4}:){0,5}[0-9A-Fa-f]{1,4})|(::([0-9A-Fa-f]{1,4}:){0,6}[0-9A-Fa-f]{1,4})|(([0-9A-Fa-f]{1,4}:){1,7}:))$/i.test(a) }, "Please enter a valid IP v6 address.");
jQuery.validator.addMethod("pattern", function (a, b, c) { return this.optional(b) || c.test(a) }, "Invalid format.");


/**
* jQuery Cookie plugin
*
* Copyright (c) 2010 Klaus Hartl (stilbuero.de)
* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
*/
/**
* Create a cookie with the given key and value and other optional parameters.
*
* @example $.cookie('the_cookie', 'the_value');
* @desc Set the value of a cookie.
* @example $.cookie('the_cookie', 'the_value', { expires: 7, path: '/', domain: 'jquery.com', secure: true });
* @desc Create a cookie with all available options.
* @example $.cookie('the_cookie', 'the_value');
* @desc Create a session cookie.
* @example $.cookie('the_cookie', null);
* @desc Delete a cookie by passing null as value. Keep in mind that you have to use the same path and domain
*       used when the cookie was set.
*
* @param String key The key of the cookie.
* @param String value The value of the cookie.
* @param Object options An object literal containing key/value pairs to provide optional cookie attributes.
* @option Number|Date expires Either an integer specifying the expiration date from now on in days or a Date object.
*                             If a negative value is specified (e.g. a date in the past), the cookie will be deleted.
*                             If set to null or omitted, the cookie will be a session cookie and will not be retained
*                             when the the browser exits.
* @option String path The value of the path atribute of the cookie (default: path of page that created the cookie).
* @option String domain The value of the domain attribute of the cookie (default: domain of page that created the cookie).
* @option Boolean secure If true, the secure attribute of the cookie will be set and the cookie transmission will
*                        require a secure protocol (like HTTPS).
* @type undefined
*
* @name $.cookie
* @cat Plugins/Cookie
* @author Klaus Hartl/klaus.hartl@stilbuero.de
*/
/**
* Get the value of a cookie with the given key.
*
* @example $.cookie('the_cookie');
* @desc Get the value of a cookie.
*
* @param String key The key of the cookie.
* @return The value of the cookie.
* @type String
*
* @name $.cookie
* @cat Plugins/Cookie
* @author Klaus Hartl/klaus.hartl@stilbuero.de
*/
jQuery.cookie = function (key, value, options) { if (arguments.length > 1 && String(value) !== "[object Object]") { options = jQuery.extend({}, options); if (value === null || value === undefined) { options.expires = -1 } if (typeof options.expires === 'number') { var days = options.expires, t = options.expires = new Date(); t.setDate(t.getDate() + days) } value = String(value); return (document.cookie = [encodeURIComponent(key), '=', options.raw ? value : encodeURIComponent(value), options.expires ? '; expires=' + options.expires.toUTCString() : '', options.path ? '; path=' + options.path : '', options.domain ? '; domain=' + options.domain : '', options.secure ? '; secure' : ''].join('')) } options = value || {}; var result, decode = options.raw ? function (s) { return s } : decodeURIComponent; return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null };


//http://stackoverflow.com/questions/901115/get-query-string-values-in-javascript/3867610#3867610
// URL Get Parameters
//var someVar = $.getQueryString('myParam');
; (function ($) { $.extend({ getQueryString: function (name) { function parseParams() { var params = {}, e, a = /\+/g, r = /([^&=]+)=?([^&]*)/g, d = function (s) { return decodeURIComponent(s.replace(a, " ")) }, q = window.location.search.substring(1); while (e = r.exec(q)) params[d(e[1])] = d(e[2]); return params } if (!this.queryStringParams) this.queryStringParams = parseParams(); return this.queryStringParams[name] } }) })(jQuery);


/*
* Lazy Load - jQuery plugin for lazy loading images
*
* Copyright (c) 2007-2011 Mika Tuupola
*
* Licensed under the MIT license:
*   http://www.opensource.org/licenses/mit-license.php
*
* Project home:
*   http://www.appelsiini.net/projects/lazyload
*
* Version:  1.6.0-dev
*
*/
(function($){$.fn.lazyload=function(options){var settings={threshold:0,failurelimit:0,event:"scroll",effect:"show",container:window};if(options){$.extend(settings,options)}var elements=this;if("scroll"==settings.event){$(settings.container).bind("scroll",function(event){var counter=0;elements.each(function(){if($.abovethetop(this,settings)||$.leftofbegin(this,settings)){}else if(!$.belowthefold(this,settings)&&!$.rightoffold(this,settings)){$(this).trigger("appear")}else{if(counter++>settings.failurelimit){return false}}});var temp=$.grep(elements,function(element){return!element.loaded});elements=$(temp)})}this.each(function(){var self=this;self.loaded=false;$(self).one("appear",function(){if(!this.loaded){$("<img />").bind("load",function(){$(self).hide().attr("src",$(self).data("original")).attr("width","")[settings.effect](settings.effectspeed);self.loaded=true}).attr("src",$(self).data("original"))}});if("scroll"!=settings.event){$(self).bind(settings.event,function(event){if(!self.loaded){$(self).trigger("appear")}})}});$(settings.container).trigger(settings.event);return this};$.belowthefold=function(element,settings){if(settings.container===undefined||settings.container===window){var fold=$(window).height()+$(window).scrollTop()}else{var fold=$(settings.container).offset().top+$(settings.container).height()}return fold<=$(element).offset().top-settings.threshold};$.rightoffold=function(element,settings){if(settings.container===undefined||settings.container===window){var fold=$(window).width()+$(window).scrollLeft()}else{var fold=$(settings.container).offset().left+$(settings.container).width()}return fold<=$(element).offset().left-settings.threshold};$.abovethetop=function(element,settings){if(settings.container===undefined||settings.container===window){var fold=$(window).scrollTop()}else{var fold=$(settings.container).offset().top}return fold>=$(element).offset().top+settings.threshold+$(element).height()};$.leftofbegin=function(element,settings){if(settings.container===undefined||settings.container===window){var fold=$(window).scrollLeft()}else{var fold=$(settings.container).offset().left}return fold>=$(element).offset().left+settings.threshold+$(element).width()};$.extend($.expr[':'],{"below-the-fold":"$.belowthefold(a, {threshold : 0, container: window})","above-the-fold":"!$.belowthefold(a, {threshold : 0, container: window})","right-of-fold":"$.rightoffold(a, {threshold : 0, container: window})","left-of-fold":"!$.rightoffold(a, {threshold : 0, container: window})"})})(jQuery);




/*
 * ----------------------------- JSTORAGE -------------------------------------
 * Simple local storage wrapper to save data on the browser side, supporting
 * all major browsers - IE6+, Firefox2+, Safari4+, Chrome4+ and Opera 10.5+
 *
 * Copyright (c) 2010 Andris Reinman, andris.reinman@gmail.com
 * Project homepage: www.jstorage.info
 */

/**
 * $.jStorage
 *
 * USAGE:
 *
 * jStorage requires Prototype, MooTools or jQuery! If jQuery is used, then
 * jQuery-JSON (http://code.google.com/p/jquery-json/) is also needed.
 * (jQuery-JSON needs to be loaded BEFORE jStorage!)
 *
 * Methods:
 *
 * -set(key, value)
 * $.jStorage.set(key, value) -> saves a value
 *
 * -get(key[, default])
 * value = $.jStorage.get(key [, default]) ->
 *    retrieves value if key exists, or default if it doesn't
 *
 * -deleteKey(key)
 * $.jStorage.deleteKey(key) -> removes a key from the storage
 **/
(function () {
    function x(a, b) {
        function f() { if ("session" == a) try { j = m.parse(window.name || "{}") } catch (b) { j = {} } } var h = !1, e = 0, g, d, j = {}; Math.random(); if (b || "undefined" == typeof window[a + "Storage"]) if ("local" == a && window.globalStorage) localStorage = window.globalStorage[window.location.hostname]; else if ("userDataBehavior" == l) {
            b && (window[a + "Storage"] && window[a + "Storage"].parentNode) && window[a + "Storage"].parentNode.removeChild(window[a + "Storage"]); d = document.createElement("button"); document.getElementsByTagName("head")[0].appendChild(d);
            "local" == a ? j = c : "session" == a && f(); for (g in j) j.hasOwnProperty(g) && ("__jstorage_meta" != g && "length" != g && "undefined" != typeof j[g]) && (g in d || e++, d[g] = j[g]); d.length = e; d.key = function (a) { var b = 0, c; f(); for (c in j) if (j.hasOwnProperty(c) && "__jstorage_meta" != c && "length" != c && "undefined" != typeof j[c]) { if (b == a) return c; b++ } }; d.getItem = function (b) { f(); return "session" == a ? j[b] : o.jStorage.get(b) }; d.setItem = function (a, b) { "undefined" != typeof b && (d[a] = (b || "").toString()) }; d.removeItem = function (b) {
                if ("local" == a) return o.jStorage.deleteKey(b);
                d[b] = void 0; h = !0; b in d && d.removeAttribute(b); h = !1
            }; d.clear = function () { "session" == a ? (window.name = "", x("session", !0)) : o.jStorage.flush() }; "local" == a && (z = function (a, b) { "length" != a && (h = !0, "undefined" == typeof b ? a in d && (e--, d.removeAttribute(a)) : (a in d || e++, d[a] = (b || "").toString()), d.length = e, h = !1) }); d.attachEvent("onpropertychange", function (b) {
                if ("length" != b.propertyName && !(h || "length" == b.propertyName)) {
                    if ("local" == a) !(b.propertyName in j) && "undefined" != typeof d[b.propertyName] && e++; else if ("session" ==
a) { f(); "undefined" != typeof d[b.propertyName] && !(b.propertyName in j) ? (j[b.propertyName] = d[b.propertyName], e++) : "undefined" == typeof d[b.propertyName] && b.propertyName in j ? (delete j[b.propertyName], e--) : j[b.propertyName] = d[b.propertyName]; "session" == a && (window.name = m.stringify(j)); d.length = e; return } o.jStorage.set(b.propertyName, d[b.propertyName]); d.length = e
                } 
            }); window[a + "Storage"] = d
        } 
    } function D() {
        var a = "{}"; if ("userDataBehavior" == l) {
            i.load("jStorage"); try { a = i.getAttribute("jStorage") } catch (b) { } try {
                q =
i.getAttribute("jStorage_update")
            } catch (c) { } k.jStorage = a
        } E(); y(); F()
    } function t() {
        var a; clearTimeout(G); G = setTimeout(function () {
            if ("localStorage" == l || "globalStorage" == l) a = k.jStorage_update; else if ("userDataBehavior" == l) { i.load("jStorage"); try { a = i.getAttribute("jStorage_update") } catch (b) { } } if (a && a != q) {
                q = a; var f = m.parse(m.stringify(c.__jstorage_meta.CRC32)), h; D(); h = m.parse(m.stringify(c.__jstorage_meta.CRC32)); var e, g = [], d = []; for (e in f) f.hasOwnProperty(e) && (h[e] ? f[e] != h[e] && g.push(e) : d.push(e)); for (e in h) h.hasOwnProperty(e) &&
(f[e] || g.push(e)); r(g, "updated"); r(d, "deleted")
            } 
        }, 25)
    } function r(a, b) { a = [].concat(a || []); if ("flushed" == b) { var a = [], c; for (c in n) n.hasOwnProperty(c) && a.push(c); b = "deleted" } c = 0; for (var h = a.length; c < h; c++) if (n[a[c]]) for (var e = 0, g = n[a[c]].length; e < g; e++) n[a[c]][e](a[c], b) } function u() { var a = (+new Date).toString(); "localStorage" == l || "globalStorage" == l ? k.jStorage_update = a : "userDataBehavior" == l && (i.setAttribute("jStorage_update", a), i.save("jStorage")); t() } function E() {
        if (k.jStorage) try { c = m.parse(String(k.jStorage)) } catch (a) {
            k.jStorage =
"{}"
        } else k.jStorage = "{}"; A = k.jStorage ? String(k.jStorage).length : 0; c.__jstorage_meta || (c.__jstorage_meta = {}); c.__jstorage_meta.CRC32 || (c.__jstorage_meta.CRC32 = {})
    } function v() {
        if (c.__jstorage_meta.PubSub) { for (var a = +new Date - 2E3, b = 0, f = c.__jstorage_meta.PubSub.length; b < f; b++) if (c.__jstorage_meta.PubSub[b][0] <= a) { c.__jstorage_meta.PubSub.splice(b, c.__jstorage_meta.PubSub.length - b); break } c.__jstorage_meta.PubSub.length || delete c.__jstorage_meta.PubSub } try {
            k.jStorage = m.stringify(c), i && (i.setAttribute("jStorage",
k.jStorage), i.save("jStorage")), A = k.jStorage ? String(k.jStorage).length : 0
        } catch (h) { } 
    } function p(a) { if (!a || "string" != typeof a && "number" != typeof a) throw new TypeError("Key name must be string or numeric"); if ("__jstorage_meta" == a) throw new TypeError("Reserved key name"); return !0 } function y() {
        var a, b, f, h, e = Infinity, g = !1, d = []; clearTimeout(H); if (c.__jstorage_meta && "object" == typeof c.__jstorage_meta.TTL) {
            a = +new Date; f = c.__jstorage_meta.TTL; h = c.__jstorage_meta.CRC32; for (b in f) f.hasOwnProperty(b) && (f[b] <=
a ? (delete f[b], delete h[b], delete c[b], g = !0, d.push(b)) : f[b] < e && (e = f[b])); Infinity != e && (H = setTimeout(y, e - a)); g && (v(), u(), r(d, "deleted"))
        } 
    } function F() { if (c.__jstorage_meta.PubSub) { for (var a, b = B, f = len = c.__jstorage_meta.PubSub.length - 1; 0 <= f; f--) if (a = c.__jstorage_meta.PubSub[f], a[0] > B) { var b = a[0], h = a[1]; a = a[2]; if (s[h]) for (var e = 0, g = s[h].length; e < g; e++) s[h][e](h, m.parse(m.stringify(a))) } B = b } } var o = window.jQuery || window.$ || (window.$ = {}), m = { parse: window.JSON && (window.JSON.parse || window.JSON.decode) || String.prototype.evalJSON &&
function (a) { return String(a).evalJSON() } || o.parseJSON || o.evalJSON, stringify: Object.toJSON || window.JSON && (window.JSON.stringify || window.JSON.encode) || o.toJSON
    }; if (!m.parse || !m.stringify) throw Error("No JSON support found, include //cdnjs.cloudflare.com/ajax/libs/json2/20110223/json2.js to page"); var c = {}, k = { jStorage: "{}" }, i = null, A = 0, l = !1, n = {}, G = !1, q = 0, s = {}, B = +new Date, H, C = { isXML: function (a) { return (a = (a ? a.ownerDocument || a : 0).documentElement) ? "HTML" !== a.nodeName : !1 }, encode: function (a) {
        if (!this.isXML(a)) return !1;
        try { return (new XMLSerializer).serializeToString(a) } catch (b) { try { return a.xml } catch (c) { } } return !1
    }, decode: function (a) { var b = "DOMParser" in window && (new DOMParser).parseFromString || window.ActiveXObject && function (a) { var b = new ActiveXObject("Microsoft.XMLDOM"); b.async = "false"; b.loadXML(a); return b }; if (!b) return !1; a = b.call("DOMParser" in window && new DOMParser || window, a, "text/xml"); return this.isXML(a) ? a : !1 } 
    }, z = function () { }; o.jStorage = { version: "0.3.0", set: function (a, b, f) {
        p(a); f = f || {}; if ("undefined" == typeof b) return this.deleteKey(a),
b; if (C.isXML(b)) b = { _is_xml: !0, xml: C.encode(b) }; else { if ("function" == typeof b) return; b && "object" == typeof b && (b = m.parse(m.stringify(b))) } c[a] = b; var h = c.__jstorage_meta.CRC32, e = m.stringify(b), g, d = 0, d = 0; g = -1; for (var j = 0, i = e.length; j < i; j++) d = (g ^ e.charCodeAt(j)) & 255, d = "0x" + "00000000 77073096 EE0E612C 990951BA 076DC419 706AF48F E963A535 9E6495A3 0EDB8832 79DCB8A4 E0D5E91E 97D2D988 09B64C2B 7EB17CBD E7B82D07 90BF1D91 1DB71064 6AB020F2 F3B97148 84BE41DE 1ADAD47D 6DDDE4EB F4D4B551 83D385C7 136C9856 646BA8C0 FD62F97A 8A65C9EC 14015C4F 63066CD9 FA0F3D63 8D080DF5 3B6E20C8 4C69105E D56041E4 A2677172 3C03E4D1 4B04D447 D20D85FD A50AB56B 35B5A8FA 42B2986C DBBBC9D6 ACBCF940 32D86CE3 45DF5C75 DCD60DCF ABD13D59 26D930AC 51DE003A C8D75180 BFD06116 21B4F4B5 56B3C423 CFBA9599 B8BDA50F 2802B89E 5F058808 C60CD9B2 B10BE924 2F6F7C87 58684C11 C1611DAB B6662D3D 76DC4190 01DB7106 98D220BC EFD5102A 71B18589 06B6B51F 9FBFE4A5 E8B8D433 7807C9A2 0F00F934 9609A88E E10E9818 7F6A0DBB 086D3D2D 91646C97 E6635C01 6B6B51F4 1C6C6162 856530D8 F262004E 6C0695ED 1B01A57B 8208F4C1 F50FC457 65B0D9C6 12B7E950 8BBEB8EA FCB9887C 62DD1DDF 15DA2D49 8CD37CF3 FBD44C65 4DB26158 3AB551CE A3BC0074 D4BB30E2 4ADFA541 3DD895D7 A4D1C46D D3D6F4FB 4369E96A 346ED9FC AD678846 DA60B8D0 44042D73 33031DE5 AA0A4C5F DD0D7CC9 5005713C 270241AA BE0B1010 C90C2086 5768B525 206F85B3 B966D409 CE61E49F 5EDEF90E 29D9C998 B0D09822 C7D7A8B4 59B33D17 2EB40D81 B7BD5C3B C0BA6CAD EDB88320 9ABFB3B6 03B6E20C 74B1D29A EAD54739 9DD277AF 04DB2615 73DC1683 E3630B12 94643B84 0D6D6A3E 7A6A5AA8 E40ECF0B 9309FF9D 0A00AE27 7D079EB1 F00F9344 8708A3D2 1E01F268 6906C2FE F762575D 806567CB 196C3671 6E6B06E7 FED41B76 89D32BE0 10DA7A5A 67DD4ACC F9B9DF6F 8EBEEFF9 17B7BE43 60B08ED5 D6D6A3E8 A1D1937E 38D8C2C4 4FDFF252 D1BB67F1 A6BC5767 3FB506DD 48B2364B D80D2BDA AF0A1B4C 36034AF6 41047A60 DF60EFC3 A867DF55 316E8EEF 4669BE79 CB61B38C BC66831A 256FD2A0 5268E236 CC0C7795 BB0B4703 220216B9 5505262F C5BA3BBE B2BD0B28 2BB45A92 5CB36A04 C2D7FFA7 B5D0CF31 2CD99E8B 5BDEAE1D 9B64C2B0 EC63F226 756AA39C 026D930A 9C0906A9 EB0E363F 72076785 05005713 95BF4A82 E2B87A14 7BB12BAE 0CB61B38 92D28E9B E5D5BE0D 7CDCEFB7 0BDBDF21 86D3D2D4 F1D4E242 68DDB3F8 1FDA836E 81BE16CD F6B9265B 6FB077E1 18B74777 88085AE6 FF0F6A70 66063BCA 11010B5C 8F659EFF F862AE69 616BFFD3 166CCF45 A00AE278 D70DD2EE 4E048354 3903B3C2 A7672661 D06016F7 4969474D 3E6E77DB AED16A4A D9D65ADC 40DF0B66 37D83BF0 A9BCAE53 DEBB9EC5 47B2CF7F 30B5FFE9 BDBDF21C CABAC28A 53B39330 24B4A3A6 BAD03605 CDD70693 54DE5729 23D967BF B3667A2E C4614AB8 5D681B02 2A6F2B94 B40BBE37 C30C8EA1 5A05DF1B 2D02EF8D".substr(9 *
d, 8), g = g >>> 8 ^ d; h[a] = g ^ -1; this.setTTL(a, f.TTL || 0); z(a, b); r(a, "updated"); return b
    }, get: function (a, b) { p(a); return a in c ? c[a] && "object" == typeof c[a] && c[a]._is_xml && c[a]._is_xml ? C.decode(c[a].xml) : c[a] : "undefined" == typeof b ? null : b }, deleteKey: function (a) { p(a); return a in c ? (delete c[a], "object" == typeof c.__jstorage_meta.TTL && a in c.__jstorage_meta.TTL && delete c.__jstorage_meta.TTL[a], delete c.__jstorage_meta.CRC32[a], z(a, void 0), v(), u(), r(a, "deleted"), !0) : !1 }, setTTL: function (a, b) {
        var f = +new Date; p(a);
        b = Number(b) || 0; return a in c ? (c.__jstorage_meta.TTL || (c.__jstorage_meta.TTL = {}), 0 < b ? c.__jstorage_meta.TTL[a] = f + b : delete c.__jstorage_meta.TTL[a], v(), y(), u(), !0) : !1
    }, getTTL: function (a) { var b = +new Date; p(a); return a in c && c.__jstorage_meta.TTL && c.__jstorage_meta.TTL[a] ? (a = c.__jstorage_meta.TTL[a] - b) || 0 : 0 }, flush: function () { c = { __jstorage_meta: { CRC32: {}} }; x("local", !0); v(); u(); r(null, "flushed"); return !0 }, storageObj: function () { function a() { } a.prototype = c; return new a }, index: function () {
        var a = [], b; for (b in c) c.hasOwnProperty(b) &&
"__jstorage_meta" != b && a.push(b); return a
    }, storageSize: function () { return A }, currentBackend: function () { return l }, storageAvailable: function () { return !!l }, listenKeyChange: function (a, b) { p(a); n[a] || (n[a] = []); n[a].push(b) }, stopListening: function (a, b) { p(a); if (n[a]) if (b) for (var c = n[a].length - 1; 0 <= c; c--) n[a][c] == b && n[a].splice(c, 1); else delete n[a] }, subscribe: function (a, b) { a = (a || "").toString(); if (!a) throw new TypeError("Channel not defined"); s[a] || (s[a] = []); s[a].push(b) }, publish: function (a, b) {
        a = (a || "").toString();
        if (!a) throw new TypeError("Channel not defined"); c.__jstorage_meta || (c.__jstorage_meta = {}); c.__jstorage_meta.PubSub || (c.__jstorage_meta.PubSub = []); c.__jstorage_meta.PubSub.unshift([+new Date, a, b]); v(); u()
    }, reInit: function () { D() } 
    }; a: 
    {
        var w = !1; if ("localStorage" in window) try { window.localStorage.setItem("_tmptest", "tmpval"), w = !0, window.localStorage.removeItem("_tmptest") } catch (I) { } if (w) try { window.localStorage && (k = window.localStorage, l = "localStorage", q = k.jStorage_update) } catch (J) { } else if ("globalStorage" in
window) try { window.globalStorage && (k = window.globalStorage[window.location.hostname], l = "globalStorage", q = k.jStorage_update) } catch (K) { } else if (i = document.createElement("link"), i.addBehavior) {
            i.style.behavior = "url(#default#userData)"; document.getElementsByTagName("head")[0].appendChild(i); try { i.load("jStorage") } catch (L) { i.setAttribute("jStorage", "{}"), i.save("jStorage"), i.load("jStorage") } w = "{}"; try { w = i.getAttribute("jStorage") } catch (M) { } try { q = i.getAttribute("jStorage_update") } catch (N) { } k.jStorage = w;
            l = "userDataBehavior"
        } else { i = null; break a } E(); y(); x("local"); x("session"); "localStorage" == l || "globalStorage" == l ? "addEventListener" in window ? window.addEventListener("storage", t, !1) : document.attachEvent("onstorage", t) : "userDataBehavior" == l && setInterval(t, 1E3); F(); "addEventListener" in window && window.addEventListener("pageshow", function (a) { a.persisted && t() }, !1)
    } 
})();


/*
 * JQuery URL Parser plugin, v2.2.1
 * Developed and maintanined by Mark Perkins, mark@allmarkedup.com
 * Source repository: https://github.com/allmarkedup/jQuery-URL-Parser
 * Licensed under an MIT-style license. See https://github.com/allmarkedup/jQuery-URL-Parser/blob/master/LICENSE for details.
 */

; (function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD available; use anonymous module
        if (typeof jQuery !== 'undefined') {
            define(['jquery'], factory);
        } else {
            define([], factory);
        }
    } else {
        // No AMD available; mutate global vars
        if (typeof jQuery !== 'undefined') {
            factory(jQuery);
        } else {
            factory();
        }
    }
})(function ($, undefined) {

    var tag2attr = {
        a: 'href',
        img: 'src',
        form: 'action',
        base: 'href',
        script: 'src',
        iframe: 'src',
        link: 'href'
    },

		key = ['source', 'protocol', 'authority', 'userInfo', 'user', 'password', 'host', 'port', 'relative', 'path', 'directory', 'file', 'query', 'fragment'], // keys available to query

		aliases = { 'anchor': 'fragment' }, // aliases for backwards compatability

		parser = {
		    strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,  //less intuitive, more accurate to the specs
		    loose: /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*):?([^:@]*))?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/ // more intuitive, fails on relative paths and deviates from specs
		},

		toString = Object.prototype.toString,

		isint = /^[0-9]+$/;

    function parseUri(url, strictMode) {
        var str = decodeURI(url),
		res = parser[strictMode || false ? 'strict' : 'loose'].exec(str),
		uri = { attr: {}, param: {}, seg: {} },
		i = 14;

        while (i--) {
            uri.attr[key[i]] = res[i] || '';
        }

        // build query and fragment parameters		
        uri.param['query'] = parseString(uri.attr['query']);
        uri.param['fragment'] = parseString(uri.attr['fragment']);

        // split path and fragement into segments		
        uri.seg['path'] = uri.attr.path.replace(/^\/+|\/+$/g, '').split('/');
        uri.seg['fragment'] = uri.attr.fragment.replace(/^\/+|\/+$/g, '').split('/');

        // compile a 'base' domain attribute        
        uri.attr['base'] = uri.attr.host ? (uri.attr.protocol ? uri.attr.protocol + '://' + uri.attr.host : uri.attr.host) + (uri.attr.port ? ':' + uri.attr.port : '') : '';

        return uri;
    };

    function getAttrName(elm) {
        var tn = elm.tagName;
        if (typeof tn !== 'undefined') return tag2attr[tn.toLowerCase()];
        return tn;
    }

    function promote(parent, key) {
        if (parent[key].length == 0) return parent[key] = {};
        var t = {};
        for (var i in parent[key]) t[i] = parent[key][i];
        parent[key] = t;
        return t;
    }

    function parse(parts, parent, key, val) {
        var part = parts.shift();
        if (!part) {
            if (isArray(parent[key])) {
                parent[key].push(val);
            } else if ('object' == typeof parent[key]) {
                parent[key] = val;
            } else if ('undefined' == typeof parent[key]) {
                parent[key] = val;
            } else {
                parent[key] = [parent[key], val];
            }
        } else {
            var obj = parent[key] = parent[key] || [];
            if (']' == part) {
                if (isArray(obj)) {
                    if ('' != val) obj.push(val);
                } else if ('object' == typeof obj) {
                    obj[keys(obj).length] = val;
                } else {
                    obj = parent[key] = [parent[key], val];
                }
            } else if (~part.indexOf(']')) {
                part = part.substr(0, part.length - 1);
                if (!isint.test(part) && isArray(obj)) obj = promote(parent, key);
                parse(parts, obj, part, val);
                // key
            } else {
                if (!isint.test(part) && isArray(obj)) obj = promote(parent, key);
                parse(parts, obj, part, val);
            }
        }
    }

    function merge(parent, key, val) {
        if (~key.indexOf(']')) {
            var parts = key.split('['),
			len = parts.length,
			last = len - 1;
            parse(parts, parent, 'base', val);
        } else {
            if (!isint.test(key) && isArray(parent.base)) {
                var t = {};
                for (var k in parent.base) t[k] = parent.base[k];
                parent.base = t;
            }
            set(parent.base, key, val);
        }
        return parent;
    }

    function parseString(str) {
        return reduce(String(str).split(/&|;/), function (ret, pair) {
            try {
                pair = decodeURIComponent(pair.replace(/\+/g, ' '));
            } catch (e) {
                // ignore
            }
            var eql = pair.indexOf('='),
				brace = lastBraceInKey(pair),
				key = pair.substr(0, brace || eql),
				val = pair.substr(brace || eql, pair.length),
				val = val.substr(val.indexOf('=') + 1, val.length);

            if ('' == key) key = pair, val = '';

            return merge(ret, key, val);
        }, { base: {} }).base;
    }

    function set(obj, key, val) {
        var v = obj[key];
        if (undefined === v) {
            obj[key] = val;
        } else if (isArray(v)) {
            v.push(val);
        } else {
            obj[key] = [v, val];
        }
    }

    function lastBraceInKey(str) {
        var len = str.length,
			 brace, c;
        for (var i = 0; i < len; ++i) {
            c = str[i];
            if (']' == c) brace = false;
            if ('[' == c) brace = true;
            if ('=' == c && !brace) return i;
        }
    }

    function reduce(obj, accumulator) {
        var i = 0,
			l = obj.length >> 0,
			curr = arguments[2];
        while (i < l) {
            if (i in obj) curr = accumulator.call(undefined, curr, obj[i], i, obj);
            ++i;
        }
        return curr;
    }

    function isArray(vArg) {
        return Object.prototype.toString.call(vArg) === "[object Array]";
    }

    function keys(obj) {
        var keys = [];
        for (prop in obj) {
            if (obj.hasOwnProperty(prop)) keys.push(prop);
        }
        return keys;
    }

    function purl(url, strictMode) {
        if (arguments.length === 1 && url === true) {
            strictMode = true;
            url = undefined;
        }
        strictMode = strictMode || false;
        url = url || window.location.toString();

        return {

            data: parseUri(url, strictMode),

            // get various attributes from the URI
            attr: function (attr) {
                attr = aliases[attr] || attr;
                return typeof attr !== 'undefined' ? this.data.attr[attr] : this.data.attr;
            },

            // return query string parameters
            param: function (param) {
                return typeof param !== 'undefined' ? this.data.param.query[param] : this.data.param.query;
            },

            // return fragment parameters
            fparam: function (param) {
                return typeof param !== 'undefined' ? this.data.param.fragment[param] : this.data.param.fragment;
            },

            // return path segments
            segment: function (seg) {
                if (typeof seg === 'undefined') {
                    return this.data.seg.path;
                } else {
                    seg = seg < 0 ? this.data.seg.path.length + seg : seg - 1; // negative segments count from the end
                    return this.data.seg.path[seg];
                }
            },

            // return fragment segments
            fsegment: function (seg) {
                if (typeof seg === 'undefined') {
                    return this.data.seg.fragment;
                } else {
                    seg = seg < 0 ? this.data.seg.fragment.length + seg : seg - 1; // negative segments count from the end
                    return this.data.seg.fragment[seg];
                }
            }

        };

    };

    if (typeof $ !== 'undefined') {

        $.fn.url = function (strictMode) {
            var url = '';
            if (this.length) {
                url = $(this).attr(getAttrName(this[0])) || '';
            }
            return purl(url, strictMode);
        };

        $.url = purl;

    } else {
        window.purl = purl;
    }

});
